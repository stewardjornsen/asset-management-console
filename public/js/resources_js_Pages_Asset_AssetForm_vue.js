(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Asset_AssetForm_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Asset/AssetForm.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Asset/AssetForm.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _components_StxViewItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/components/StxViewItem */ "./resources/js/components/StxViewItem.vue");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _vue_composition_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @vue/composition-api */ "./node_modules/@vue/composition-api/dist/vue-composition-api.esm.js");
/* harmony import */ var _store_mainStore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/store/mainStore */ "./resources/js/store/mainStore.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



 // import StxEditButton from "@/components/StxEditButton.vue";
// import StxSmallButton from "@/components/StxSmallButton.vue";
// import StxAddNewButton from "@/components/StxAddNewButton.vue";

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,_vue_composition_api__WEBPACK_IMPORTED_MODULE_3__.defineComponent)({
  props: {
    activeData: {
      type: Object,
      required: true
    }
  },
  components: {
    StxViewItem: _components_StxViewItem__WEBPACK_IMPORTED_MODULE_0__.default
  },
  data: function data() {
    return {
      busy: false,
      form: this.$inertia.form(_defineProperty({
        id: -1,
        name: "",
        description: "",
        code: "",
        tag_number: "",
        barcode_number: "",
        dimension: "",
        weight: "",
        standard_stock: 0,
        current_stock: 0,
        unit: "Box",
        sub_unit: "Bottle",
        sub_items_quantity: 0,
        manufacturer: "",
        country: "NG",
        model_name: "",
        model_no: "",
        serial_no: "",
        manufactured_at: "",
        year_of_construction: "",
        expire_at: "",
        warranty_start: "",
        warranty_end: "",
        service_due_at: "",
        purchased_at: "",
        purchase_amount: 0,
        purchase_currency: "NGN",
        category: "",
        status: "",
        note: "",
        is_asset: this.$page.props.is_asset
      }, "status", "Available")),
      currencies: ["NGN", "USD", "EUR", "GBP", "JPY", "CHF"]
    };
  },
  mounted: function mounted() {
    this.loadDefaults();
  },
  computed: {
    data: function data() {
      return this.activeData;
    },
    status: function status() {
      return _store_mainStore__WEBPACK_IMPORTED_MODULE_2__.status;
    },
    pageTitle: function pageTitle() {
      return parseInt(this.form.id) > 0 ? "Edit: <span class=\"font-bold\">".concat(this.form.name, "</span>") : "Add New Record";
    },
    is_asset: function is_asset() {
      return this.$page.props.is_asset === 1;
    }
  },
  methods: {
    showToast: function showToast(title, icon) {
      sweetalert2__WEBPACK_IMPORTED_MODULE_1___default().fire({
        position: "top-right",
        icon: icon,
        title: title,
        showConfirmButton: false,
        toast: true,
        timer: 2500
      });
    },
    saveChanges: function saveChanges() {
      var _this = this;

      var response = {
        preserveScroll: true,
        errorBag: "bag",
        onStart: function onStart() {
          console.log("Started");
          _this.busy = true;
        },
        onSuccess: function onSuccess(res) {
          console.log("RESPONSE", res);

          _this.form.reset();

          _this.showToast("Record has been added successfully", "success");
        },
        onError: function onError(err) {
          console.log("THERE WAS AN ERROR", err);

          _this.showToast("An error occurred!", "error");
        },
        onFinish: function onFinish() {
          console.log("Finished");
          _this.busy = false;
        }
      };

      if (this.form.id > 0) {
        this.form.put(route(this.$page.props.updateRoute, this.form.id), response);
      } else {
        console.log(JSON.stringify(this.$page.props.storeRoute, null, 2));
        this.form.post(route(this.$page.props.storeRoute), response);
      }
    },
    loadDefaults: function loadDefaults() {
      if (this.activeData.id > 0) {
        this.form.id = this.activeData.id || -1;
        this.form.name = this.activeData.name;
        this.form.description = this.activeData.description;
        this.form.code = this.activeData.code;
        this.form.tag_number = this.activeData.tag_number;
        this.form.barcode_number = this.activeData.barcode_number;
        this.form.dimension = this.activeData.dimension;
        this.form.weight = this.activeData.weight;
        this.form.standard_stock = this.activeData.standard_stock || 0;
        this.form.current_stock = this.activeData.current_stock || 0;
        this.form.unit = this.activeData.unit;
        this.form.sub_unit = this.activeData.sub_unit;
        this.form.sub_items_quantity = this.activeData.sub_items_quantity;
        this.form.manufacturer = this.activeData.manufacturer;
        this.form.country = this.activeData.country || "NG";
        this.form.model_name = this.activeData.model_name;
        this.form.model_no = this.activeData.model_no;
        this.form.serial_no = this.activeData.serial_no;
        this.form.manufactured_at = this.activeData.manufactured_at;
        this.form.year_of_construction = this.activeData.year_of_construction;
        this.form.expire_at = this.activeData.expire_at;
        this.form.warranty_start = this.activeData.warranty_start;
        this.form.warranty_end = this.activeData.warranty_end;
        this.form.service_due_at = this.activeData.service_due_at;
        this.form.purchased_at = this.activeData.purchased_at;
        this.form.purchase_amount = this.activeData.purchase_amount || 0;
        this.form.purchase_currency = this.activeData.purchase_currency || "NGN";
        this.form.category = this.activeData.category;
        this.form.status = this.activeData.status || "Active";
        this.form.note = this.activeData.note;
        this.form.is_asset = this.$page.props.is_asset;
        this.form.status = this.activeData.status;
      }
    }
  }
}));

/***/ }),

/***/ "./resources/js/Pages/Asset/AssetForm.vue":
/*!************************************************!*\
  !*** ./resources/js/Pages/Asset/AssetForm.vue ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _AssetForm_vue_vue_type_template_id_6752be3c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AssetForm.vue?vue&type=template&id=6752be3c&scoped=true& */ "./resources/js/Pages/Asset/AssetForm.vue?vue&type=template&id=6752be3c&scoped=true&");
/* harmony import */ var _AssetForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AssetForm.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Asset/AssetForm.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _AssetForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _AssetForm_vue_vue_type_template_id_6752be3c_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _AssetForm_vue_vue_type_template_id_6752be3c_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "6752be3c",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Asset/AssetForm.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Asset/AssetForm.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/js/Pages/Asset/AssetForm.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AssetForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AssetForm.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Asset/AssetForm.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AssetForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/Pages/Asset/AssetForm.vue?vue&type=template&id=6752be3c&scoped=true&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/Pages/Asset/AssetForm.vue?vue&type=template&id=6752be3c&scoped=true& ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AssetForm_vue_vue_type_template_id_6752be3c_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AssetForm_vue_vue_type_template_id_6752be3c_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AssetForm_vue_vue_type_template_id_6752be3c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AssetForm.vue?vue&type=template&id=6752be3c&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Asset/AssetForm.vue?vue&type=template&id=6752be3c&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Asset/AssetForm.vue?vue&type=template&id=6752be3c&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Asset/AssetForm.vue?vue&type=template&id=6752be3c&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "mb-10  w-full" },
    [
      _c(
        "b-form",
        {
          staticClass: " w-full",
          on: {
            submit: function($event) {
              $event.stopPropagation()
              $event.preventDefault()
              return _vm.saveChanges($event)
            }
          }
        },
        [
          _c(
            "div",
            {
              staticClass:
                "fixed top-0 right-0 rounded-bl-xl bg-gray-700 p-4 border-b border-gray-200 w-auto z-50"
            },
            [
              _c(
                "button",
                {
                  staticClass:
                    "rounded bg-brand-500 text-white py-1 px-3 focus:outline-none hover:bg-brand-400 mr-2",
                  attrs: { type: "submit", disabled: _vm.busy }
                },
                [_vm._v("\n        Save Changes\n      ")]
              ),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass:
                    "rounded bg-brand-300 text-white py-1 px-3 focus:outline-none hover:bg-brand-500",
                  attrs: { type: "button", disabled: _vm.busy },
                  on: {
                    click: function($event) {
                      return _vm.loadDefaults()
                    }
                  }
                },
                [_vm._v("\n        Reset\n      ")]
              )
            ]
          ),
          _vm._v(" "),
          _c("div", {
            staticClass: "module-header",
            domProps: { innerHTML: _vm._s(_vm.pageTitle) }
          }),
          _vm._v(" "),
          _c("module-links"),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "bg-white rounded-xl p-4 mt-2 shadow-sm" },
            [
              _c("stx-view-item", {
                attrs: { title: "Description/Status", look: "style3" }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "grid lg:grid-cols-4 gap-2" }, [
                _c(
                  "div",
                  { staticClass: "col-span-2" },
                  [
                    _vm.form && _vm.form.id
                      ? _c(
                          "b-form-group",
                          {
                            staticClass: "col-span-2",
                            attrs: {
                              id: "assetData-group-name",
                              label: "Item",
                              "label-for": "___input-name",
                              "invalid-feedback": _vm.form.errors.name,
                              state: !_vm.form.errors.name
                            }
                          },
                          [
                            _c("b-form-input", {
                              attrs: {
                                id: "___input-name",
                                type: "text",
                                state: !_vm.form.errors.name ? null : false
                              },
                              model: {
                                value: _vm.form.name,
                                callback: function($$v) {
                                  _vm.$set(_vm.form, "name", $$v)
                                },
                                expression: "form.name"
                              }
                            })
                          ],
                          1
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.form && _vm.form.id
                      ? _c(
                          "b-form-group",
                          {
                            staticClass: "w-auto",
                            attrs: {
                              id: "assetData-group-code",
                              label: "Technical Code Name",
                              "label-for": "___input-code",
                              "invalid-feedback": _vm.form.errors.code,
                              state: !_vm.form.errors.code
                            }
                          },
                          [
                            _c("b-form-input", {
                              attrs: {
                                id: "___input-code",
                                type: "text",
                                state: !_vm.form.errors.code ? null : false
                              },
                              model: {
                                value: _vm.form.code,
                                callback: function($$v) {
                                  _vm.$set(_vm.form, "code", $$v)
                                },
                                expression: "form.code"
                              }
                            })
                          ],
                          1
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.form && _vm.form.id
                      ? _c(
                          "b-form-group",
                          {
                            attrs: {
                              id: "assetData-group-tag_number",
                              label: "Tag Number",
                              "label-for": "___input-tag_number",
                              "invalid-feedback": _vm.form.errors.tag_number,
                              state: !_vm.form.errors.tag_number
                            }
                          },
                          [
                            _c("b-form-input", {
                              attrs: {
                                id: "___input-tag_number",
                                type: "text",
                                state: !_vm.form.errors.tag_number
                                  ? null
                                  : false
                              },
                              model: {
                                value: _vm.form.tag_number,
                                callback: function($$v) {
                                  _vm.$set(_vm.form, "tag_number", $$v)
                                },
                                expression: "form.tag_number"
                              }
                            })
                          ],
                          1
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.form && _vm.form.id
                      ? _c(
                          "b-form-group",
                          {
                            staticClass: "w-full",
                            attrs: {
                              id: "assetData-group-barcode_number",
                              label: "Barcode",
                              "label-for": "___input-barcode_number",
                              "invalid-feedback":
                                _vm.form.errors.barcode_number,
                              state: !_vm.form.errors.barcode_number
                            }
                          },
                          [
                            _c("b-form-input", {
                              attrs: {
                                id: "___input-barcode_number",
                                type: "text",
                                state: !_vm.form.errors.barcode_number
                                  ? null
                                  : false
                              },
                              model: {
                                value: _vm.form.barcode_number,
                                callback: function($$v) {
                                  _vm.$set(_vm.form, "barcode_number", $$v)
                                },
                                expression: "form.barcode_number"
                              }
                            })
                          ],
                          1
                        )
                      : _vm._e()
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "w-full col-span-2" },
                  [
                    _vm.form && _vm.form.id
                      ? _c(
                          "b-form-group",
                          {
                            attrs: {
                              id: "assetData-group-description",
                              label: "Description",
                              "label-for": "___input-description",
                              "invalid-feedback": _vm.form.errors.description,
                              state: !_vm.form.errors.description
                            }
                          },
                          [
                            _c("b-form-textarea", {
                              attrs: {
                                id: "___input-description",
                                rows: "6",
                                state: !_vm.form.errors.description
                                  ? null
                                  : false
                              },
                              model: {
                                value: _vm.form.description,
                                callback: function($$v) {
                                  _vm.$set(_vm.form, "description", $$v)
                                },
                                expression: "form.description"
                              }
                            })
                          ],
                          1
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.form && _vm.form.id
                      ? _c(
                          "b-form-group",
                          {
                            staticClass: "w-full",
                            attrs: {
                              id: "assetData-group-status",
                              label: "Status",
                              "label-for": "___input-status",
                              "invalid-feedback": _vm.form.errors.status,
                              state: !_vm.form.errors.status
                            }
                          },
                          [
                            _c("b-form-select", {
                              attrs: {
                                id: "___input-status",
                                options: _vm.status,
                                state: !_vm.form.errors.status ? null : false
                              },
                              model: {
                                value: _vm.form.status,
                                callback: function($$v) {
                                  _vm.$set(_vm.form, "status", $$v)
                                },
                                expression: "form.status"
                              }
                            })
                          ],
                          1
                        )
                      : _vm._e()
                  ],
                  1
                )
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "bg-white rounded-xl p-4 mt-2 shadow-sm" },
            [
              _c("stx-view-item", {
                attrs: { title: "Measurements", look: "style3" }
              }),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "grid md:grid-cols-1 xl:grid-cols-4 gap-2" },
                [
                  _vm.form && _vm.form.id
                    ? _c(
                        "b-form-group",
                        {
                          staticClass: "xl:col-span-2",
                          attrs: {
                            id: "assetData-group-dimension",
                            label: "Dimension",
                            "label-for": "___input-dimension",
                            "invalid-feedback": _vm.form.errors.dimension,
                            state: !_vm.form.errors.dimension
                          }
                        },
                        [
                          _c("b-form-input", {
                            attrs: {
                              id: "___input-dimension",
                              type: "text",
                              state: !_vm.form.errors.dimension ? null : false
                            },
                            model: {
                              value: _vm.form.dimension,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "dimension", $$v)
                              },
                              expression: "form.dimension"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.form && _vm.form.id
                    ? _c(
                        "b-form-group",
                        {
                          staticClass: "w-full",
                          attrs: {
                            id: "assetData-group-weight",
                            label: "Weight",
                            "label-for": "___input-weight",
                            "invalid-feedback": _vm.form.errors.weight,
                            state: !_vm.form.errors.weight
                          }
                        },
                        [
                          _c("b-form-input", {
                            attrs: {
                              id: "___input-weight",
                              type: "text",
                              state: !_vm.form.errors.weight ? null : false
                            },
                            model: {
                              value: _vm.form.weight,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "weight", $$v)
                              },
                              expression: "form.weight"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.form && _vm.form.id
                    ? _c(
                        "b-form-group",
                        {
                          attrs: {
                            id: "assetData-group-category",
                            label: "Category",
                            "label-for": "___input-category",
                            "invalid-feedback": _vm.form.errors.category,
                            state: !_vm.form.errors.category
                          }
                        },
                        [
                          _c("b-form-select", {
                            attrs: {
                              id: "___input-category",
                              type: "text",
                              state: !_vm.form.errors.category ? null : false
                            },
                            model: {
                              value: _vm.form.category,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "category", $$v)
                              },
                              expression: "form.category"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e()
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "bg-white rounded-xl p-4 mt-2 shadow-sm" },
            [
              _c("stx-view-item", {
                attrs: { title: "Product Info", look: "style3" }
              }),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass:
                    "grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-2"
                },
                [
                  _vm.form && _vm.form.id
                    ? _c(
                        "b-form-group",
                        {
                          staticClass: "w-full",
                          attrs: {
                            id: "assetData-group-manufacturer",
                            label: "Manufacturer",
                            "label-for": "___input-manufacturer",
                            "invalid-feedback": _vm.form.errors.manufacturer,
                            state: !_vm.form.errors.manufacturer
                          }
                        },
                        [
                          _c("b-form-input", {
                            attrs: {
                              id: "___input-manufacturer",
                              type: "text",
                              state: !_vm.form.errors.manufacturer
                                ? null
                                : false
                            },
                            model: {
                              value: _vm.form.manufacturer,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "manufacturer", $$v)
                              },
                              expression: "form.manufacturer"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.form && _vm.form.id
                    ? _c(
                        "b-form-group",
                        {
                          staticClass: "w-full",
                          attrs: {
                            id: "assetData-group-manufactured_at",
                            label: "ManufacturedDate",
                            "label-for": "___input-manufactured_at",
                            "invalid-feedback": _vm.form.errors.manufactured_at,
                            state: !_vm.form.errors.manufactured_at
                          }
                        },
                        [
                          _c("date-picker", {
                            staticClass: "w-full",
                            class: {
                              "border-2 rounded border-red-500": !!_vm.form
                                .errors.manufactured_at
                            },
                            attrs: { valueType: "format" },
                            model: {
                              value: _vm.form.manufactured_at,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "manufactured_at", $$v)
                              },
                              expression: "form.manufactured_at"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.form && _vm.form.id
                    ? _c(
                        "b-form-group",
                        {
                          attrs: {
                            id: "assetData-group-model_name",
                            label: "Model Name",
                            "label-for": "___textarea-model_name",
                            "invalid-feedback": _vm.form.errors.model_name,
                            state: !_vm.form.errors.model_name
                          }
                        },
                        [
                          _c("b-form-input", {
                            attrs: {
                              id: "___input-model_name",
                              type: "text",
                              state: !_vm.form.errors.model_name ? null : false
                            },
                            model: {
                              value: _vm.form.model_name,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "model_name", $$v)
                              },
                              expression: "form.model_name"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.form && _vm.form.id
                    ? _c(
                        "b-form-group",
                        {
                          staticClass: "w-full",
                          attrs: {
                            id: "assetData-group-model_no",
                            label: "Model No",
                            "label-for": "___input-model_no",
                            "invalid-feedback": _vm.form.errors.model_no,
                            state: !_vm.form.errors.model_no
                          }
                        },
                        [
                          _c("b-form-input", {
                            attrs: {
                              id: "___input-model_no",
                              type: "text",
                              state: !_vm.form.errors.model_no ? null : false
                            },
                            model: {
                              value: _vm.form.model_no,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "model_no", $$v)
                              },
                              expression: "form.model_no"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.form && _vm.form.id
                    ? _c(
                        "b-form-group",
                        {
                          staticClass: "w-full",
                          attrs: {
                            id: "assetData-group-serial_no",
                            label: "Serial No.",
                            "label-for": "___input-serial_no",
                            "invalid-feedback": _vm.form.errors.serial_no,
                            state: !_vm.form.errors.serial_no
                          }
                        },
                        [
                          _c("b-form-input", {
                            attrs: {
                              id: "___input-serial_no",
                              type: "text",
                              state: !_vm.form.errors.serial_no ? null : false
                            },
                            model: {
                              value: _vm.form.serial_no,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "serial_no", $$v)
                              },
                              expression: "form.serial_no"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.form && _vm.form.id
                    ? _c(
                        "b-form-group",
                        {
                          staticClass: "w-full",
                          attrs: {
                            id: "assetData-group-country",
                            label: "Country",
                            "label-for": "___input-country",
                            "invalid-feedback": _vm.form.errors.country,
                            state: !_vm.form.errors.country
                          }
                        },
                        [
                          _c("b-form-input", {
                            attrs: {
                              id: "___input-country",
                              type: "text",
                              state: !_vm.form.errors.country ? null : false
                            },
                            model: {
                              value: _vm.form.country,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "country", $$v)
                              },
                              expression: "form.country"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.form && _vm.form.id
                    ? _c(
                        "b-form-group",
                        {
                          staticClass: "w-full",
                          attrs: {
                            id: "assetData-group-year_of_construction",
                            label: "Year of Construction",
                            "label-for": "___input-year_of_construction",
                            "invalid-feedback":
                              _vm.form.errors.year_of_construction,
                            state: !_vm.form.errors.year_of_construction
                          }
                        },
                        [
                          _c("b-form-input", {
                            attrs: {
                              id: "___input-year_of_construction",
                              type: "text",
                              state: !_vm.form.errors.year_of_construction
                                ? null
                                : false
                            },
                            model: {
                              value: _vm.form.year_of_construction,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "year_of_construction", $$v)
                              },
                              expression: "form.year_of_construction"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e()
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "bg-white rounded-xl p-4 mt-2 shadow-sm" },
            [
              _c("stx-view-item", {
                attrs: { title: "Finance and Purchase Info", look: "style3" }
              }),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "grid lg:grid-cols-2 xl:grid-cols-3 gap-12" },
                [
                  _vm.form && _vm.form.id
                    ? _c(
                        "b-form-group",
                        {
                          attrs: {
                            id: "assetData-group-purchase_amount",
                            label: "Purchase Amount",
                            "label-for": "___input-purchase_amount",
                            "invalid-feedback": _vm.form.errors.purchase_amount,
                            state: !_vm.form.errors.purchase_amount
                          }
                        },
                        [
                          _c("b-form-input", {
                            attrs: {
                              id: "___input-purchase_amount",
                              type: "number",
                              state: !_vm.form.errors.purchase_amount
                                ? null
                                : false
                            },
                            model: {
                              value: _vm.form.purchase_amount,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "purchase_amount", $$v)
                              },
                              expression: "form.purchase_amount"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.form && _vm.form.id
                    ? _c(
                        "b-form-group",
                        {
                          attrs: {
                            id: "assetData-group-purchase_currency",
                            label: "Purchase Currency",
                            "label-for": "___input-purchase_currency",
                            "invalid-feedback":
                              _vm.form.errors.purchase_currency,
                            state: !_vm.form.errors.purchase_currency
                          }
                        },
                        [
                          _c("b-form-select", {
                            attrs: {
                              id: "___input-purchase_currency",
                              options: _vm.currencies,
                              state: !_vm.form.errors.purchase_currency
                                ? null
                                : false
                            },
                            model: {
                              value: _vm.form.purchase_currency,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "purchase_currency", $$v)
                              },
                              expression: "form.purchase_currency"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.form && _vm.form.id
                    ? _c(
                        "b-form-group",
                        {
                          staticClass: "w-full",
                          attrs: {
                            id: "assetData-group-purchased_at",
                            label: "Purchased Date",
                            "label-for": "___input-purchased_at",
                            "invalid-feedback": _vm.form.errors.purchased_at,
                            state: !_vm.form.errors.purchased_at
                          }
                        },
                        [
                          _c("date-picker", {
                            staticClass: "w-full",
                            class: {
                              "border-2 rounded border-red-500": !!_vm.form
                                .errors.purchased_at
                            },
                            attrs: { valueType: "format" },
                            model: {
                              value: _vm.form.purchased_at,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "purchased_at", $$v)
                              },
                              expression: "form.purchased_at"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e()
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: !_vm.is_asset,
                  expression: "!is_asset"
                }
              ],
              staticClass: "bg-white rounded-xl p-4 mt-2 shadow-sm"
            },
            [
              _c("stx-view-item", {
                attrs: { title: "Unit & Stock Info", look: "style3" }
              }),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "grid lg:grid-cols-2 xl:grid-cols-3 gap-12" },
                [
                  _vm.form && _vm.form.id
                    ? _c(
                        "b-form-group",
                        {
                          attrs: {
                            id: "assetData-group-standard_stock",
                            label: "Standard Stock",
                            "label-for": "___input-standard_stock",
                            "invalid-feedback": _vm.form.errors.standard_stock,
                            state: !_vm.form.errors.standard_stock
                          }
                        },
                        [
                          _c("b-form-input", {
                            attrs: {
                              id: "___input-standard_stock",
                              type: "number",
                              state: !_vm.form.errors.standard_stock
                                ? null
                                : false
                            },
                            model: {
                              value: _vm.form.standard_stock,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "standard_stock", $$v)
                              },
                              expression: "form.standard_stock"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.form && _vm.form.id
                    ? _c(
                        "b-form-group",
                        {
                          attrs: {
                            id: "assetData-group-current_stock",
                            label: "Current Stock",
                            "label-for": "___input-current_stock",
                            "invalid-feedback": _vm.form.errors.current_stock,
                            state: !_vm.form.errors.current_stock
                          }
                        },
                        [
                          _c("b-form-input", {
                            attrs: {
                              id: "___input-current_stock",
                              type: "number",
                              state: !_vm.form.errors.current_stock
                                ? null
                                : false
                            },
                            model: {
                              value: _vm.form.current_stock,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "current_stock", $$v)
                              },
                              expression: "form.current_stock"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.form && _vm.form.id
                    ? _c(
                        "b-form-group",
                        {
                          attrs: {
                            id: "assetData-group-unit",
                            label: "Main Measuring Unit",
                            "label-for": "___input-unit",
                            "invalid-feedback": _vm.form.errors.unit,
                            state: !_vm.form.errors.unit
                          }
                        },
                        [
                          _c("b-form-input", {
                            attrs: {
                              id: "___input-unit",
                              type: "text",
                              state: !_vm.form.errors.unit ? null : false
                            },
                            model: {
                              value: _vm.form.unit,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "unit", $$v)
                              },
                              expression: "form.unit"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.form && _vm.form.id
                    ? _c(
                        "b-form-group",
                        {
                          attrs: {
                            id: "assetData-group-sub_unit",
                            label: "Measuring Sub Unit",
                            "label-for": "___input-sub_unit",
                            "invalid-feedback": _vm.form.errors.sub_unit,
                            state: !_vm.form.errors.sub_unit
                          }
                        },
                        [
                          _c("b-form-input", {
                            attrs: {
                              id: "___input-sub_unit",
                              type: "text",
                              max: "20",
                              state: !_vm.form.errors.sub_unit ? null : false
                            },
                            model: {
                              value: _vm.form.sub_unit,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "sub_unit", $$v)
                              },
                              expression: "form.sub_unit"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.form && _vm.form.id
                    ? _c(
                        "b-form-group",
                        {
                          attrs: {
                            id: "assetData-group-sub_items_quantity",
                            label: "Quantity of Subitems in Main Unit",
                            "label-for": "___input-sub_items_quantity",
                            "invalid-feedback":
                              _vm.form.errors.sub_items_quantity,
                            state: !_vm.form.errors.sub_items_quantity
                          }
                        },
                        [
                          _c("b-form-input", {
                            attrs: {
                              id: "___input-sub_items_quantity",
                              type: "number",
                              state: !_vm.form.errors.sub_items_quantity
                                ? null
                                : false
                            },
                            model: {
                              value: _vm.form.sub_items_quantity,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "sub_items_quantity", $$v)
                              },
                              expression: "form.sub_items_quantity"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e()
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { staticClass: "grid xl:grid-cols-2 gap-2" }, [
            _c(
              "div",
              { staticClass: "bg-white rounded-xl p-4 mt-2 shadow-sm" },
              [
                _c(
                  "div",
                  [
                    _c("stx-view-item", {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.is_asset,
                          expression: "is_asset"
                        }
                      ],
                      attrs: { title: "Warranty and Service", look: "style3" }
                    }),
                    _vm._v(" "),
                    _c("stx-view-item", {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: !_vm.is_asset,
                          expression: "!is_asset"
                        }
                      ],
                      attrs: { title: "Expiration", look: "style3" }
                    }),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "grid md:grid-cols-1 xl:grid-cols-2 gap-2"
                      },
                      [
                        _vm.form && _vm.form.id
                          ? _c(
                              "b-form-group",
                              {
                                directives: [
                                  {
                                    name: "show",
                                    rawName: "v-show",
                                    value: !_vm.is_asset,
                                    expression: "!is_asset"
                                  }
                                ],
                                staticClass: "w-full",
                                attrs: {
                                  id: "assetData-group-expire_at",
                                  label: "Expires At",
                                  "label-for": "___input-expire_at",
                                  "invalid-feedback": _vm.form.errors.expire_at,
                                  state: !_vm.form.errors.expire_at
                                }
                              },
                              [
                                _c("date-picker", {
                                  staticClass: "w-full",
                                  class: {
                                    "border-2 rounded border-red-500": !!_vm
                                      .form.errors.expire_at
                                  },
                                  attrs: { valueType: "format" },
                                  model: {
                                    value: _vm.form.expire_at,
                                    callback: function($$v) {
                                      _vm.$set(_vm.form, "expire_at", $$v)
                                    },
                                    expression: "form.expire_at"
                                  }
                                })
                              ],
                              1
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.form && _vm.form.id
                          ? _c(
                              "b-form-group",
                              {
                                directives: [
                                  {
                                    name: "show",
                                    rawName: "v-show",
                                    value: _vm.is_asset,
                                    expression: "is_asset"
                                  }
                                ],
                                staticClass: "w-full",
                                attrs: {
                                  id: "assetData-group-warranty_start",
                                  label: "Warranty Starts",
                                  "label-for": "___input-warranty_start",
                                  "invalid-feedback":
                                    _vm.form.errors.warranty_start,
                                  state: !_vm.form.errors.warranty_start
                                }
                              },
                              [
                                _c("date-picker", {
                                  staticClass: "w-full",
                                  class: {
                                    "border-2 rounded border-red-500": !!_vm
                                      .form.errors.warranty_start
                                  },
                                  attrs: { valueType: "format" },
                                  model: {
                                    value: _vm.form.warranty_start,
                                    callback: function($$v) {
                                      _vm.$set(_vm.form, "warranty_start", $$v)
                                    },
                                    expression: "form.warranty_start"
                                  }
                                })
                              ],
                              1
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.form && _vm.form.id
                          ? _c(
                              "b-form-group",
                              {
                                directives: [
                                  {
                                    name: "show",
                                    rawName: "v-show",
                                    value: _vm.is_asset,
                                    expression: "is_asset"
                                  }
                                ],
                                staticClass: "w-full",
                                attrs: {
                                  id: "assetData-group-warranty_end",
                                  label: "Warranty Ends",
                                  "label-for": "___input-warranty_end",
                                  "invalid-feedback":
                                    _vm.form.errors.warranty_end,
                                  state: !_vm.form.errors.warranty_end
                                }
                              },
                              [
                                _c("date-picker", {
                                  staticClass: "w-full",
                                  class: {
                                    "border-2 rounded border-red-500": !!_vm
                                      .form.errors.warranty_end
                                  },
                                  attrs: { valueType: "format" },
                                  model: {
                                    value: _vm.form.warranty_end,
                                    callback: function($$v) {
                                      _vm.$set(_vm.form, "warranty_end", $$v)
                                    },
                                    expression: "form.warranty_end"
                                  }
                                })
                              ],
                              1
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.form && _vm.form.id
                          ? _c(
                              "b-form-group",
                              {
                                directives: [
                                  {
                                    name: "show",
                                    rawName: "v-show",
                                    value: _vm.is_asset,
                                    expression: "is_asset"
                                  }
                                ],
                                staticClass: "w-full",
                                attrs: {
                                  id: "assetData-group-service_due_at",
                                  label: "Service Due At",
                                  "label-for": "___input-service_due_at",
                                  "invalid-feedback":
                                    _vm.form.errors.service_due_at,
                                  state: !_vm.form.errors.service_due_at
                                }
                              },
                              [
                                _c("date-picker", {
                                  staticClass: "w-full",
                                  class: {
                                    "border-2 rounded border-red-500": !!_vm
                                      .form.errors.service_due_at
                                  },
                                  attrs: { valueType: "format" },
                                  model: {
                                    value: _vm.form.service_due_at,
                                    callback: function($$v) {
                                      _vm.$set(_vm.form, "service_due_at", $$v)
                                    },
                                    expression: "form.service_due_at"
                                  }
                                })
                              ],
                              1
                            )
                          : _vm._e()
                      ],
                      1
                    )
                  ],
                  1
                )
              ]
            ),
            _vm._v(" "),
            _c("div", [
              _c(
                "div",
                { staticClass: "bg-white rounded-xl p-4 mt-2 shadow-sm" },
                [
                  _c("stx-view-item", {
                    attrs: { title: "Remarks/Notes/Warnings", look: "style3" }
                  }),
                  _vm._v(" "),
                  _vm.form && _vm.form.id
                    ? _c(
                        "b-form-group",
                        {
                          staticClass: "w-full",
                          attrs: {
                            id: "assetData-group-note",
                            label: "Notes",
                            "label-for": "___input-note",
                            "invalid-feedback": _vm.form.errors.note,
                            state: !_vm.form.errors.note
                          }
                        },
                        [
                          _c("b-form-textarea", {
                            attrs: {
                              id: "___input-note",
                              rows: "6",
                              state: !_vm.form.errors.note ? null : false
                            },
                            model: {
                              value: _vm.form.note,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "note", $$v)
                              },
                              expression: "form.note"
                            }
                          })
                        ],
                        1
                      )
                    : _vm._e()
                ],
                1
              )
            ])
          ])
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);