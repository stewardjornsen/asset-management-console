(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Asset_AssetLinks_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Asset/AssetLinks.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Asset/AssetLinks.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  props: {
    id: {
      type: Number,
      "default": -1
    }
  }
});

/***/ }),

/***/ "./resources/js/Pages/Asset/AssetLinks.vue":
/*!*************************************************!*\
  !*** ./resources/js/Pages/Asset/AssetLinks.vue ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _AssetLinks_vue_vue_type_template_id_8bfcce1e_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AssetLinks.vue?vue&type=template&id=8bfcce1e&scoped=true& */ "./resources/js/Pages/Asset/AssetLinks.vue?vue&type=template&id=8bfcce1e&scoped=true&");
/* harmony import */ var _AssetLinks_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AssetLinks.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Asset/AssetLinks.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _AssetLinks_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _AssetLinks_vue_vue_type_template_id_8bfcce1e_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _AssetLinks_vue_vue_type_template_id_8bfcce1e_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "8bfcce1e",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Asset/AssetLinks.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Asset/AssetLinks.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/Pages/Asset/AssetLinks.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AssetLinks_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AssetLinks.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Asset/AssetLinks.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AssetLinks_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/Pages/Asset/AssetLinks.vue?vue&type=template&id=8bfcce1e&scoped=true&":
/*!********************************************************************************************!*\
  !*** ./resources/js/Pages/Asset/AssetLinks.vue?vue&type=template&id=8bfcce1e&scoped=true& ***!
  \********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AssetLinks_vue_vue_type_template_id_8bfcce1e_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AssetLinks_vue_vue_type_template_id_8bfcce1e_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AssetLinks_vue_vue_type_template_id_8bfcce1e_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AssetLinks.vue?vue&type=template&id=8bfcce1e&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Asset/AssetLinks.vue?vue&type=template&id=8bfcce1e&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Asset/AssetLinks.vue?vue&type=template&id=8bfcce1e&scoped=true&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Asset/AssetLinks.vue?vue&type=template&id=8bfcce1e&scoped=true& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm.$page.props.id > 0
      ? _c(
          "div",
          {
            staticClass:
              "mt-10 mb-2 flex-col md:flex-row flex flex-wrap md:justify-between justify-items-stretch"
          },
          [
            _c(
              "div",
              { staticClass: "flex flex-wrap flex-col md:flex-row" },
              [
                !_vm.route().current(_vm.$page.props.indexRoute)
                  ? _c(
                      "inertia-link",
                      {
                        staticClass:
                          "bg-yellow-300 text-black rounded-full py-1 px-4 hover:opacity-75 mr-1 mt-1",
                        attrs: {
                          href: _vm.route(_vm.$page.props.indexRoute),
                          as: "button"
                        }
                      },
                      [
                        _c("b-icon", {
                          staticClass: "mr-1",
                          attrs: { icon: "arrow-left" }
                        }),
                        _vm._v(" All\n        Assets")
                      ],
                      1
                    )
                  : _vm._e(),
                _vm._v(" "),
                !_vm.route().current(_vm.$page.props.createRoute)
                  ? _c(
                      "inertia-link",
                      {
                        staticClass:
                          "bg-brand-500 text-white rounded-full py-1 px-4 hover:opacity-75 mr-1 mt-1",
                        attrs: {
                          href: _vm.route(_vm.$page.props.createRoute),
                          as: "button"
                        }
                      },
                      [
                        _c("b-icon", {
                          staticClass: "mr-1",
                          attrs: { icon: "plus-circle" }
                        }),
                        _vm._v(" Add\n        New")
                      ],
                      1
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.route().current(_vm.$page.props.showRoute)
                  ? _c(
                      "inertia-link",
                      {
                        staticClass:
                          "bg-brand-800 text-white rounded-full py-1 px-4 hover:opacity-75 mr-1 mt-1",
                        attrs: {
                          href: _vm.route(
                            _vm.$page.props.editRoute,
                            _vm.$page.props.id
                          ),
                          as: "button"
                        }
                      },
                      [
                        _c("b-icon", {
                          staticClass: "mr-1",
                          attrs: { icon: "pencil-square" }
                        }),
                        _vm._v("\n        Edit")
                      ],
                      1
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.route().current(_vm.$page.props.editRoute)
                  ? _c(
                      "inertia-link",
                      {
                        staticClass:
                          "bg-brand-800 text-white rounded-full py-1 px-4 hover:opacity-75 mr-1 mt-1",
                        attrs: {
                          href: _vm.route(
                            _vm.$page.props.showRoute,
                            _vm.$page.props.id
                          ),
                          as: "button"
                        }
                      },
                      [
                        _c("b-icon", {
                          staticClass: "mr-1",
                          attrs: { icon: "eye" }
                        }),
                        _vm._v("\n        View")
                      ],
                      1
                    )
                  : _vm._e()
              ],
              1
            ),
            _vm._v(" "),
            _c("div", [
              _vm.route().current(_vm.$page.props.showRoute)
                ? _c(
                    "button",
                    {
                      staticClass:
                        "bg-gray-900 text-white rounded-full self-stretch w-full py-1 px-4 hover:opacity-75 mr-1 mt-1",
                      on: {
                        click: function($event) {
                          return _vm.removeItem(
                            _vm.$page.props.destroyRoute,
                            _vm.$page.props.id
                          )
                        }
                      }
                    },
                    [
                      _c("b-icon", {
                        staticClass: "mr-1",
                        attrs: { icon: "trash" }
                      }),
                      _vm._v("\n        Delete\n      ")
                    ],
                    1
                  )
                : _vm._e()
            ])
          ]
        )
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);