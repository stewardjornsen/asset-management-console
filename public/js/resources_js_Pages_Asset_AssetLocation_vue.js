(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Asset_AssetLocation_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Asset/AssetLocation.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Asset/AssetLocation.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Location_LocationView_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Location/LocationView.vue */ "./resources/js/Pages/Location/LocationView.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: {
    LocationView: _Location_LocationView_vue__WEBPACK_IMPORTED_MODULE_0__.default
  },
  props: {
    locations: {
      type: Array
    },
    assetId: Number
  },
  data: function data() {
    return {
      showLocationForm: false,
      modal: false,
      location: {},
      location_id: null
    };
  },
  mounted: function mounted() {
    console.log(this.location_one);
  },
  computed: {
    locations_all: function locations_all() {
      return this.$page.props.locations;
    },
    location_one: function location_one() {
      return this.locations;
    },
    location_selected: function location_selected() {
      var _this = this;

      if (this.locations_all) {
        return this.location = this.locations_all.find(function (item) {
          return item.id == _this.location_id;
        });
      }

      return;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Location/LocationView.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Location/LocationView.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _components_StxViewItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/components/StxViewItem */ "./resources/js/components/StxViewItem.vue");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _vue_composition_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @vue/composition-api */ "./node_modules/@vue/composition-api/dist/vue-composition-api.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,_vue_composition_api__WEBPACK_IMPORTED_MODULE_2__.defineComponent)({
  props: {
    activeData: {
      type: Object,
      required: true
    }
  },
  components: {
    StxViewItem: _components_StxViewItem__WEBPACK_IMPORTED_MODULE_0__.default
  },
  data: function data() {
    return {
      busy: false,
      form: this.$inertia.form({
        id: -1,
        name: "",
        short_name: "",
        address: "",
        city: "Victoria Island",
        state: "Lagos",
        country: "NG",
        email: "",
        mobile: "",
        phone_1: "",
        phone_2: "",
        fax: "",
        postal_zip: "",
        category: "",
        is_operating: 1
      })
    };
  },
  mounted: function mounted() {
    this.loadDefaults();
  },
  computed: {
    data: function data() {
      return this.activeData;
    }
  },
  methods: {
    showToast: function showToast(title, icon) {
      sweetalert2__WEBPACK_IMPORTED_MODULE_1___default().fire({
        position: "top-right",
        icon: icon,
        title: title,
        showConfirmButton: false,
        toast: true,
        timer: 2500
      });
    },
    saveChanges: function saveChanges() {
      var _this = this;

      var response = {
        preserveScroll: true,
        errorBag: "bag",
        onStart: function onStart() {
          console.log("Started");
          _this.busy = true;
        },
        onSuccess: function onSuccess(res) {
          console.log("RESPONSE", res);

          _this.form.reset();

          _this.showToast("Record has been added successfully", "success");
        },
        onError: function onError(err) {
          console.log("THERE WAS AN ERROR", err);

          _this.showToast("An error occurred!", "error");
        },
        onFinish: function onFinish() {
          console.log("Finished");
          _this.busy = false;
        }
      };

      if (this.form.id > 0) {
        this.form.put(route("locations.update", this.form.id), response);
      } else {
        this.form.post(route("locations.store"), response);
      }
    },
    loadDefaults: function loadDefaults() {
      //   this.form = {
      this.form.id = this.activeData.id || "-1", this.form.name = this.activeData.name || "", this.form.short_name = this.activeData.short_name || "", this.form.address = this.activeData.address || "", this.form.city = this.activeData.city || "Victoria Island", this.form.state = this.activeData.state || "Lagos", this.form.country = this.activeData.country || "NG", this.form.email = this.activeData.email || "", this.form.mobile = this.activeData.mobile || "", this.form.phone_1 = this.activeData.phone_1 || "", this.form.phone_2 = this.activeData.phone_2 || "", this.form.fax = this.activeData.fax || "", this.form.postal_zip = this.activeData.postal_zip || "", this.form.category = this.activeData.category || "", this.form.is_operating = this.activeData.is_operating; //   };
    }
  }
}));

/***/ }),

/***/ "./resources/js/Pages/Asset/AssetLocation.vue":
/*!****************************************************!*\
  !*** ./resources/js/Pages/Asset/AssetLocation.vue ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _AssetLocation_vue_vue_type_template_id_7ff200ad_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AssetLocation.vue?vue&type=template&id=7ff200ad&scoped=true& */ "./resources/js/Pages/Asset/AssetLocation.vue?vue&type=template&id=7ff200ad&scoped=true&");
/* harmony import */ var _AssetLocation_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AssetLocation.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Asset/AssetLocation.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _AssetLocation_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _AssetLocation_vue_vue_type_template_id_7ff200ad_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _AssetLocation_vue_vue_type_template_id_7ff200ad_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "7ff200ad",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Asset/AssetLocation.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Location/LocationView.vue":
/*!******************************************************!*\
  !*** ./resources/js/Pages/Location/LocationView.vue ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _LocationView_vue_vue_type_template_id_6900885b_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./LocationView.vue?vue&type=template&id=6900885b&scoped=true& */ "./resources/js/Pages/Location/LocationView.vue?vue&type=template&id=6900885b&scoped=true&");
/* harmony import */ var _LocationView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./LocationView.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Location/LocationView.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _LocationView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _LocationView_vue_vue_type_template_id_6900885b_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _LocationView_vue_vue_type_template_id_6900885b_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "6900885b",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Location/LocationView.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Asset/AssetLocation.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/Pages/Asset/AssetLocation.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AssetLocation_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AssetLocation.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Asset/AssetLocation.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AssetLocation_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/Pages/Location/LocationView.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/Pages/Location/LocationView.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_LocationView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./LocationView.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Location/LocationView.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_LocationView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/Pages/Asset/AssetLocation.vue?vue&type=template&id=7ff200ad&scoped=true&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/Pages/Asset/AssetLocation.vue?vue&type=template&id=7ff200ad&scoped=true& ***!
  \***********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AssetLocation_vue_vue_type_template_id_7ff200ad_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AssetLocation_vue_vue_type_template_id_7ff200ad_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AssetLocation_vue_vue_type_template_id_7ff200ad_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AssetLocation.vue?vue&type=template&id=7ff200ad&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Asset/AssetLocation.vue?vue&type=template&id=7ff200ad&scoped=true&");


/***/ }),

/***/ "./resources/js/Pages/Location/LocationView.vue?vue&type=template&id=6900885b&scoped=true&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/Pages/Location/LocationView.vue?vue&type=template&id=6900885b&scoped=true& ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LocationView_vue_vue_type_template_id_6900885b_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LocationView_vue_vue_type_template_id_6900885b_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LocationView_vue_vue_type_template_id_6900885b_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./LocationView.vue?vue&type=template&id=6900885b&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Location/LocationView.vue?vue&type=template&id=6900885b&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Asset/AssetLocation.vue?vue&type=template&id=7ff200ad&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Asset/AssetLocation.vue?vue&type=template&id=7ff200ad&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("div", { staticClass: "bg-white font-poppins" }, [
        _c(
          "div",
          {
            staticClass:
              "px-0 py-0  text-white rounded-t-xl flex items-center justify-between"
          },
          [
            _c(
              "inertia-link",
              {
                staticClass:
                  "border-2 border-gray-400 text-brand-800 cursor-pointer pt-1 px-2 rounded-full text-xs my-2 mx-2 hover:opacity-75",
                attrs: { href: _vm.route("locations.create"), as: "button" }
              },
              [_vm._v("ADD NEW")]
            ),
            _vm._v(" "),
            _c(
              "span",
              {
                staticClass:
                  "border-2 border-gray-400 text-brand-800 cursor-pointer pt-1 px-2 rounded-full text-xs my-2 ml-0 hover:opacity-75",
                on: {
                  click: function($event) {
                    _vm.showLocationForm = !_vm.showLocationForm
                  }
                }
              },
              [_vm._v("ASSIGN VENDOR")]
            )
          ],
          1
        ),
        _vm._v(" "),
        _vm.showLocationForm
          ? _c(
              "div",
              { staticClass: "p-2" },
              [
                _c(
                  "b-form",
                  {
                    staticClass: " w-full",
                    on: {
                      submit: function($event) {
                        $event.stopPropagation()
                        $event.preventDefault()
                        return _vm.saveChanges($event)
                      }
                    }
                  },
                  [
                    _vm.locations_all
                      ? _c(
                          "b-form-group",
                          {
                            attrs: {
                              id: "assetData-group-location_id",
                              label: "",
                              "label-for": "___input-location_id"
                            }
                          },
                          [
                            _c("b-form-select", {
                              attrs: {
                                id: "___input-location_id",
                                options: _vm.locations_all,
                                "text-field": "name",
                                "value-field": "id",
                                "select-size": 4
                              },
                              model: {
                                value: _vm.location_id,
                                callback: function($$v) {
                                  _vm.location_id = $$v
                                },
                                expression: "location_id"
                              }
                            })
                          ],
                          1
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.location_selected
                      ? _c("div", [
                          _c("div", { staticClass: "mt-1 font-semibold" }, [
                            _vm._v(
                              "\n            Location: " +
                                _vm._s(_vm.location_selected.name) +
                                "\n          "
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "mb-2 text-xs" }, [
                            _vm._v(
                              "\n            Address: " +
                                _vm._s(_vm.location_selected.address) +
                                "\n          "
                            )
                          ])
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.location_id
                      ? _c(
                          "div",
                          { staticClass: "flex justify-between flex-wrap" },
                          [
                            _c(
                              "b-button",
                              {
                                directives: [
                                  {
                                    name: "b-modal",
                                    rawName: "v-b-modal.locationModal",
                                    modifiers: { locationModal: true }
                                  }
                                ],
                                attrs: {
                                  pill: "",
                                  variants: "secondary",
                                  size: "sm"
                                }
                              },
                              [
                                _c("b-icon", {
                                  staticClass: "mr-1",
                                  attrs: { icon: "eye", scale: "0.8" }
                                }),
                                _vm._v("\n            View")
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "inertia-link",
                              {
                                staticClass:
                                  "ml-2 whitespace-nowrap bg-brand-200 text-white py-1 px-3 rounded-full",
                                attrs: {
                                  as: "button",
                                  href: _vm.route("assets.location", {
                                    asset: _vm.assetId,
                                    location: _vm.location_id
                                  })
                                }
                              },
                              [
                                _c("b-icon", {
                                  staticClass: "mr-1",
                                  attrs: { icon: "person-plus" }
                                }),
                                _vm._v("\n            Assign")
                              ],
                              1
                            )
                          ],
                          1
                        )
                      : _vm._e()
                  ],
                  1
                )
              ],
              1
            )
          : _vm._e(),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "p-2 rounded-b-2xl" },
          _vm._l(_vm.locations, function(item) {
            return _c(
              "div",
              {
                key: item.id,
                staticClass:
                  "mt-2 pt-0 border-t border-brand-200 flex justify-between flex-wrap",
                on: {
                  click: function($event) {
                    _vm.location_id = item.id
                  }
                }
              },
              [
                _c(
                  "div",
                  {
                    directives: [
                      {
                        name: "b-modal",
                        rawName: "v-b-modal.locationModal",
                        modifiers: { locationModal: true }
                      }
                    ],
                    staticClass: "text-sm py-1"
                  },
                  [
                    _c("div", { staticClass: "font-bold" }, [
                      _vm._v(_vm._s(item.name))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "font-normal text-xs" }, [
                      _vm._v(_vm._s(item.contact_person))
                    ])
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "py-1" },
                  [
                    _c(
                      "inertia-link",
                      {
                        staticClass: "text-xs",
                        attrs: {
                          href: _vm.route("assets.location", {
                            asset: _vm.assetId,
                            location: item.id,
                            action: "remove"
                          })
                        }
                      },
                      [_vm._v("Remove")]
                    )
                  ],
                  1
                )
              ]
            )
          }),
          0
        )
      ]),
      _vm._v(" "),
      _c(
        "b-modal",
        { attrs: { id: "locationModal", size: "xl" } },
        [
          _vm.location_selected
            ? _c("location-view", {
                attrs: { activeData: _vm.location_selected }
              })
            : _vm._e()
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Location/LocationView.vue?vue&type=template&id=6900885b&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Location/LocationView.vue?vue&type=template&id=6900885b&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("module-links", { attrs: { id: _vm.activeData.id } }),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "px-4 py-6 mb-10 bg-white max-w-4xl rounded-xl shadow-md"
        },
        [
          _c("h2", [_vm._v("Locations")]),
          _vm._v(" "),
          _c("div", { staticClass: "mb-4 mt-2 border-t border-gray-200" }),
          _vm._v(" "),
          _c("stx-view-item", {
            attrs: { title: "Description", look: "style2" }
          }),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "grid md:grid-cols-2 gap-6" },
            [
              _c("stx-view-item", { attrs: { title: "Name" } }, [
                _vm._v(_vm._s(_vm.data.name))
              ]),
              _vm._v(" "),
              _c("stx-view-item", { attrs: { title: "Short Name" } }, [
                _vm._v(_vm._s(_vm.data.short_name))
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c("stx-view-item", { attrs: { title: "Status", look: "style2" } }),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "grid md:grid-cols-2 gap-6" },
            [
              _c("stx-view-item", { attrs: { title: "Is Operating?" } }, [
                _vm._v(_vm._s(_vm.data.is_operating == 1 ? "Yes" : "No"))
              ]),
              _vm._v(" "),
              _c("stx-view-item", { attrs: { title: "Category" } }, [
                _vm._v(_vm._s(_vm.data.category))
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c("stx-view-item", {
            attrs: { title: "Contact Details", look: "style2" }
          }),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "grid md:grid-cols-2 gap-6" },
            [
              _c("stx-view-item", { attrs: { title: "Address" } }, [
                _vm._v(_vm._s(_vm.data.address))
              ]),
              _vm._v(" "),
              _c("stx-view-item", { attrs: { title: "City" } }, [
                _vm._v(_vm._s(_vm.data.city))
              ]),
              _vm._v(" "),
              _c("stx-view-item", { attrs: { title: "State" } }, [
                _vm._v(_vm._s(_vm.data.state))
              ]),
              _vm._v(" "),
              _c("stx-view-item", { attrs: { title: "Country" } }, [
                _vm._v(_vm._s(_vm.data.country))
              ]),
              _vm._v(" "),
              _c("stx-view-item", { attrs: { title: "Email" } }, [
                _vm._v(_vm._s(_vm.data.email))
              ]),
              _vm._v(" "),
              _c("stx-view-item", { attrs: { title: "Mobile" } }, [
                _vm._v(_vm._s(_vm.data.mobile))
              ]),
              _vm._v(" "),
              _c("stx-view-item", { attrs: { title: "Phone 1" } }, [
                _vm._v(_vm._s(_vm.data.phone_1))
              ]),
              _vm._v(" "),
              _c("stx-view-item", { attrs: { title: "Phone 2" } }, [
                _vm._v(_vm._s(_vm.data.phone_2))
              ]),
              _vm._v(" "),
              _c("stx-view-item", { attrs: { title: "Zip/Postal code" } }, [
                _vm._v(_vm._s(_vm.data.postal_zip))
              ]),
              _vm._v(" "),
              _c("stx-view-item", { attrs: { title: "Fax" } }, [
                _vm._v(_vm._s(_vm.data.fax))
              ])
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);