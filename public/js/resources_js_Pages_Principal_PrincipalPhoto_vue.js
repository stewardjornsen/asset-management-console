(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Principal_PrincipalPhoto_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Principal/PrincipalPhoto.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Principal/PrincipalPhoto.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _vue_composition_api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @vue/composition-api */ "./node_modules/@vue/composition-api/dist/vue-composition-api.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,_vue_composition_api__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  props: {
    id: Number,
    photoData: Object,
    classes: {
      type: String,
      "default": "rounded-full"
    },
    url: {
      type: String,
      required: true
    }
  },
  //   stxInertia: this.$inertia,
  data: function data() {
    // const stxRoute = this.route;
    return {
      // @ts-ignore
      form: this.$inertia.form({
        file: null,
        preserveState: true
      }),
      image: null,
      show: false
    };
  },
  mounted: function mounted() {
    // console.log(this.stxInertia);
    console.log("PHOTO DATA => 91", this.photoData); // this.$refs.pickfile.click()
  },
  computed: {
    dataTypeChecked: function dataTypeChecked() {
      return this.photoData;
    },
    progress: function progress() {
      var _this$form$progress;

      return parseInt((_this$form$progress = this.form.progress) === null || _this$form$progress === void 0 ? void 0 : _this$form$progress.percentage) || 0;
    },
    fullname: function fullname() {
      var _this$photoData, _this$photoData2, _this$photoData3, _this$photoData4, _this$photoData5;

      if ((_this$photoData = this.photoData) !== null && _this$photoData !== void 0 && _this$photoData.name) return (_this$photoData2 = this.photoData) === null || _this$photoData2 === void 0 ? void 0 : _this$photoData2.name;
      return ((_this$photoData3 = this.photoData) === null || _this$photoData3 === void 0 ? void 0 : _this$photoData3.first_name) + " " + ((_this$photoData4 = this.photoData) === null || _this$photoData4 === void 0 ? void 0 : _this$photoData4.middle_name) + " " + ((_this$photoData5 = this.photoData) === null || _this$photoData5 === void 0 ? void 0 : _this$photoData5.last_name);
    }
  },
  methods: {
    submit: function submit() {
      var _this$photoData6;

      var vm = this;
      var stxInertia = this.$inertia;
      var stxRoute = this.route; // console.log(this.form);

      this.form.post(stxRoute(this.url, (_this$photoData6 = this.photoData) === null || _this$photoData6 === void 0 ? void 0 : _this$photoData6.id), {
        preserveState: true,
        onError: function onError(err) {
          console.log(err);
        },
        onSuccess: function onSuccess(res) {
          console.log("success", "122", res);
          vm.$emit("finished", res);
          vm.show = false; // vm.$inertia.reload({only:['principals']})
          // vm.$inertia.get(
          //     // route("principal.index"),
          //     route("principal.edit", vm.photoData.id),
          //     { photo: res.photo },
          //     {
          //         preserveScroll: true,
          //         preserveState: false
          //     }
          // );
        }
      });
    },
    picked: function picked(e) {
      this.form.clearErrors();
      var file = window.URL.createObjectURL(e.target.files[0]);
      this.form.file = e.target.files[0];
      this.image = file; // this.submit();
    }
  }
}));

/***/ }),

/***/ "./resources/js/Pages/Principal/PrincipalPhoto.vue":
/*!*********************************************************!*\
  !*** ./resources/js/Pages/Principal/PrincipalPhoto.vue ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _PrincipalPhoto_vue_vue_type_template_id_2c07be24_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PrincipalPhoto.vue?vue&type=template&id=2c07be24&scoped=true& */ "./resources/js/Pages/Principal/PrincipalPhoto.vue?vue&type=template&id=2c07be24&scoped=true&");
/* harmony import */ var _PrincipalPhoto_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PrincipalPhoto.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Principal/PrincipalPhoto.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _PrincipalPhoto_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _PrincipalPhoto_vue_vue_type_template_id_2c07be24_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _PrincipalPhoto_vue_vue_type_template_id_2c07be24_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "2c07be24",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Principal/PrincipalPhoto.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Principal/PrincipalPhoto.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/Pages/Principal/PrincipalPhoto.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PrincipalPhoto_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./PrincipalPhoto.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Principal/PrincipalPhoto.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PrincipalPhoto_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/Pages/Principal/PrincipalPhoto.vue?vue&type=template&id=2c07be24&scoped=true&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/Pages/Principal/PrincipalPhoto.vue?vue&type=template&id=2c07be24&scoped=true& ***!
  \****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PrincipalPhoto_vue_vue_type_template_id_2c07be24_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PrincipalPhoto_vue_vue_type_template_id_2c07be24_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PrincipalPhoto_vue_vue_type_template_id_2c07be24_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./PrincipalPhoto.vue?vue&type=template&id=2c07be24&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Principal/PrincipalPhoto.vue?vue&type=template&id=2c07be24&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Principal/PrincipalPhoto.vue?vue&type=template&id=2c07be24&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Principal/PrincipalPhoto.vue?vue&type=template&id=2c07be24&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        {
          staticClass: "block",
          on: {
            click: function($event) {
              _vm.show = !_vm.show
            }
          }
        },
        [
          _c("b-img-lazy", {
            staticClass: "bg-brand-100  focus:outline-none cursor-pointer",
            class: _vm.classes,
            attrs: { src: _vm.dataTypeChecked.photo }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "b-modal",
        {
          attrs: {
            title: _vm.fullname,
            centered: "",
            "ok-only": "",
            "hide-footer": ""
          },
          model: {
            value: _vm.show,
            callback: function($$v) {
              _vm.show = $$v
            },
            expression: "show"
          }
        },
        [
          _c(
            "div",
            { staticClass: "relative" },
            [
              _c("b-progress", {
                staticClass: "relative top-0 left-0 z-50",
                attrs: {
                  value: _vm.progress,
                  max: 100,
                  "show-progress": "",
                  animated: ""
                }
              }),
              _vm._v(" "),
              _vm.image
                ? _c(
                    "div",
                    {
                      staticClass:
                        "  w-full h-full bg-brand-200  object-fill z-20",
                      style:
                        "background-image: url(" +
                        _vm.image +
                        "); background-size: cover;"
                    },
                    [
                      _c("img", {
                        staticClass:
                          "w-full h-auto rounded bg-brand-200 focus:outline-none z-10 object-cover",
                        attrs: { src: _vm.image }
                      })
                    ]
                  )
                : _c("b-img-lazy", {
                    staticClass:
                      " w-full h-auto rounded bg-brand-200 focus:outline-none z-10",
                    attrs: { src: _vm.dataTypeChecked.photo }
                  })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "b-form",
            {
              staticClass: "mx-auto",
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  return _vm.submit($event)
                }
              }
            },
            [
              _c(
                "b-form-invalid-feedback",
                {
                  staticClass: "my-3 font-bold",
                  attrs: { state: !_vm.form.hasErrors }
                },
                [
                  _vm._v(
                    "\n        " + _vm._s(_vm.form.errors.file) + "\n      "
                  )
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "bg-brand-200" }, [
                _c(
                  "div",
                  { staticClass: "flex justify-between items-center " },
                  [
                    _c(
                      "div",
                      [
                        _c("b-form-file", {
                          ref: "pickfile",
                          staticClass: "m-2 w-full",
                          attrs: {
                            type: "file",
                            accept: ".jpg, .png, .gif",
                            placeholder: "Pick a Passport Photo",
                            capture: ""
                          },
                          on: { change: _vm.picked }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      [
                        _c(
                          "b-button",
                          {
                            staticClass: "m-2",
                            attrs: { variant: "primary", type: "submit" }
                          },
                          [_vm._v("Save")]
                        )
                      ],
                      1
                    )
                  ]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "m-3 text-xs" }, [
                _vm._v(
                  "\n        Please note that image would be cropped to 413px by 531px\n      "
                )
              ])
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);