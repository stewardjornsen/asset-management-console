(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Oldasset_OldassetView_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _components_StxViewItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/components/StxViewItem */ "./resources/js/components/StxViewItem.vue");
/* harmony import */ var _vue_composition_api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @vue/composition-api */ "./node_modules/@vue/composition-api/dist/vue-composition-api.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,_vue_composition_api__WEBPACK_IMPORTED_MODULE_1__.defineComponent)({
  props: {
    activeData: {
      type: Object,
      required: true
    }
  },
  components: {
    StxViewItem: _components_StxViewItem__WEBPACK_IMPORTED_MODULE_0__.default
  }
}));

/***/ }),

/***/ "./resources/js/Pages/Oldasset/OldassetView.vue":
/*!******************************************************!*\
  !*** ./resources/js/Pages/Oldasset/OldassetView.vue ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _OldassetView_vue_vue_type_template_id_0f35eadb_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./OldassetView.vue?vue&type=template&id=0f35eadb&scoped=true& */ "./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=template&id=0f35eadb&scoped=true&");
/* harmony import */ var _OldassetView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./OldassetView.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _OldassetView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _OldassetView_vue_vue_type_template_id_0f35eadb_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _OldassetView_vue_vue_type_template_id_0f35eadb_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "0f35eadb",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Oldasset/OldassetView.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OldassetView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./OldassetView.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OldassetView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=template&id=0f35eadb&scoped=true&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=template&id=0f35eadb&scoped=true& ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OldassetView_vue_vue_type_template_id_0f35eadb_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OldassetView_vue_vue_type_template_id_0f35eadb_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OldassetView_vue_vue_type_template_id_0f35eadb_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./OldassetView.vue?vue&type=template&id=0f35eadb&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=template&id=0f35eadb&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=template&id=0f35eadb&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=template&id=0f35eadb&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "flex justify-between pt-4 mt-2" }, [
      _c("div", [
        _vm._v("\n            PHOTO:\n            "),
        _c("div", [_vm._v(_vm._s(_vm.activeData.name))])
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "flex justify-between border-t border-gray-200 pt-4 mt-2"
      },
      [
        _c("div", [
          _vm._v("\n            Description:\n            "),
          _c("div", { staticClass: "font-poppins font-bold" }, [
            _vm._v(
              "\n              " +
                _vm._s(_vm.activeData.description) +
                "\n            "
            )
          ])
        ]),
        _vm._v(" "),
        _c(
          "div",
          [
            _c("stx-view-item", { attrs: { title: "Serial No:" } }, [
              _vm._v(_vm._s(_vm.activeData.serial_no))
            ]),
            _vm._v(" "),
            _c(
              "stx-view-item",
              { staticClass: "mt-2", attrs: { title: "Model No:" } },
              [_vm._v(_vm._s(_vm.activeData.model_no))]
            )
          ],
          1
        )
      ]
    ),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "flex justify-between border-t border-gray-200 pt-4 mt-2"
      },
      [
        _c("stx-view-item", { attrs: { title: "Manufactured At:" } }, [
          _vm._v(_vm._s(_vm.activeData.manufactured_at))
        ]),
        _vm._v(" "),
        _c("stx-view-item", { attrs: { title: "Warantee Start:" } }, [
          _vm._v(_vm._s(_vm.activeData.warantee_start))
        ]),
        _vm._v(" "),
        _c("stx-view-item", { attrs: { title: " Warantee End:" } }, [
          _vm._v(_vm._s(_vm.activeData.warantee_end) + "\n          ")
        ]),
        _vm._v(" "),
        _c("stx-view-item", { attrs: { title: " Service Due Date:" } }, [
          _vm._v(_vm._s(_vm.activeData.service_due_at))
        ])
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "flex justify-between border-t border-gray-200 pt-4 mt-2"
      },
      [
        _c("stx-view-item", { attrs: { title: "Purchased At:" } }, [
          _vm._v(_vm._s(_vm.activeData.purchased_at))
        ]),
        _vm._v(" "),
        _c("stx-view-item", { attrs: { title: "Status:" } }, [
          _vm._v(_vm._s(_vm.activeData.status))
        ]),
        _vm._v(" "),
        _c("stx-view-item", { attrs: { title: "Location:" } }, [
          _vm._v(_vm._s(_vm.activeData.location_id))
        ]),
        _vm._v(" "),
        _c("stx-view-item", { attrs: { title: "Base Location:" } }, [
          _vm._v(_vm._s(_vm.activeData.base_location))
        ])
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "flex justify-between border-t border-gray-200 pt-4 mt-2"
      },
      [
        _c("stx-view-item", { attrs: { title: "Year of Construction:" } }, [
          _vm._v(_vm._s(_vm.activeData.year_of_construction))
        ]),
        _vm._v(" "),
        _c("stx-view-item", { attrs: { title: "Country:" } }, [
          _vm._v(_vm._s(_vm.activeData.country))
        ]),
        _vm._v(" "),
        _c("stx-view-item", { attrs: { title: "Purchase Amount:" } }, [
          _vm._v(_vm._s(_vm.activeData.purchase_amount))
        ]),
        _vm._v(" "),
        _c("stx-view-item", { attrs: { title: "Currency:" } }, [
          _vm._v(_vm._s(_vm.activeData.purchase_currency))
        ])
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "flex justify-between border-t border-gray-200 pt-4 mt-2"
      },
      [
        _c("stx-view-item", { attrs: { title: "Note:" } }, [
          _vm._v(_vm._s(_vm.activeData.note))
        ])
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);