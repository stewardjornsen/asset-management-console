(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Vendor_VendorView_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Vendor/VendorView.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Vendor/VendorView.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _components_StxViewItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/components/StxViewItem */ "./resources/js/components/StxViewItem.vue");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _vue_composition_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @vue/composition-api */ "./node_modules/@vue/composition-api/dist/vue-composition-api.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,_vue_composition_api__WEBPACK_IMPORTED_MODULE_2__.defineComponent)({
  props: {
    activeData: {
      type: Object,
      required: true
    }
  },
  components: {
    StxViewItem: _components_StxViewItem__WEBPACK_IMPORTED_MODULE_0__.default
  },
  data: function data() {
    return {
      busy: false,
      form: this.$inertia.form({// id: -1,
        // name: "",
        // short_name: "",
        // address: "",
        // city: "Victoria Island",
        // state: "Lagos",
        // country: "NG",
        // email: "",
        // mobile: "",
        // phone_1: "",
        // phone_2: "",
        // fax: "",
        // postal_zip: "",
        // category: "",
        // is_operating: 1
      })
    };
  },
  mounted: function mounted() {
    this.loadDefaults();
  },
  computed: {
    data: function data() {
      return this.activeData;
    }
  },
  methods: {
    showToast: function showToast(title, icon) {
      sweetalert2__WEBPACK_IMPORTED_MODULE_1___default().fire({
        position: "top-right",
        icon: icon,
        title: title,
        showConfirmButton: false,
        toast: true,
        timer: 2500
      });
    },
    saveChanges: function saveChanges() {
      var _this = this;

      var response = {
        preserveScroll: true,
        errorBag: "bag",
        onStart: function onStart() {
          console.log("Started");
          _this.busy = true;
        },
        onSuccess: function onSuccess(res) {
          console.log("RESPONSE", res);

          _this.form.reset();

          _this.showToast("Record has been added successfully", "success");
        },
        onError: function onError(err) {
          console.log("THERE WAS AN ERROR", err);

          _this.showToast("An error occurred!", "error");
        },
        onFinish: function onFinish() {
          console.log("Finished");
          _this.busy = false;
        }
      };

      if (this.form.id > 0) {
        this.form.put(route("vendors.update", this.form.id), response);
      } else {
        this.form.post(route("vendors.store"), response);
      }
    },
    loadDefaults: function loadDefaults() {
      //   this.form = {
      this.form.id = this.activeData.id || "-1", this.form.name = this.activeData.name || "", this.form.contact_person = this.activeData.contact_person || "", this.form.address = this.activeData.address || "", this.form.city = this.activeData.city || "", this.form.state = this.activeData.state || "", this.form.country = this.activeData.country || "NG", this.form.email = this.activeData.email || "", this.form.mobile = this.activeData.mobile || "", this.form.phone = this.activeData.phone || "", // (this.form.phone_2 = this.activeData.phone_2 || ""),
      this.form.website = this.activeData.website || "", this.form.postal_zip = this.activeData.postal_zip || "", this.form.notes = this.activeData.notes || "", this.form.status = this.activeData.status || "Active"; //   };
    }
  }
}));

/***/ }),

/***/ "./resources/js/Pages/Vendor/VendorView.vue":
/*!**************************************************!*\
  !*** ./resources/js/Pages/Vendor/VendorView.vue ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _VendorView_vue_vue_type_template_id_12c09a0a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./VendorView.vue?vue&type=template&id=12c09a0a&scoped=true& */ "./resources/js/Pages/Vendor/VendorView.vue?vue&type=template&id=12c09a0a&scoped=true&");
/* harmony import */ var _VendorView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./VendorView.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Vendor/VendorView.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _VendorView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _VendorView_vue_vue_type_template_id_12c09a0a_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _VendorView_vue_vue_type_template_id_12c09a0a_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "12c09a0a",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Vendor/VendorView.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Vendor/VendorView.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/Pages/Vendor/VendorView.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_VendorView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./VendorView.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Vendor/VendorView.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_VendorView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/Pages/Vendor/VendorView.vue?vue&type=template&id=12c09a0a&scoped=true&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/Pages/Vendor/VendorView.vue?vue&type=template&id=12c09a0a&scoped=true& ***!
  \*********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VendorView_vue_vue_type_template_id_12c09a0a_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VendorView_vue_vue_type_template_id_12c09a0a_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VendorView_vue_vue_type_template_id_12c09a0a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./VendorView.vue?vue&type=template&id=12c09a0a&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Vendor/VendorView.vue?vue&type=template&id=12c09a0a&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Vendor/VendorView.vue?vue&type=template&id=12c09a0a&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Vendor/VendorView.vue?vue&type=template&id=12c09a0a&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "bg-white rounded-2xl p-5 mt-4 shadow-md" }, [
    _c(
      "div",
      { staticClass: "px-2 py-6 mb-10 bg-white max-w-4xl mx-auto" },
      [
        _c(
          "h2",
          [
            _vm._v("\n      Vendor:\n      "),
            !_vm.route().current("vendors.show")
              ? _c(
                  "inertia-link",
                  { attrs: { href: _vm.route("vendors.show", _vm.data.id) } },
                  [
                    _vm._v("\n        " + _vm._s(_vm.data.name) + "\n        "),
                    _c("b-icon", {
                      staticClass: "ml-2",
                      attrs: { icon: "box-arrow-up-right" }
                    })
                  ],
                  1
                )
              : _c("span", { staticClass: "text-gray-400" }, [
                  _vm._v("\n        " + _vm._s(_vm.data.name) + "\n        ")
                ])
          ],
          1
        ),
        _vm._v(" "),
        _c("div", { staticClass: "mb-4 mt-2 border-t border-gray-200" }),
        _vm._v(" "),
        _c("stx-view-item", {
          attrs: { title: "Personal Details", look: "style2" }
        }),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "grid md:grid-cols-3 gap-6" },
          [
            _c("stx-view-item", { attrs: { title: "Name" } }, [
              _vm._v(_vm._s(_vm.data.name))
            ]),
            _vm._v(" "),
            _c("stx-view-item", { attrs: { title: "Contact Person" } }, [
              _vm._v(_vm._s(_vm.data.contact_person))
            ]),
            _vm._v(" "),
            _c("stx-view-item", { attrs: { title: "Status" } }, [
              _vm._v(_vm._s(_vm.data.status))
            ])
          ],
          1
        ),
        _vm._v(" "),
        _c("stx-view-item", {
          attrs: { title: "Contact Details", look: "style2" }
        }),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "grid md:grid-cols-3 gap-6" },
          [
            _c("stx-view-item", { attrs: { title: "Address" } }, [
              _vm._v(_vm._s(_vm.data.address))
            ]),
            _vm._v(" "),
            _c("stx-view-item", { attrs: { title: "City" } }, [
              _vm._v(_vm._s(_vm.data.city))
            ]),
            _vm._v(" "),
            _c("stx-view-item", { attrs: { title: "State" } }, [
              _vm._v(_vm._s(_vm.data.state))
            ]),
            _vm._v(" "),
            _c("stx-view-item", { attrs: { title: "Country" } }, [
              _vm._v(_vm._s(_vm.data.country))
            ]),
            _vm._v(" "),
            _c("stx-view-item", { attrs: { title: "Email" } }, [
              _vm._v(_vm._s(_vm.data.email))
            ]),
            _vm._v(" "),
            _c("stx-view-item", { attrs: { title: "Mobile" } }, [
              _vm._v(_vm._s(_vm.data.mobile))
            ]),
            _vm._v(" "),
            _c("stx-view-item", { attrs: { title: "Phone" } }, [
              _vm._v(_vm._s(_vm.data.phone))
            ])
          ],
          1
        ),
        _vm._v(" "),
        _c("stx-view-item", { attrs: { title: "Status", look: "style2" } }),
        _vm._v(" "),
        _c("stx-view-item", { attrs: { title: "Notes" } }, [
          _vm._v(_vm._s(_vm.data.website))
        ]),
        _vm._v(" "),
        _c("stx-view-item", {
          attrs: { title: "Notes/Remarks/Reports", look: "style2" }
        }),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "grid md:grid-cols-3 gap-6" },
          [
            _c("stx-view-item", { attrs: { title: "Notes" } }, [
              _vm._v(_vm._s(_vm.data.notes))
            ])
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);