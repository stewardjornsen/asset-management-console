(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Location_LocationForm_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Location/LocationForm.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Location/LocationForm.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _components_StxViewItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/components/StxViewItem */ "./resources/js/components/StxViewItem.vue");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _vue_composition_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @vue/composition-api */ "./node_modules/@vue/composition-api/dist/vue-composition-api.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,_vue_composition_api__WEBPACK_IMPORTED_MODULE_2__.defineComponent)({
  props: {
    activeData: {
      type: Object,
      required: true
    }
  },
  components: {
    StxViewItem: _components_StxViewItem__WEBPACK_IMPORTED_MODULE_0__.default
  },
  data: function data() {
    return {
      busy: false,
      form: this.$inertia.form({
        id: -1,
        name: "",
        short_name: "",
        address: "",
        city: "",
        state: "",
        country: "NG",
        email: "",
        mobile: "",
        phone_1: "",
        phone_2: "",
        fax: "",
        postal_zip: "",
        category: "",
        is_operating: 1
      })
    };
  },
  mounted: function mounted() {
    this.loadDefaults();
  },
  computed: {
    data: function data() {
      return this.activeData;
    }
  },
  methods: {
    showToast: function showToast(title, icon) {
      sweetalert2__WEBPACK_IMPORTED_MODULE_1___default().fire({
        position: "top-right",
        icon: icon,
        title: title,
        showConfirmButton: false,
        toast: true,
        timer: 2500
      });
    },
    saveChanges: function saveChanges() {
      var _this = this;

      var response = {
        preserveScroll: true,
        errorBag: "bag",
        onStart: function onStart() {
          console.log("Started");
          _this.busy = true;
        },
        onSuccess: function onSuccess(res) {
          console.log("RESPONSE", res);

          _this.form.reset();

          _this.showToast("Record has been added successfully", "success");
        },
        onError: function onError(err) {
          console.log("THERE WAS AN ERROR", err);

          _this.showToast("An error occurred!", "error");
        },
        onFinish: function onFinish() {
          console.log("Finished");
          _this.busy = false;
        }
      };

      if (this.form.id > 0) {
        this.form.put(route("locations.update", this.form.id), response);
      } else {
        this.form.post(route("locations.store"), response);
      }
    },
    loadDefaults: function loadDefaults() {
      //   this.form = {
      this.form.id = this.activeData.id || "-1", this.form.name = this.activeData.name || "", this.form.short_name = this.activeData.short_name || "", this.form.address = this.activeData.address || "", this.form.city = this.activeData.city || "Victoria Island", this.form.state = this.activeData.state || "Lagos", this.form.country = this.activeData.country || "NG", this.form.email = this.activeData.email || "", this.form.mobile = this.activeData.mobile || "", this.form.phone_1 = this.activeData.phone_1 || "", this.form.phone_2 = this.activeData.phone_2 || "", this.form.fax = this.activeData.fax || "", this.form.postal_zip = this.activeData.postal_zip || "", this.form.category = this.activeData.category || "", this.form.is_operating = this.activeData.is_operating; //   };
    }
  }
}));

/***/ }),

/***/ "./resources/js/Pages/Location/LocationForm.vue":
/*!******************************************************!*\
  !*** ./resources/js/Pages/Location/LocationForm.vue ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _LocationForm_vue_vue_type_template_id_3df532fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./LocationForm.vue?vue&type=template&id=3df532fa&scoped=true& */ "./resources/js/Pages/Location/LocationForm.vue?vue&type=template&id=3df532fa&scoped=true&");
/* harmony import */ var _LocationForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./LocationForm.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Location/LocationForm.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _LocationForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _LocationForm_vue_vue_type_template_id_3df532fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _LocationForm_vue_vue_type_template_id_3df532fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "3df532fa",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Location/LocationForm.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Location/LocationForm.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/Pages/Location/LocationForm.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_LocationForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./LocationForm.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Location/LocationForm.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_LocationForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/Pages/Location/LocationForm.vue?vue&type=template&id=3df532fa&scoped=true&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/Pages/Location/LocationForm.vue?vue&type=template&id=3df532fa&scoped=true& ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LocationForm_vue_vue_type_template_id_3df532fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LocationForm_vue_vue_type_template_id_3df532fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LocationForm_vue_vue_type_template_id_3df532fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./LocationForm.vue?vue&type=template&id=3df532fa&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Location/LocationForm.vue?vue&type=template&id=3df532fa&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Location/LocationForm.vue?vue&type=template&id=3df532fa&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Location/LocationForm.vue?vue&type=template&id=3df532fa&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("module-links", { attrs: { id: _vm.activeData.id } }),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "p-3 mb-10 bg-white rounded-2xl shadow-lg w-full" },
        [
          _c(
            "b-form",
            {
              staticClass: " w-full",
              on: {
                submit: function($event) {
                  $event.stopPropagation()
                  $event.preventDefault()
                  return _vm.saveChanges($event)
                }
              }
            },
            [
              _c(
                "div",
                {
                  staticClass:
                    "fixed top-0 right-0 rounded-bl-xl bg-gray-700 p-4 border-b border-gray-200 w-auto z-50"
                },
                [
                  _c(
                    "button",
                    {
                      staticClass:
                        "rounded bg-brand-500 text-white py-1 px-3 focus:outline-none hover:bg-brand-400 mr-2",
                      attrs: { type: "submit", disabled: _vm.busy }
                    },
                    [_vm._v("\n          Save Changes\n        ")]
                  ),
                  _vm._v(" "),
                  _c(
                    "button",
                    {
                      staticClass:
                        "rounded bg-brand-300 text-white py-1 px-3 focus:outline-none hover:bg-brand-500",
                      attrs: { type: "button", disabled: _vm.busy },
                      on: {
                        click: function($event) {
                          return _vm.loadDefaults()
                        }
                      }
                    },
                    [_vm._v("\n          Reset\n        ")]
                  )
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "flex justify-between  mt-2" }, [
                _c("div", [
                  _c(
                    "div",
                    { staticClass: "text-2xl font-poppins font-bold mt-2" },
                    [
                      _c("span", { staticClass: "font-light" }, [
                        _vm._v(" Location Name:")
                      ])
                    ]
                  )
                ])
              ]),
              _vm._v(" "),
              _c("stx-view-item", {
                attrs: { title: "Description/Status", look: "style2" }
              }),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "grid lg:grid-cols-4 gap-6" },
                [
                  _c(
                    "b-form-group",
                    {
                      staticClass: "col-span-2",
                      attrs: {
                        id: "assetData-group-name",
                        label: "Location Name",
                        "label-for": "___input-name",
                        "invalid-feedback": _vm.form.errors.name,
                        state: !_vm.form.errors.name
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "___input-name",
                          type: "text",
                          state: !_vm.form.errors.name ? null : false
                        },
                        model: {
                          value: _vm.form.name,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "name", $$v)
                          },
                          expression: "form.name"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-form-group",
                    {
                      staticClass: "w-auto",
                      attrs: {
                        id: "assetData-group-short_name",
                        label: "Short Name",
                        "label-for": "___input-short_name",
                        "invalid-feedback": _vm.form.errors.short_name,
                        state: !_vm.form.errors.short_name
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "___input-short_name",
                          type: "text",
                          state: !_vm.form.errors.short_name ? null : false
                        },
                        model: {
                          value: _vm.form.short_name,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "short_name", $$v)
                          },
                          expression: "form.short_name"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-is_operating",
                        label: "Is Operating?",
                        "label-for": "___input-is_operating",
                        "invalid-feedback": _vm.form.errors.is_operating,
                        state: !_vm.form.errors.is_operating
                      }
                    },
                    [
                      _c("b-form-checkbox", {
                        attrs: {
                          id: "___input-is_operating",
                          value: 1,
                          "unchecked-value": 0,
                          state: !_vm.form.errors.is_operating ? null : false
                        },
                        model: {
                          value: _vm.form.is_operating,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "is_operating", $$v)
                          },
                          expression: "form.is_operating"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c("stx-view-item", {
                attrs: { title: "Contact Address", look: "style2" }
              }),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "grid md:grid-cols-2 lg:grid-cols-4 gap-6" },
                [
                  _c(
                    "b-form-group",
                    {
                      attrs: {
                        id: "assetData-group-address",
                        label: "Address",
                        "label-for": "___textarea-address",
                        "invalid-feedback": _vm.form.errors.address,
                        state: !_vm.form.errors.address
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "___input-address",
                          type: "text",
                          state: !_vm.form.errors.address ? null : false
                        },
                        model: {
                          value: _vm.form.address,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "address", $$v)
                          },
                          expression: "form.address"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-city",
                        label: "City",
                        "label-for": "___input-city",
                        "invalid-feedback": _vm.form.errors.city,
                        state: !_vm.form.errors.city
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "___input-city",
                          type: "text",
                          state: !_vm.form.errors.city ? null : false
                        },
                        model: {
                          value: _vm.form.city,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "city", $$v)
                          },
                          expression: "form.city"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-State",
                        label: "State",
                        "label-for": "___input-State",
                        "invalid-feedback": _vm.form.errors.State,
                        state: !_vm.form.errors.State
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "___input-State",
                          type: "text",
                          state: !_vm.form.errors.State ? null : false
                        },
                        model: {
                          value: _vm.form.State,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "State", $$v)
                          },
                          expression: "form.State"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-country",
                        label: "Country",
                        "label-for": "___input-country",
                        "invalid-feedback": _vm.form.errors.country,
                        state: !_vm.form.errors.country
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "___input-country",
                          type: "text",
                          state: !_vm.form.errors.country ? null : false
                        },
                        model: {
                          value: _vm.form.country,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "country", $$v)
                          },
                          expression: "form.country"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c("stx-view-item", {
                attrs: { title: "Communications", look: "style2" }
              }),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "grid md:grid-cols-4 gap-6" },
                [
                  _c(
                    "b-form-group",
                    {
                      staticClass: "w-full col-span-2",
                      attrs: {
                        id: "assetData-group-email",
                        label: "Email",
                        "label-for": "___input-email",
                        "invalid-feedback": _vm.form.errors.email,
                        state: !_vm.form.errors.email
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "___input-email",
                          type: "email",
                          state: !_vm.form.errors.email ? null : false
                        },
                        model: {
                          value: _vm.form.email,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "email", $$v)
                          },
                          expression: "form.email"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-mobile",
                        label: "Mobile",
                        "label-for": "___input-mobile",
                        "invalid-feedback": _vm.form.errors.mobile,
                        state: !_vm.form.errors.mobile
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "___input-mobile",
                          type: "text",
                          state: !_vm.form.errors.mobile ? null : false
                        },
                        model: {
                          value: _vm.form.mobile,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "mobile", $$v)
                          },
                          expression: "form.mobile"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-phone_1",
                        label: "Phone 1",
                        "label-for": "___input-phone_1",
                        "invalid-feedback": _vm.form.errors.phone_1,
                        state: !_vm.form.errors.phone_1
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "___input-phone_1",
                          type: "text",
                          state: !_vm.form.errors.phone_1 ? null : false
                        },
                        model: {
                          value: _vm.form.phone_1,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "phone_1", $$v)
                          },
                          expression: "form.phone_1"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-phone_2",
                        label: "Phone 2",
                        "label-for": "___input-phone_2",
                        "invalid-feedback": _vm.form.errors.phone_2,
                        state: !_vm.form.errors.phone_2
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "___input-phone_2",
                          type: "text",
                          state: !_vm.form.errors.phone_2 ? null : false
                        },
                        model: {
                          value: _vm.form.phone_2,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "phone_2", $$v)
                          },
                          expression: "form.phone_2"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-fax",
                        label: "Fax",
                        "label-for": "___input-fax",
                        "invalid-feedback": _vm.form.errors.fax,
                        state: !_vm.form.errors.fax
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "___input-fax",
                          type: "text",
                          state: !_vm.form.errors.fax ? null : false
                        },
                        model: {
                          value: _vm.form.fax,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "fax", $$v)
                          },
                          expression: "form.fax"
                        }
                      })
                    ],
                    1
                  ),
                  _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-zip_code",
                        label: "Zip/Postal Code",
                        "label-for": "___input-zip_code",
                        "invalid-feedback": _vm.form.errors.zip_code,
                        state: !_vm.form.errors.zip_code
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "___input-zip_code",
                          type: "text",
                          state: !_vm.form.errors.zip_code ? null : false
                        },
                        model: {
                          value: _vm.form.zip_code,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "zip_code", $$v)
                          },
                          expression: "form.zip_code"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c("stx-view-item", {
                attrs: { title: "Group", look: "style2" }
              }),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "grid sm:grid-cols-2 md:grid-cols-3 gap-6" },
                [
                  _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-category",
                        label: "Category",
                        "label-for": "___input-category",
                        "invalid-feedback": _vm.form.errors.category,
                        state: !_vm.form.errors.category
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "___input-category",
                          type: "text",
                          state: !_vm.form.errors.category ? null : false
                        },
                        model: {
                          value: _vm.form.category,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "category", $$v)
                          },
                          expression: "form.category"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);