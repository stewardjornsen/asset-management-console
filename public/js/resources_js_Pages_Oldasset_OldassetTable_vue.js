(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Oldasset_OldassetTable_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetForm.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetForm.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _components_StxViewItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/components/StxViewItem */ "./resources/js/components/StxViewItem.vue");
/* harmony import */ var _vue_composition_api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @vue/composition-api */ "./node_modules/@vue/composition-api/dist/vue-composition-api.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,_vue_composition_api__WEBPACK_IMPORTED_MODULE_1__.defineComponent)({
  props: {
    activeData: {
      type: Object,
      required: true
    }
  },
  components: {
    StxViewItem: _components_StxViewItem__WEBPACK_IMPORTED_MODULE_0__.default
  },
  data: function data() {
    return {
      savedDefaults: {},
      form: this.$inertia.form({
        id: -1,
        name: "",
        location_id: "",
        description: "",
        code: "",
        status: "",
        dimension: "",
        weight: "",
        standard_stock: 0,
        current_stock: 0,
        sub_unit: "",
        stock_status: "",
        items_per_unit: 0,
        sub_items: "",
        manufacturer: "",
        vendor: "",
        service_technician: "",
        country: "Nigeria",
        year_of_construction: 0,
        manufactured_at: "",
        purchased_at: "",
        service_due_at: "",
        warantee_start: "",
        warantee_end: "",
        model_no: "",
        serial_no: "",
        part_no: "",
        purchase_amount: 0,
        purchase_currency: "",
        note: "",
        location_classification: "",
        photo: ""
      })
    };
  },
  mounted: function mounted() {
    this.savedDefaults = this.activeData;
    this.loadDefaults();
  },
  computed: {
    data: function data() {
      return this.activeData;
    }
  },
  methods: {
    saveChanges: function saveChanges() {
      this.form.put(route("asset.update", this.activeData.id));
    },
    loadDefaults: function loadDefaults() {
      //   this.form = {
      this.form.id = this.savedDefaults.id, this.form.name = this.savedDefaults.name, this.form.location_id = this.savedDefaults.location_id, this.form.description = this.savedDefaults.description, this.form.code = this.savedDefaults.code, this.form.status = this.savedDefaults.status, this.form.dimension = this.savedDefaults.dimension, this.form.weight = this.savedDefaults.weight, this.form.standard_stock = this.savedDefaults.standard_stock, this.form.current_stock = this.savedDefaults.current_stock, this.form.sub_unit = this.savedDefaults.sub_unit, this.form.stock_status = this.savedDefaults.stock_status, this.form.items_per_unit = this.savedDefaults.items_per_unit, this.form.sub_items = this.savedDefaults.sub_items, this.form.manufacturer = this.savedDefaults.manufacturer, this.form.vendor = this.savedDefaults.vendor, this.form.service_technician = this.savedDefaults.service_technician, this.form.country = this.savedDefaults.country, this.form.year_of_construction = this.savedDefaults.year_of_construction, this.form.manufactured_at = this.savedDefaults.manufactured_at, this.form.purchased_at = this.savedDefaults.purchased_at, this.form.service_due_at = this.savedDefaults.service_due_at, this.form.warantee_start = this.savedDefaults.warantee_start, this.form.warantee_end = this.savedDefaults.warantee_end, this.form.model_no = this.savedDefaults.model_no, this.form.serial_no = this.savedDefaults.serial_no, this.form.part_no = this.savedDefaults.part_no, this.form.purchase_amount = this.savedDefaults.purchase_amount, this.form.purchase_currency = this.savedDefaults.purchase_currency, this.form.note = this.savedDefaults.note, this.form.location_classification = this.savedDefaults.location_classification, this.form.photo = this.savedDefaults.photo; //   };
    }
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetTable.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetTable.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _vue_composition_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @vue/composition-api */ "./node_modules/@vue/composition-api/dist/vue-composition-api.esm.js");
/* harmony import */ var _components_StxViewItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/components/StxViewItem */ "./resources/js/components/StxViewItem.vue");
/* harmony import */ var _OldassetView__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./OldassetView */ "./resources/js/Pages/Oldasset/OldassetView.vue");
/* harmony import */ var _OldassetForm__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./OldassetForm */ "./resources/js/Pages/Oldasset/OldassetForm.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,_vue_composition_api__WEBPACK_IMPORTED_MODULE_3__.defineComponent)({
  props: {
    assets: Array
  },
  components: {
    StxViewItem: _components_StxViewItem__WEBPACK_IMPORTED_MODULE_0__.default,
    OldassetView: _OldassetView__WEBPACK_IMPORTED_MODULE_1__.default,
    OldassetForm: _OldassetForm__WEBPACK_IMPORTED_MODULE_2__.default
  },
  data: function data() {
    return {
      mode: "edit",
      activeData: {},
      filter: "",
      sortBy: "id",
      sortDesc: true,
      currentPage: 1,
      per_page: 20,
      fields: [{
        key: "action",
        sortable: false
      }, {
        key: "id",
        sortable: true
      }, {
        key: "name",
        label: "Item",
        sortable: true
      }, {
        key: "model_no",
        sortable: true
      }, {
        key: "serial_no",
        sortable: true
      }, {
        key: "dimension",
        sortable: true
      }, {
        key: "location_id",
        label: "Location",
        sortable: true
      }, {
        key: "warantee_start",
        sortable: true
      }, {
        key: "warantee_end",
        sortable: true
      }, {
        key: "service_due_at",
        label: "Service Due",
        sortable: true
      }]
    };
  },
  computed: {
    tableData: function tableData() {
      console.log(JSON.stringify(this.assets.links, null, 2));
      return this.assets;
    }
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _components_StxViewItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/components/StxViewItem */ "./resources/js/components/StxViewItem.vue");
/* harmony import */ var _vue_composition_api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @vue/composition-api */ "./node_modules/@vue/composition-api/dist/vue-composition-api.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,_vue_composition_api__WEBPACK_IMPORTED_MODULE_1__.defineComponent)({
  props: {
    activeData: {
      type: Object,
      required: true
    }
  },
  components: {
    StxViewItem: _components_StxViewItem__WEBPACK_IMPORTED_MODULE_0__.default
  }
}));

/***/ }),

/***/ "./resources/js/Pages/Oldasset/OldassetForm.vue":
/*!******************************************************!*\
  !*** ./resources/js/Pages/Oldasset/OldassetForm.vue ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _OldassetForm_vue_vue_type_template_id_37aad50c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./OldassetForm.vue?vue&type=template&id=37aad50c&scoped=true& */ "./resources/js/Pages/Oldasset/OldassetForm.vue?vue&type=template&id=37aad50c&scoped=true&");
/* harmony import */ var _OldassetForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./OldassetForm.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Oldasset/OldassetForm.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _OldassetForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _OldassetForm_vue_vue_type_template_id_37aad50c_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _OldassetForm_vue_vue_type_template_id_37aad50c_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "37aad50c",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Oldasset/OldassetForm.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Oldasset/OldassetTable.vue":
/*!*******************************************************!*\
  !*** ./resources/js/Pages/Oldasset/OldassetTable.vue ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _OldassetTable_vue_vue_type_template_id_7d38f9f0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./OldassetTable.vue?vue&type=template&id=7d38f9f0&scoped=true& */ "./resources/js/Pages/Oldasset/OldassetTable.vue?vue&type=template&id=7d38f9f0&scoped=true&");
/* harmony import */ var _OldassetTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./OldassetTable.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Oldasset/OldassetTable.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _OldassetTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _OldassetTable_vue_vue_type_template_id_7d38f9f0_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _OldassetTable_vue_vue_type_template_id_7d38f9f0_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "7d38f9f0",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Oldasset/OldassetTable.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Oldasset/OldassetView.vue":
/*!******************************************************!*\
  !*** ./resources/js/Pages/Oldasset/OldassetView.vue ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _OldassetView_vue_vue_type_template_id_0f35eadb_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./OldassetView.vue?vue&type=template&id=0f35eadb&scoped=true& */ "./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=template&id=0f35eadb&scoped=true&");
/* harmony import */ var _OldassetView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./OldassetView.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _OldassetView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _OldassetView_vue_vue_type_template_id_0f35eadb_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _OldassetView_vue_vue_type_template_id_0f35eadb_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "0f35eadb",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Oldasset/OldassetView.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Oldasset/OldassetForm.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/Pages/Oldasset/OldassetForm.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OldassetForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./OldassetForm.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetForm.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OldassetForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/Pages/Oldasset/OldassetTable.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/Pages/Oldasset/OldassetTable.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OldassetTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./OldassetTable.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetTable.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OldassetTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OldassetView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./OldassetView.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OldassetView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/Pages/Oldasset/OldassetForm.vue?vue&type=template&id=37aad50c&scoped=true&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/Pages/Oldasset/OldassetForm.vue?vue&type=template&id=37aad50c&scoped=true& ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OldassetForm_vue_vue_type_template_id_37aad50c_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OldassetForm_vue_vue_type_template_id_37aad50c_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OldassetForm_vue_vue_type_template_id_37aad50c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./OldassetForm.vue?vue&type=template&id=37aad50c&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetForm.vue?vue&type=template&id=37aad50c&scoped=true&");


/***/ }),

/***/ "./resources/js/Pages/Oldasset/OldassetTable.vue?vue&type=template&id=7d38f9f0&scoped=true&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/Pages/Oldasset/OldassetTable.vue?vue&type=template&id=7d38f9f0&scoped=true& ***!
  \**************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OldassetTable_vue_vue_type_template_id_7d38f9f0_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OldassetTable_vue_vue_type_template_id_7d38f9f0_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OldassetTable_vue_vue_type_template_id_7d38f9f0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./OldassetTable.vue?vue&type=template&id=7d38f9f0&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetTable.vue?vue&type=template&id=7d38f9f0&scoped=true&");


/***/ }),

/***/ "./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=template&id=0f35eadb&scoped=true&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=template&id=0f35eadb&scoped=true& ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OldassetView_vue_vue_type_template_id_0f35eadb_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OldassetView_vue_vue_type_template_id_0f35eadb_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_OldassetView_vue_vue_type_template_id_0f35eadb_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./OldassetView.vue?vue&type=template&id=0f35eadb&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=template&id=0f35eadb&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetForm.vue?vue&type=template&id=37aad50c&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetForm.vue?vue&type=template&id=37aad50c&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "p-3 mb-10  w-full" },
    [
      _c(
        "b-form",
        {
          staticClass: " w-full",
          on: {
            submit: function($event) {
              $event.stopPropagation()
              $event.preventDefault()
              return _vm.saveChanges($event)
            }
          }
        },
        [
          _c(
            "div",
            {
              staticClass:
                "fixed top-0 right-0 rounded-bl-xl bg-gray-700 p-4 border-b border-gray-200 w-auto z-50"
            },
            [
              _c(
                "button",
                {
                  staticClass:
                    "rounded bg-brand-500 text-white py-1 px-3 focus:outline-none hover:bg-brand-400 mr-2",
                  attrs: { type: "submit" }
                },
                [_vm._v("\n        Save Changes\n      ")]
              ),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass:
                    "rounded bg-brand-300 text-white py-1 px-3 focus:outline-none hover:bg-brand-500",
                  attrs: { type: "button" },
                  on: {
                    click: function($event) {
                      return _vm.loadDefaults()
                    }
                  }
                },
                [_vm._v("\n        Reset\n      ")]
              )
            ]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "flex justify-between  mt-2" }, [
            _c("div", [
              _c(
                "div",
                { staticClass: "text-2xl font-poppins font-bold mt-2" },
                [
                  _vm._v(
                    "\n          " + _vm._s(_vm.activeData.name) + "\n        "
                  )
                ]
              )
            ])
          ]),
          _vm._v(" "),
          _c("stx-view-item", {
            attrs: { title: "Description", look: "style2" }
          }),
          _vm._v(" "),
          _vm.data && _vm.data.id
            ? _c(
                "b-form-group",
                {
                  attrs: {
                    id: "assetData-group-name",
                    label: "Item Name",
                    "label-for": "assetData_input-name",
                    "invalid-feedback": _vm.form.errors.name,
                    state: !_vm.form.errors.name
                  }
                },
                [
                  _c("b-form-input", {
                    attrs: {
                      id: "assetData_input-name",
                      type: "text",
                      state: !_vm.form.errors.name ? null : false
                    },
                    model: {
                      value: _vm.form.name,
                      callback: function($$v) {
                        _vm.$set(_vm.form, "name", $$v)
                      },
                      expression: "form.name"
                    }
                  })
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "grid lg:grid-cols-3 gap-6" },
            [
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "col-span-2",
                      attrs: {
                        id: "assetData-group-description",
                        label: "Item description",
                        "label-for": "assetData_textarea-description",
                        "invalid-feedback": _vm.form.errors.description,
                        state: !_vm.form.errors.description
                      }
                    },
                    [
                      _c("b-form-textarea", {
                        attrs: {
                          id: "assetData_textarea-description",
                          type: "text",
                          rows: "4",
                          state: !_vm.form.errors.description ? null : false
                        },
                        model: {
                          value: _vm.form.description,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "description", $$v)
                          },
                          expression: "form.description"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-auto",
                      attrs: {
                        id: "assetData-group-location_id",
                        label: "Item location_id",
                        "label-for": "assetData_input-location_id",
                        "invalid-feedback": _vm.form.errors.location_id,
                        state: !_vm.form.errors.location_id
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-location_id",
                          type: "text",
                          state: !_vm.form.errors.location_id ? null : false
                        },
                        model: {
                          value: _vm.form.location_id,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "location_id", $$v)
                          },
                          expression: "form.location_id"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e()
            ],
            1
          ),
          _vm._v(" "),
          _c("stx-view-item", {
            attrs: { title: "Measurements", look: "style2" }
          }),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "grid md:grid-cols-2 lg:grid-cols-4 gap-6" },
            [
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-code",
                        label: "Item code",
                        "label-for": "assetData_input-code",
                        "invalid-feedback": _vm.form.errors.code,
                        state: !_vm.form.errors.code
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-code",
                          type: "text",
                          state: !_vm.form.errors.code ? null : false
                        },
                        model: {
                          value: _vm.form.code,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "code", $$v)
                          },
                          expression: "form.code"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-status",
                        label: "Item status",
                        "label-for": "assetData_input-status",
                        "invalid-feedback": _vm.form.errors.status,
                        state: !_vm.form.errors.status
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-status",
                          type: "text",
                          state: !_vm.form.errors.status ? null : false
                        },
                        model: {
                          value: _vm.form.status,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "status", $$v)
                          },
                          expression: "form.status"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-dimension",
                        label: "Item dimension",
                        "label-for": "assetData_input-dimension",
                        "invalid-feedback": _vm.form.errors.dimension,
                        state: !_vm.form.errors.dimension
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-dimension",
                          type: "text",
                          state: !_vm.form.errors.dimension ? null : false
                        },
                        model: {
                          value: _vm.form.dimension,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "dimension", $$v)
                          },
                          expression: "form.dimension"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-weight",
                        label: "Item weight",
                        "label-for": "assetData_input-weight",
                        "invalid-feedback": _vm.form.errors.weight,
                        state: !_vm.form.errors.weight
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-weight",
                          type: "text",
                          state: !_vm.form.errors.weight ? null : false
                        },
                        model: {
                          value: _vm.form.weight,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "weight", $$v)
                          },
                          expression: "form.weight"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e()
            ],
            1
          ),
          _vm._v(" "),
          _c("stx-view-item", {
            attrs: { title: "Item Details", look: "style2" }
          }),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "grid md:grid-cols-2 gap-6" },
            [
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-manufacturer",
                        label: "Manufacturer",
                        "label-for": "assetData_input-manufacturer",
                        "invalid-feedback": _vm.form.errors.manufacturer,
                        state: !_vm.form.errors.manufacturer
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-manufacturer",
                          type: "text",
                          state: !_vm.form.errors.manufacturer ? null : false
                        },
                        model: {
                          value: _vm.form.manufacturer,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "manufacturer", $$v)
                          },
                          expression: "form.manufacturer"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-manufactured_at",
                        label: "Manufactured Date",
                        "label-for": "assetData_input-manufactured_at",
                        "invalid-feedback": _vm.form.errors.manufactured_at,
                        state: !_vm.form.errors.manufactured_at
                      }
                    },
                    [
                      _c("date-picker", {
                        staticClass: "w-full",
                        class: {
                          "border-2 rounded border-red-500": !!_vm.form.errors
                            .manufactured_at
                        },
                        attrs: { valueType: "format" },
                        model: {
                          value: _vm.form.manufactured_at,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "manufactured_at", $$v)
                          },
                          expression: "form.manufactured_at"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-model_no",
                        label: "Model no",
                        "label-for": "assetData_input-model_no",
                        "invalid-feedback": _vm.form.errors.model_no,
                        state: !_vm.form.errors.model_no
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-model_no",
                          type: "text",
                          state: !_vm.form.errors.model_no ? null : false
                        },
                        model: {
                          value: _vm.form.model_no,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "model_no", $$v)
                          },
                          expression: "form.model_no"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-serial_no",
                        label: "Serial no",
                        "label-for": "assetData_input-serial_no",
                        "invalid-feedback": _vm.form.errors.serial_no,
                        state: !_vm.form.errors.serial_no
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-serial_no",
                          type: "text",
                          state: !_vm.form.errors.serial_no ? null : false
                        },
                        model: {
                          value: _vm.form.serial_no,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "serial_no", $$v)
                          },
                          expression: "form.serial_no"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-part_no",
                        label: "Part no",
                        "label-for": "assetData_input-part_no",
                        "invalid-feedback": _vm.form.errors.part_no,
                        state: !_vm.form.errors.part_no
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-part_no",
                          type: "text",
                          state: !_vm.form.errors.part_no ? null : false
                        },
                        model: {
                          value: _vm.form.part_no,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "part_no", $$v)
                          },
                          expression: "form.part_no"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e()
            ],
            1
          ),
          _vm._v(" "),
          _c("stx-view-item", { attrs: { title: "Stock", look: "style2" } }),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "grid sm:grid-cols-2 md:grid-cols-3 gap-6" },
            [
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-standard_stock",
                        label: "Standard Stock",
                        "label-for": "assetData_input-standard_stock",
                        "invalid-feedback": _vm.form.errors.standard_stock,
                        state: !_vm.form.errors.standard_stock
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-standard_stock",
                          type: "text",
                          state: !_vm.form.errors.standard_stock ? null : false
                        },
                        model: {
                          value: _vm.form.standard_stock,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "standard_stock", $$v)
                          },
                          expression: "form.standard_stock"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-current_stock",
                        label: "Current Stock",
                        "label-for": "assetData_input-current_stock",
                        "invalid-feedback": _vm.form.errors.current_stock,
                        state: !_vm.form.errors.current_stock
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-current_stock",
                          type: "text",
                          state: !_vm.form.errors.current_stock ? null : false
                        },
                        model: {
                          value: _vm.form.current_stock,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "current_stock", $$v)
                          },
                          expression: "form.current_stock"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-sub_unit",
                        label: "Item Unit (db:sub_unit)",
                        "label-for": "assetData_input-sub_unit",
                        "invalid-feedback": _vm.form.errors.sub_unit,
                        state: !_vm.form.errors.sub_unit
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-sub_unit",
                          type: "text",
                          state: !_vm.form.errors.sub_unit ? null : false
                        },
                        model: {
                          value: _vm.form.sub_unit,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "sub_unit", $$v)
                          },
                          expression: "form.sub_unit"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e()
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "grid sm:grid-cols-2 md:grid-cols-3 gap-6" },
            [
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-sub_items",
                        label: "Sub Item Name/Unit",
                        "label-for": "assetData_input-sub_items",
                        "invalid-feedback": _vm.form.errors.sub_items,
                        state: !_vm.form.errors.sub_items
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-sub_items",
                          type: "text",
                          state: !_vm.form.errors.sub_items ? null : false
                        },
                        model: {
                          value: _vm.form.sub_items,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "sub_items", $$v)
                          },
                          expression: "form.sub_items"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-items_per_unit",
                        label: "Sub Item Max Quantity (db:items_per_unit)",
                        "label-for": "assetData_input-items_per_unit",
                        "invalid-feedback": _vm.form.errors.items_per_unit,
                        state: !_vm.form.errors.items_per_unit
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-items_per_unit",
                          type: "text",
                          state: !_vm.form.errors.items_per_unit ? null : false
                        },
                        model: {
                          value: _vm.form.items_per_unit,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "items_per_unit", $$v)
                          },
                          expression: "form.items_per_unit"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-stock_status",
                        label: "Sub item Current Stock (db:stock_status)",
                        "label-for": "assetData_input-stock_status",
                        "invalid-feedback": _vm.form.errors.stock_status,
                        state: !_vm.form.errors.stock_status
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-stock_status",
                          type: "text",
                          state: !_vm.form.errors.stock_status ? null : false
                        },
                        model: {
                          value: _vm.form.stock_status,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "stock_status", $$v)
                          },
                          expression: "form.stock_status"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e()
            ],
            1
          ),
          _vm._v(" "),
          _c("stx-view-item", {
            attrs: { title: "Purchase Details", look: "style2" }
          }),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "grid md:grid-cols-3 gap-6" },
            [
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-purchased_at",
                        label: "Purchase Date",
                        "label-for": "assetData_input-purchased_at",
                        "invalid-feedback": _vm.form.errors.purchased_at,
                        state: !_vm.form.errors.purchased_at
                      }
                    },
                    [
                      _c("date-picker", {
                        staticClass: "w-full",
                        class: {
                          "border-2 rounded border-red-500": !!_vm.form.errors
                            .purchased_at
                        },
                        attrs: { valueType: "format" },
                        model: {
                          value: _vm.form.purchased_at,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "purchased_at", $$v)
                          },
                          expression: "form.purchased_at"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-purchase_amount",
                        label: "Purchase Amount",
                        "label-for": "assetData_input-purchase_amount",
                        "invalid-feedback": _vm.form.errors.purchase_amount,
                        state: !_vm.form.errors.purchase_amount
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-purchase_amount",
                          type: "text",
                          state: !_vm.form.errors.purchase_amount ? null : false
                        },
                        model: {
                          value: _vm.form.purchase_amount,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "purchase_amount", $$v)
                          },
                          expression: "form.purchase_amount"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-purchase_currency",
                        label: "Purchase Currency",
                        "label-for": "assetData_input-purchase_currency",
                        "invalid-feedback": _vm.form.errors.purchase_currency,
                        state: !_vm.form.errors.purchase_currency
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-purchase_currency",
                          type: "text",
                          state: !_vm.form.errors.purchase_currency
                            ? null
                            : false
                        },
                        model: {
                          value: _vm.form.purchase_currency,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "purchase_currency", $$v)
                          },
                          expression: "form.purchase_currency"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e()
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "grid sm:grid-cols-3 md:grid-cols-4 gap-6" },
            [
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full sm:col-span-3 md:col-span-2",
                      attrs: {
                        id: "assetData-group-vendor",
                        label: "Vendor",
                        "label-for": "assetData_input-vendor",
                        "invalid-feedback": _vm.form.errors.vendor,
                        state: !_vm.form.errors.vendor
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-vendor",
                          type: "text",
                          state: !_vm.form.errors.vendor ? null : false
                        },
                        model: {
                          value: _vm.form.vendor,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "vendor", $$v)
                          },
                          expression: "form.vendor"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full sm:col-span-2",
                      attrs: {
                        id: "assetData-group-country",
                        label: "Country",
                        "label-for": "assetData_input-country",
                        "invalid-feedback": _vm.form.errors.country,
                        state: !_vm.form.errors.country
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-country",
                          type: "text",
                          state: !_vm.form.errors.country ? null : false
                        },
                        model: {
                          value: _vm.form.country,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "country", $$v)
                          },
                          expression: "form.country"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-year_of_construction",
                        label: "Production Year",
                        "label-for": "assetData_input-year_of_construction",
                        "invalid-feedback":
                          _vm.form.errors.year_of_construction,
                        state: !_vm.form.errors.year_of_construction
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-year_of_construction",
                          type: "text",
                          state: !_vm.form.errors.year_of_construction
                            ? null
                            : false
                        },
                        model: {
                          value: _vm.form.year_of_construction,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "year_of_construction", $$v)
                          },
                          expression: "form.year_of_construction"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e()
            ],
            1
          ),
          _vm._v(" "),
          _c("stx-view-item", {
            attrs: { title: "Maintenance Details", look: "style2" }
          }),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "grid md:grid-cols-2 gap-6" },
            [
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-service_technician",
                        label: "Service Technician",
                        "label-for": "assetData_input-service_technician",
                        "invalid-feedback": _vm.form.errors.service_technician,
                        state: !_vm.form.errors.service_technician
                      }
                    },
                    [
                      _c("b-form-input", {
                        attrs: {
                          id: "assetData_input-service_technician",
                          type: "text",
                          state: !_vm.form.errors.service_technician
                            ? null
                            : false
                        },
                        model: {
                          value: _vm.form.service_technician,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "service_technician", $$v)
                          },
                          expression: "form.service_technician"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-service_due_at",
                        label: "Service Due",
                        "label-for": "assetData_input-service_due_at",
                        "invalid-feedback": _vm.form.errors.service_due_at,
                        state: !_vm.form.errors.service_due_at
                      }
                    },
                    [
                      _c("date-picker", {
                        staticClass: "w-full",
                        class: {
                          "border-2 rounded border-red-500": !!_vm.form.errors
                            .service_due_at
                        },
                        attrs: { valueType: "format" },
                        model: {
                          value: _vm.form.service_due_at,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "service_due_at", $$v)
                          },
                          expression: "form.service_due_at"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e()
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "grid md:grid-cols-2 gap-6" },
            [
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-warantee_start",
                        label: "Warantee Starts",
                        "label-for": "assetData_input-warantee_start",
                        "invalid-feedback": _vm.form.errors.warantee_start,
                        state: !_vm.form.errors.warantee_start
                      }
                    },
                    [
                      _c("date-picker", {
                        staticClass: "w-full",
                        class: {
                          "border-2 rounded border-red-500": !!_vm.form.errors
                            .warantee_start
                        },
                        attrs: { valueType: "format" },
                        model: {
                          value: _vm.form.warantee_start,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "warantee_start", $$v)
                          },
                          expression: "form.warantee_start"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.data && _vm.data.id
                ? _c(
                    "b-form-group",
                    {
                      staticClass: "w-full",
                      attrs: {
                        id: "assetData-group-warantee_end",
                        label: "Warantee Ends",
                        "label-for": "assetData_input-warantee_end",
                        "invalid-feedback": _vm.form.errors.warantee_end,
                        state: !_vm.form.errors.warantee_end
                      }
                    },
                    [
                      _c("date-picker", {
                        staticClass: "w-full",
                        class: {
                          "border-2 rounded border-red-500": !!_vm.form.errors
                            .warantee_end
                        },
                        attrs: { valueType: "format" },
                        model: {
                          value: _vm.form.warantee_end,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "warantee_end", $$v)
                          },
                          expression: "form.warantee_end"
                        }
                      })
                    ],
                    1
                  )
                : _vm._e()
            ],
            1
          ),
          _vm._v(" "),
          _c("stx-view-item", { attrs: { title: "Extras", look: "style2" } }),
          _vm._v(" "),
          _vm.data && _vm.data.id
            ? _c(
                "b-form-group",
                {
                  staticClass: "w-full",
                  attrs: {
                    id: "assetData-group-note",
                    label: "Additional Notes",
                    "label-for": "assetData_textarea-note",
                    "invalid-feedback": _vm.form.errors.note,
                    state: !_vm.form.errors.note
                  }
                },
                [
                  _c("b-form-textarea", {
                    attrs: {
                      id: "assetData_textarea-note",
                      type: "text",
                      rows: "4",
                      state: !_vm.form.errors.note ? null : false
                    },
                    model: {
                      value: _vm.form.note,
                      callback: function($$v) {
                        _vm.$set(_vm.form, "note", $$v)
                      },
                      expression: "form.note"
                    }
                  })
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.data && _vm.data.id
            ? _c(
                "b-form-group",
                {
                  staticClass: "w-full",
                  attrs: {
                    id: "assetData-group-location_classification",
                    label: "Location Classification",
                    "label-for": "assetData_input-location_classification",
                    "invalid-feedback": _vm.form.errors.location_classification,
                    state: !_vm.form.errors.location_classification
                  }
                },
                [
                  _c("b-form-input", {
                    attrs: {
                      id: "assetData_input-location_classification",
                      type: "text",
                      state: !_vm.form.errors.location_classification
                        ? null
                        : false
                    },
                    model: {
                      value: _vm.form.location_classification,
                      callback: function($$v) {
                        _vm.$set(_vm.form, "location_classification", $$v)
                      },
                      expression: "form.location_classification"
                    }
                  })
                ],
                1
              )
            : _vm._e()
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetTable.vue?vue&type=template&id=7d38f9f0&scoped=true&":
/*!*****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetTable.vue?vue&type=template&id=7d38f9f0&scoped=true& ***!
  \*****************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("h3", { staticClass: "text-xl" }, [_vm._v("Asset Register")]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "form-group" },
      [
        _c("label", { attrs: { for: "" } }),
        _vm._v(" "),
        _c(
          "b-input-group",
          {
            attrs: {
              size: "md",
              prepend: "Filter",
              append: "Records: " + (_vm.tableData.length || 0)
            }
          },
          [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.filter,
                  expression: "filter"
                }
              ],
              staticClass: "form-control",
              attrs: {
                type: "text",
                "aria-describedby": "helpId",
                placeholder: "",
                prepend: "search"
              },
              domProps: { value: _vm.filter },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.filter = $event.target.value
                }
              }
            })
          ]
        ),
        _vm._v(" "),
        _c(
          "small",
          { staticClass: "form-text text-muted", attrs: { id: "helpId" } },
          [_vm._v("Filter Records")]
        )
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "bg-white p-3" },
      [
        _c("b-pagination", {
          attrs: {
            "total-rows": _vm.tableData.length || 0,
            "per-page": _vm.per_page,
            "aria-controls": "my-table",
            align: "right"
          },
          model: {
            value: _vm.currentPage,
            callback: function($$v) {
              _vm.currentPage = $$v
            },
            expression: "currentPage"
          }
        }),
        _vm._v(" "),
        _c("b-table", {
          staticClass: "text-sm",
          attrs: {
            stacked: "md",
            items: _vm.tableData,
            fields: _vm.fields,
            "sort-by": _vm.sortBy,
            "sort-desc": _vm.sortDesc,
            responsive: "xl",
            "primary-key": "id",
            "current-page": _vm.currentPage,
            "per-page": _vm.per_page,
            filter: _vm.filter,
            striped: "",
            bordered: "",
            selectMode: "single"
          },
          on: {
            "update:sortBy": function($event) {
              _vm.sortBy = $event
            },
            "update:sort-by": function($event) {
              _vm.sortBy = $event
            },
            "update:sortDesc": function($event) {
              _vm.sortDesc = $event
            },
            "update:sort-desc": function($event) {
              _vm.sortDesc = $event
            }
          },
          scopedSlots: _vm._u([
            {
              key: "cell(action)",
              fn: function(data) {
                return [
                  _c(
                    "div",
                    { staticClass: "whitespace-nowrap" },
                    [
                      _c(
                        "b-button",
                        {
                          directives: [
                            {
                              name: "b-modal",
                              rawName: "v-b-modal.stxviewmodal",
                              modifiers: { stxviewmodal: true }
                            }
                          ],
                          staticClass: "mr-2",
                          attrs: { pill: "", variant: "secondary", size: "sm" },
                          on: {
                            click: function($event) {
                              _vm.mode = "view"
                              _vm.activeData = data.item
                            }
                          }
                        },
                        [
                          _c("b-icon", {
                            staticClass: "mr-1",
                            attrs: { icon: "eye" }
                          }),
                          _vm._v(" View")
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "b-button",
                        {
                          directives: [
                            {
                              name: "b-modal",
                              rawName: "v-b-modal.stxviewmodal",
                              modifiers: { stxviewmodal: true }
                            }
                          ],
                          attrs: { pill: "", variant: "primary", size: "sm" },
                          on: {
                            click: function($event) {
                              _vm.mode = "edit"
                              _vm.activeData = data.item
                            }
                          }
                        },
                        [
                          _c("b-icon", {
                            staticClass: "mr-1",
                            attrs: { icon: "pencil-square" }
                          }),
                          _vm._v("\n            Edit")
                        ],
                        1
                      )
                    ],
                    1
                  )
                ]
              }
            }
          ])
        }),
        _vm._v(" "),
        _c("b-pagination", {
          attrs: {
            "total-rows": _vm.tableData.length || 0,
            "per-page": _vm.per_page,
            "aria-controls": "my-table",
            align: "right"
          },
          model: {
            value: _vm.currentPage,
            callback: function($$v) {
              _vm.currentPage = $$v
            },
            expression: "currentPage"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      [
        _c(
          "b-modal",
          {
            attrs: {
              id: "stxviewmodal",
              title: _vm.mode.toUpperCase(),
              size: "lg"
            }
          },
          [
            _vm.mode == "edit"
              ? _c("OldassetForm", { attrs: { activeData: _vm.activeData } })
              : _vm._e(),
            _vm._v(" "),
            _vm.mode == "view"
              ? _c("OldassetView", { attrs: { activeData: _vm.activeData } })
              : _vm._e()
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=template&id=0f35eadb&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Oldasset/OldassetView.vue?vue&type=template&id=0f35eadb&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "flex justify-between pt-4 mt-2" }, [
      _c("div", [
        _vm._v("\n            PHOTO:\n            "),
        _c("div", [_vm._v(_vm._s(_vm.activeData.name))])
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "flex justify-between border-t border-gray-200 pt-4 mt-2"
      },
      [
        _c("div", [
          _vm._v("\n            Description:\n            "),
          _c("div", { staticClass: "font-poppins font-bold" }, [
            _vm._v(
              "\n              " +
                _vm._s(_vm.activeData.description) +
                "\n            "
            )
          ])
        ]),
        _vm._v(" "),
        _c(
          "div",
          [
            _c("stx-view-item", { attrs: { title: "Serial No:" } }, [
              _vm._v(_vm._s(_vm.activeData.serial_no))
            ]),
            _vm._v(" "),
            _c(
              "stx-view-item",
              { staticClass: "mt-2", attrs: { title: "Model No:" } },
              [_vm._v(_vm._s(_vm.activeData.model_no))]
            )
          ],
          1
        )
      ]
    ),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "flex justify-between border-t border-gray-200 pt-4 mt-2"
      },
      [
        _c("stx-view-item", { attrs: { title: "Manufactured At:" } }, [
          _vm._v(_vm._s(_vm.activeData.manufactured_at))
        ]),
        _vm._v(" "),
        _c("stx-view-item", { attrs: { title: "Warantee Start:" } }, [
          _vm._v(_vm._s(_vm.activeData.warantee_start))
        ]),
        _vm._v(" "),
        _c("stx-view-item", { attrs: { title: " Warantee End:" } }, [
          _vm._v(_vm._s(_vm.activeData.warantee_end) + "\n          ")
        ]),
        _vm._v(" "),
        _c("stx-view-item", { attrs: { title: " Service Due Date:" } }, [
          _vm._v(_vm._s(_vm.activeData.service_due_at))
        ])
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "flex justify-between border-t border-gray-200 pt-4 mt-2"
      },
      [
        _c("stx-view-item", { attrs: { title: "Purchased At:" } }, [
          _vm._v(_vm._s(_vm.activeData.purchased_at))
        ]),
        _vm._v(" "),
        _c("stx-view-item", { attrs: { title: "Status:" } }, [
          _vm._v(_vm._s(_vm.activeData.status))
        ]),
        _vm._v(" "),
        _c("stx-view-item", { attrs: { title: "Location:" } }, [
          _vm._v(_vm._s(_vm.activeData.location_id))
        ]),
        _vm._v(" "),
        _c("stx-view-item", { attrs: { title: "Base Location:" } }, [
          _vm._v(_vm._s(_vm.activeData.base_location))
        ])
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "flex justify-between border-t border-gray-200 pt-4 mt-2"
      },
      [
        _c("stx-view-item", { attrs: { title: "Year of Construction:" } }, [
          _vm._v(_vm._s(_vm.activeData.year_of_construction))
        ]),
        _vm._v(" "),
        _c("stx-view-item", { attrs: { title: "Country:" } }, [
          _vm._v(_vm._s(_vm.activeData.country))
        ]),
        _vm._v(" "),
        _c("stx-view-item", { attrs: { title: "Purchase Amount:" } }, [
          _vm._v(_vm._s(_vm.activeData.purchase_amount))
        ]),
        _vm._v(" "),
        _c("stx-view-item", { attrs: { title: "Currency:" } }, [
          _vm._v(_vm._s(_vm.activeData.purchase_currency))
        ])
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "flex justify-between border-t border-gray-200 pt-4 mt-2"
      },
      [
        _c("stx-view-item", { attrs: { title: "Note:" } }, [
          _vm._v(_vm._s(_vm.activeData.note))
        ])
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);