(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Dependant_DependantThumb_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Dependant/DependantThumb.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Dependant/DependantThumb.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _vue_composition_api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @vue/composition-api */ "./node_modules/@vue/composition-api/dist/vue-composition-api.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,_vue_composition_api__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  components: {},
  props: {
    thumbData: Object,
    title: String
  },
  computed: {
    data: function data() {
      return this.thumbData;
    }
  }
}));

/***/ }),

/***/ "./resources/js/Pages/Dependant/DependantThumb.vue":
/*!*********************************************************!*\
  !*** ./resources/js/Pages/Dependant/DependantThumb.vue ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _DependantThumb_vue_vue_type_template_id_7d7ccc78_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DependantThumb.vue?vue&type=template&id=7d7ccc78&scoped=true& */ "./resources/js/Pages/Dependant/DependantThumb.vue?vue&type=template&id=7d7ccc78&scoped=true&");
/* harmony import */ var _DependantThumb_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DependantThumb.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Dependant/DependantThumb.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _DependantThumb_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _DependantThumb_vue_vue_type_template_id_7d7ccc78_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _DependantThumb_vue_vue_type_template_id_7d7ccc78_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "7d7ccc78",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Dependant/DependantThumb.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Dependant/DependantThumb.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/Pages/Dependant/DependantThumb.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DependantThumb_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./DependantThumb.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Dependant/DependantThumb.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DependantThumb_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/Pages/Dependant/DependantThumb.vue?vue&type=template&id=7d7ccc78&scoped=true&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/Pages/Dependant/DependantThumb.vue?vue&type=template&id=7d7ccc78&scoped=true& ***!
  \****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DependantThumb_vue_vue_type_template_id_7d7ccc78_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DependantThumb_vue_vue_type_template_id_7d7ccc78_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DependantThumb_vue_vue_type_template_id_7d7ccc78_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./DependantThumb.vue?vue&type=template&id=7d7ccc78&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Dependant/DependantThumb.vue?vue&type=template&id=7d7ccc78&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Dependant/DependantThumb.vue?vue&type=template&id=7d7ccc78&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Dependant/DependantThumb.vue?vue&type=template&id=7d7ccc78&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {},
    [
      _c("div", { staticClass: "flex gap-8 items-center" }, [
        _c(
          "div",
          { staticClass: "w-2/6" },
          [
            _c("b-img", {
              staticClass: "rounded-lg",
              attrs: { src: _vm.data.photo, alt: "" }
            })
          ],
          1
        ),
        _vm._v(" "),
        _c("div", { staticClass: "w-4/6 pb-3" }, [
          _c("div", { staticClass: "text-xl font-bold text-brand-100" }, [
            _vm._v(_vm._s(_vm.title))
          ]),
          _vm._v(" "),
          _c("div", [
            _c("div", { staticClass: "text-xs font-base mt-2" }, [
              _vm._v("\n          Name:\n        ")
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "text-sm font-semibold mt-0" }, [
              _vm._v(
                "\n          " +
                  _vm._s(_vm.data.first_name) +
                  " " +
                  _vm._s(_vm.data.middle_name) +
                  " " +
                  _vm._s(_vm.data.last_name) +
                  "\n        "
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", [
            _c("div", { staticClass: "text-xs font-base mt-3 inline-block" }, [
              _vm._v("\n          DOB:\n        ")
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "text-sm font-semibold mt-1 inline-block" },
              [
                _vm._v(
                  "\n          " + _vm._s(_vm.data.date_of_birth) + "\n        "
                )
              ]
            )
          ]),
          _vm._v(" "),
          _c("div", [
            _c("div", { staticClass: "text-xs font-base mt-3 inline-block" }, [
              _vm._v("\n          Gender:\n        ")
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "text-sm font-semibold mt-1 inline-block" },
              [_vm._v("\n          " + _vm._s(_vm.data.sex) + "\n        ")]
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _vm._t("default")
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);