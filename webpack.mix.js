const mix = require("laravel-mix");
require("laravel-mix-purgecss");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/js/app.js", "public/js")
.sass('resources/js/app1.scss', 'public/css')
    .vue()
    .postCss("resources/css/app.css", "public/css", [
        require("postcss-import"),
        require("tailwindcss"),
        require("autoprefixer")
    ])
    .purgeCss({
        enabled: true,
        enabled: mix.inProduction(),
        folders: ["resources", "js"],
        extensions: ["html", "js", "php", "vue", "ts"]
    })
    .webpackConfig(require("./webpack.config"));

mix.browserSync({
    proxy: "localhost:8000",
    open: false,
    notify: false,
    browser: "chrome",
    // scrollRestoreTechnique: "cookie",
    watchOptions: {
        usePolling: true,
        interval: 500,
        poll: true,
        ignored: /node_modules/
    }
    // https: {
    //     cert: "F:\\New folder (2)\\certificate.crt",
    //     key: "F:\\New folder (2)\\privateKey.key"
    // }
});

if (mix.inProduction()) {
    mix.version();
}
