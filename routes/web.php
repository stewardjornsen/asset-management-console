<?php

use Inertia\Inertia;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\CardController;
use App\Http\Controllers\AssetController;
use App\Http\Controllers\PhotoController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\VendorController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\OldassetController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DependantController;
use App\Http\Controllers\PrincipalController;
use App\Http\Controllers\TechnicianController;
// use Laravel\Fortify\Http\Controllers\AuthenticatedSessionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => false,
        // 'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('dashboard/test', [DashboardController::class, 'index'])->name('dashboard');

Route::get('/foo', function () {
    return [
        'text' => 'Here you come!'
    ];
});

// Route::get('/logout', [AuthenticatedSessionController::class, 'destroy']);


Route::group(['middleware' => ['auth:sanctum', 'verified']], function () {
    Route::get('principal/pick/{principal}', [PrincipalController::class, 'pickPhoto'])->name('principal.pick');
    Route::post('dependant/create/{principal}', [PrincipalController::class, 'upload'])->name('principal.dependant.create');
    Route::post('principal/upload/{principal}', [PrincipalController::class, 'upload'])->name('principal.upload');
    Route::get('principal/create/client/{client}', [PrincipalController::class, 'create'])->name('client.principal.create');
    Route::get('principal/import', [PrincipalController::class, 'import'])->name('principal.import');
    Route::get('principal/export', [PrincipalController::class, 'export'])->name('principal.export');


    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::get('assets/{asset}/vendor/{vendor}/{action?}', [AssetController::class, 'vendor'])->name('assets.vendor');
    Route::get('assets/{asset}/technician/{technician}/{action?}', [AssetController::class, 'technician'])->name('assets.technician');
    Route::get('assets/{asset}/location/{location}/{action?}', [AssetController::class, 'location'])->name('assets.location');
    Route::get('assets/{asset}/convert-to/{type}', [AssetController::class, 'convert'])->name('assets.convert');

    Route::get('consumables/', [AssetController::class, 'index_consumables'])->name('consumables.index');
    Route::get('consumables/create', [AssetController::class, 'create_consumables'])->name('consumables.create');
    Route::get('consumables/{asset}', [AssetController::class, 'show_consumables'])->name('consumables.show');
    Route::get('consumables/{asset}/edit', [AssetController::class, 'edit_consumables'])->name('consumables.edit');
    Route::post('consumables', [AssetController::class, 'store_consumables'])->name('consumables.store');
    Route::put('consumables/{asset}', [AssetController::class, 'update_consumables'])->name('consumables.update');
    // Route::post('photos/{asset}', [PhotoController::class, 'asset_upload'])->name('photos.consumable');
    // Route::delete('photos/{asset}/{photo}', [PhotoController::class, 'asset_delete'])->name('photos.consumable.delete');

    Route::post('photos/assets/{asset}', [PhotoController::class, 'asset_upload'])->name('photos.asset');
    Route::delete('photos/assets/{photo}', [PhotoController::class, 'asset_delete'])->name('photos.asset.delete');

    Route::post('photos/vendors/{vendor}', [PhotoController::class, 'vendor_upload'])->name('photos.vendor');
    Route::delete('photos/vendors/{photo}', [PhotoController::class, 'vendor_delete'])->name('photos.vendor.delete');

    Route::post('photos/technicians/{technician}', [PhotoController::class, 'technician_upload'])->name('photos.technician');
    Route::delete('photos/technicians/{photo}', [PhotoController::class, 'technician_delete'])->name('photos.technician.delete');

    Route::resource('assets', AssetController::class);
    Route::resource('oldassets', OldassetController::class);
    Route::resource('locations', LocationController::class);
    Route::resource('vendors', VendorController::class);
    Route::resource('technicians', TechnicianController::class);
    Route::resource('principal', PrincipalController::class);


    Route::get('dependant/pick/{dependant}', [DependantController::class, 'pickPhoto'])->name('dependant.pick');
    Route::get('dependant/create/{principal}/', [DependantController::class, 'create'])->name('principal.dependant.create');
    Route::post('dependant/upload/{dependant}', [DependantController::class, 'upload'])->name('dependant.upload');
    Route::resource('dependant', DependantController::class);

    Route::post('client/upload/{client}', [ClientController::class, 'upload'])->name('client.upload');
    Route::resource('client', ClientController::class);

    Route::get('card/principal/{principal}', [CardController::class, 'principalCard'])->name('card.photo.principal');
    Route::get('card/dependant/{dependant}', [CardController::class, 'dependantCard'])->name('card.photo.dependant');
    Route::get('pdf/principal/{principal}', [CardController::class, 'principalPdf'])->name('card.pdf.principal');
    Route::get('pdf/dependant/{dependant}', [CardController::class, 'dependantPdf'])->name('card.pdf.dependant');
});

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return Inertia\Inertia::render('Dashboard');
// })->name('dashboard');
