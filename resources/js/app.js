require("./bootstrap");

import Vue from "vue";
import VueMeta from "vue-meta";

import PortalVue from "portal-vue";

import { App, plugin } from "@inertiajs/inertia-vue";
import { InertiaProgress } from "@inertiajs/progress/src";


import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
// Install BootstrapVue
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);

import Swal from "sweetalert2";
Vue.prototype.$Swal = Swal;

import moment from "moment";
Vue.prototype.$moment = moment;

// import "./app.scss";
// import "@/styler.css";

import StxCard from "@/components/StxCard.vue";
Vue.component("StxCard", StxCard);

import StxSidebar from "@/components/StxSidebar.vue";
Vue.component("StxSidebar", StxSidebar);

import CountCard from "@/components/CountCard.vue";
Vue.component("CountCard", CountCard);

import StxViewItem from "@/components/StxViewItem";
Vue.component("StxViewItem", StxViewItem);

import TakePhoto from "@/components/TakePhoto";
Vue.component("TakePhoto", TakePhoto);

import ModuleLinks from "@/components/ModuleLinks";
Vue.component("ModuleLinks", ModuleLinks);

import DatePicker from "vue2-datepicker";
//   import 'vue2-datepicker/index.css';
Vue.component("DatePicker", DatePicker);

Vue.config.productionTip = false;
Vue.mixin({ methods: { route: window.route } });
Vue.mixin({ methods: { route } });
Vue.use(PortalVue);
Vue.use(VueMeta);

InertiaProgress.init({
    // The delay after which the progress bar will
    // appear during navigation, in milliseconds.
    delay: 250,

    // The color of the progress bar.
    color: "#faca15",

    // Whether to include the default NProgress styles.
    includeCSS: true,

    // Whether the NProgress spinner will be shown.
    showSpinner: true
});

import _ from "lodash";
Vue.use(plugin);
Vue.use(_);

Vue.prototype.$route = route;

const el = document.getElementById("app");

new Vue({
    render: h =>
        h(App, {
            props: {
                initialPage: JSON.parse(el.dataset.page),
                resolveComponent: name =>
                    import(`./Pages/${name}`).then(module => module.default),
                resolveErrors: page => page.props.errors || {} // Customize if needed
            }
        })
}).$mount(el);
