const navigation = [
    {
        route: "dashboard",
        url: "dashboard",
        label: "Dashboard",
        param: {},
        icon: "file-bar-graph",
        divider: false,
        sidebar: true,
        navbar: true
    },
    {
        route: "assets.index",
        url: "assets",
        label: "Assets",
        param: {},
        icon: "hammer",
        divider: false,
        sidebar: true,
        navbar: true
    },
    {
        route: "consumables.index",
        url: "consumables",
        label: "Consumables",
        param: {},
        icon: "bag",
        divider: false,
        sidebar: true,
        navbar: true
    },
    {
        route: "locations.index",
        url: "locations",
        label: "Locations",
        param: {},
        icon: "geo-alt-fill",
        divider: true,
        sidebar: true,
        navbar: false
    },
    {
        route: "vendors.index",
        url: "vendors",
        label: "Vendors",
        param: {},
        icon: "cart",
        divider: false,
        sidebar: true,
        navbar: false
    },
    {
        route: "technicians.index",
        url: "technicians",
        label: "Technicians",
        param: {},
        icon: "person",
        divider: false,
        sidebar: true,
        navbar: false
    },
    {
        route: "oldassets.index",
        url: "oldassets",
        label: "Old Asset Records",
        param: {},
        icon: "archive",
        divider: true,
        sidebar: true,
        navbar: false
    },
    {
        route: "logout",
        url: "logout",
        label: "Log Out",
        param: {},
        icon: "archive",
        divider: true,
        sidebar: true,
        navbar: false
    }
];

const status = ["Active", "Inactive", "Suspended", "Processing"];

export { navigation, status };
