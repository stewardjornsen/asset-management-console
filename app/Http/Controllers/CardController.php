<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use PDF;
use App\Models\Dependant;
use App\Models\Principal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class CardController extends Controller
{
    //

    public function principalCard(Principal $principal)
    {
        $personaldata = $principal;
        // return
        //     view('Card', ['personaldata' => $personaldata]);

        // $pdf = PDF::loadView('Card', compact('personaldata'));
        // return $pdf->stream();
        // return $pdf->download('invoice.pdf');

        return Inertia::render('Card')->with(compact('personaldata'));
    }

    public function dependantCard(Dependant $dependant)
    {
        $personaldata = $dependant;
        return Inertia::render('Card')->with(compact('personaldata'));
    }

    public function principalPdf(Principal $principal)
    {
        $personaldata = $principal;
        return Inertia::render('Pdf')->with(compact('personaldata'));
    }

    public function dependantPdf(Dependant $dependant)
    {
        $personaldata = $dependant;
        return Inertia::render('Pdf')->with(compact('personaldata'));
    }

    public function viewCard()
    {
    }
}
