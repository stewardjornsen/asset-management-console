<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Asset;
use App\Models\Vendor;
use App\Models\Location;
use App\Models\Oldasset;
use App\Models\Technician;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function index()
    {

        $cards = [
            [
                'header' => 'Assets',
                'description' => 'Fixed assets with service and warranty dates',
                'icon' => 'hammer',
                'count' => Asset::count(),
                'createRoute' => 'assets.create',
                'indexRoute' => 'assets.index',
            ],
            [
                'header' => 'Locations',
                'description' => 'Place where all assets and consumables are kept',
                'icon' => 'geo-alt',
                'count' => Location::count(),
                'createRoute' => 'locations.create',
                'indexRoute' => 'locations.index',
            ],
            [
                'header' => 'Vendors',
                'description' => 'People or Companies that supply assets/consumables',
                'icon' => 'cart',
                'count' => Vendor::count(),
                'createRoute' => 'vendors.create',
                'indexRoute' => 'vendors.index',
            ],
            [
                'header' => 'Technicians',
                'description' => 'People or Companies that service or maintain assets',
                'icon' => 'person',
                'count' => Technician::count(),
                'createRoute' => 'technicians.create',
                'indexRoute' => 'technicians.index',
            ],
            [
                'header' => 'Old Assets',
                'description' => 'Old Asset records from previous database',
                'icon' => 'archive',
                'count' => Oldasset::count(),
                'createRoute' => 'oldassets.create',
                'indexRoute' => 'oldassets.index',
            ],
        ];


        return Inertia::render('Dashboard', [
            'cards' => $cards,
        ]);
    }
}
