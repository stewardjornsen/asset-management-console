<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Asset;
use App\Models\Vendor;
use App\Models\Location;
use App\Models\Technician;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Route;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // return Asset::expiredAssets()->get();
        // return Asset::expiredAssets()->get();

        $header = 'Asset';
        $today = Carbon::today()->toDateString();
        $lastTwoMonths = Carbon::today()->subMonth(2)->toDateString();
        $lastOneMonths = Carbon::today()->subMonth(1)->toDateString();
        //If warranty, expiration or service is less than today, item is expired
        $warranty_valid = Asset::where('is_asset', 1)->whereDate('warranty_end', '>', $today)->get();
        $warranty_warning = Asset::where('is_asset', 1)->whereDate('warranty_end', '<', $today)->whereDate('warranty_end', '>', $lastTwoMonths)->get();
        $warranty_expired = Asset::where('is_asset', 1)->whereDate('warranty_end', '<=', $today)->get();

        // return [
        //     'today' => $today,
        //     'lastOneMonths' => $lastOneMonths,
        //     'lastTwoMonths' => $lastTwoMonths,
        //     'warranty_valid' => $warranty_valid,
        //     'warranty_warning' => $warranty_warning,
        //     'warranty_expired' => $warranty_expired,
        // ];

        $assets = Asset::where('is_asset', 1)->orderByDesc('updated_at')->get();
        return Inertia::render('Asset/AssetHome', [
            'records' => $assets,
            // 'id' => $assets->id,
            'count' => $assets->count(),
            'title' => 'Assets Register',
            'icon' => 'hammer',
            'indexRoute' => 'assets.index',
            'createRoute' => 'assets.create',
            'showRoute' => 'assets.show',
            'editRoute' => 'assets.edit',
            'destroyRoute' => 'assets.destroy',
            'description' => 'Assets with Warranty and Service Dates',
            'header' => ['singular' => $header, 'plural' => Str::plural($header)],
            'is_asset' => 1,
            'fields' => [
                ['key' => "action", 'sortable' => false],
                ['key' => "id", 'sortable' => true],
                ['key' => "name", 'label' => "Item Name", 'sortable' => true],
                // ['key' => "code", 'sortable' => true],
                ['key' => "tag_number", 'label' => "City", 'sortable' => true],
                // ['key' => "model_name", 'sortable' => true],
                ['key' => "model_no", 'sortable' => true],
                ['key' => "serial_no", 'sortable' => true],
                ['key' => "warranty_end", 'sortable' => true],
                ['key' => "service_due_at", 'sortable' => true],
                ['key' => "status", 'sortable' => true]
            ]
        ]);
    }
    public function index_consumables()
    {
        $header = 'Consumable';
        $consumables = Asset::where('is_asset', 0)->orderByDesc('updated_at')->get();
        return Inertia::render('Asset/AssetHome', [
            'records' => $consumables,
            // 'id' => $consumable->id,
            'count' => $consumables->count(),
            'title' => 'Consumables Register',
            'icon' => 'bag',
            'indexRoute' => 'consumables.index',
            'createRoute' => 'consumables.create',
            'showRoute' => 'consumables.show',
            'editRoute' => 'consumables.edit',
            'destroyRoute' => 'consumables.destroy',
            'description' => 'Items with Quantities and Expiration Dates',
            'header' => ['singular' => $header, 'plural' => Str::plural($header)],
            'is_asset' => 0,
            'fields' => [
                ['key' => "action", 'sortable' => false],
                ['key' => "id", 'sortable' => true],
                ['key' => "name", 'label' => "Item Name", 'sortable' => true],
                // ['key' => "code", 'sortable' => true],
                ['key' => "tag_number", 'label' => "City", 'sortable' => true],
                // ['key' => "model_name", 'sortable' => true],
                ['key' => "model_no", 'sortable' => true],
                ['key' => "purchase_at", 'sortable' => true],
                ['key' => "expire_at", 'sortable' => true],
                ['key' => "standard_stock", 'sortable' => true],
                ['key' => "current_stock", 'sortable' => true],
                ['key' => "status", 'sortable' => true]
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = 'Asset';
        $assets = Asset::where('is_asset', 1)->get();
        return Inertia::render('Asset/AssetHome', [
            'records' => $assets,
            'activeData' => (object) ['id' => -1],
            'display' => 'form',
            // 'id' => $assets->id,
            'count' => $assets->count(),
            'title' => 'Assets Register',
            'icon' => 'hammer',
            'indexRoute' => 'assets.index',
            'createRoute' => 'assets.create',
            'storeRoute' => 'assets.store',
            'showRoute' => 'assets.show',
            'editRoute' => 'assets.edit',
            'destroyRoute' => 'assets.destroy',
            'description' => 'Assets with Warranty and Service Dates',
            'header' => ['singular' => $header, 'plural' => Str::plural($header)],
            'is_asset' => 1,
        ]);
        // return Inertia::render('Asset/AssetHome', [
        //     'activeData' => (object) ['id' => -1],
        //     'display' => 'form',
        //     'count' => Asset::count()
        // ]);
    }
    public function create_consumables()
    {
        $header = 'Consumable';
        $consumable = Asset::where('is_asset', 0)->get();
        return Inertia::render('Asset/AssetHome', [
            'records' => $consumable,
            'activeData' => (object) ['id' => -1],
            'display' => 'form',
            // 'id' => $consumable->id,
            'count' => $consumable->count(),
            'title' => 'Consumables Register',
            'icon' => 'bag',
            'indexRoute' => 'consumables.index',
            'createRoute' => 'consumables.create',
            'storeRoute' => 'consumables.store',
            'showRoute' => 'consumables.show',
            'editRoute' => 'consumables.edit',
            'destroyRoute' => 'consumables.destroy',
            'description' => 'Items with Quantities and Expiration Dates',
            'header' => ['singular' => $header, 'plural' => Str::plural($header)],
            'is_asset' => 0,
        ]);
        // return Inertia::render('Asset/AssetHome', [
        //     'activeData' => (object) ['id' => -1],
        //     'display' => 'form',
        //     'count' => Asset::count()
        // ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function do_validation(Request $request)
    {
        // dd($request);

        if ($request->is_asset == 1) {
            $data = $request->validate([
                "name" => "required",
                "description" => "required",
                "code" => "",
                "tag_number" => "",
                "barcode_number" => "",
                "dimension" => "nullable",
                "weight" => "",
                // "standard_stock" => "numeric",
                // "current_stock" => "numeric",
                // "unit" => "",
                // "sub_unit" => "",
                // "sub_items_quantity" => "numeric",
                "manufacturer" => "",
                "country" => "",
                "model_name" => "",
                "model_no" => "",
                "serial_no" => "",
                "manufactured_at" => "date|nullable",
                "year_of_construction" => "numeric|nullable",
                // "expire_at" => "date|nullable",
                "warranty_start" => "date|nullable",
                "warranty_end" => "date|nullable",
                "service_due_at" => "date|nullable",
                "purchased_at" => "date|nullable",
                "purchase_amount" => "numeric",
                "purchase_currency" => "alpha",
                "category" => "",
                "status" => "",
                "note" => "",
                "is_asset" => "numeric",
                "status" => "required"
            ]);
        } else {
            $data = $request->validate(
                [
                    "name" => "required",
                    "description" => "required",
                    "code" => "",
                    "tag_number" => "",
                    "barcode_number" => "",
                    "dimension" => "nullable",
                    "weight" => "",
                    "standard_stock" => "numeric",
                    "current_stock" => "numeric",
                    "unit" => "",
                    "sub_unit" => "",
                    "sub_items_quantity" => "numeric",
                    "manufacturer" => "",
                    "country" => "",
                    "model_name" => "",
                    "model_no" => "",
                    "serial_no" => "",
                    "manufactured_at" => "date|nullable",
                    "year_of_construction" => "numeric|nullable",
                    "expire_at" => "date|nullable",
                    // "warranty_start" => "date|nullable",
                    // "warranty_end" => "date|nullable",
                    // "service_due_at" => "date|nullable",
                    "purchased_at" => "date|nullable",
                    "purchase_amount" => "numeric",
                    "purchase_currency" => "alpha",
                    "category" => "",
                    "status" => "",
                    "note" => "",
                    "is_asset" => "numeric",
                    "status" => "required"
                ]
            );
        }
        return $data;
    }
    public function store(Request $request)
    {
        // return $request->validated();
        $data = $this->do_validation($request);
        // $data = (object)$this->do_validation($request);
        $asset = new Asset;
        $asset->name = $request->name;
        $asset->description = $request->description;
        $asset->code = $request->code;
        $asset->tag_number = $request->tag_number;
        $asset->barcode_number = $request->barcode_number;
        $asset->dimension = $request->dimension;
        $asset->weight = $request->weight;
        $asset->manufacturer = $request->manufacturer;
        $asset->country = $request->country;
        $asset->model_name = $request->model_name;
        $asset->model_no = $request->model_no;
        $asset->serial_no = $request->serial_no;
        $asset->manufactured_at = $request->manufactured_at;
        $asset->year_of_construction = $request->year_of_construction;
        $asset->warranty_start = $request->warranty_start;
        $asset->warranty_end = $request->warranty_end;
        $asset->service_due_at = $request->service_due_at;
        $asset->purchased_at = $request->purchased_at;
        $asset->purchase_amount = $request->purchase_amount;
        $asset->purchase_currency = $request->purchase_currency;
        $asset->category = $request->category;
        $asset->status = $request->status;
        $asset->note = $request->note;
        $asset->is_asset = $request->is_asset;
        $asset->status = $request->status;
        $id =  $asset->save();
        // dd( $data);
        // return $id = Asset::create($data);
        // return $id = Asset::create(['name'=>'Johnny']);
        return redirect(route('assets.edit', $id))->with('msg', 'Completed');
        // return redirect(route('assets.edit', $id))->with('msg', 'Completed');
    }
    public function store_consumables(Request $request)
    {
        // return $request;
        $data = $this->do_validation($request);
        $asset = new Asset;
        $asset->name = $request->name;
        $asset->description = $request->description;
        $asset->code = $request->code;
        $asset->tag_number = $request->tag_number;
        $asset->barcode_number = $request->barcode_number;
        $asset->dimension = $request->dimension;
        $asset->weight = $request->weight;
        $asset->manufacturer = $request->manufacturer;
        $asset->country = $request->country;
        $asset->model_name = $request->model_name;
        $asset->model_no = $request->model_no;
        $asset->serial_no = $request->serial_no;
        $asset->manufactured_at = $request->manufactured_at;
        $asset->year_of_construction = $request->year_of_construction;
        $asset->standard_stock = $request->standard_stock;
        $asset->current_stock = $request->current_stock;
        $asset->unit = $request->unit;
        $asset->sub_unit = $request->sub_unit;
        $asset->sub_items_quantity = $request->sub_items_quantity;
        $asset->purchased_at = $request->purchased_at;
        $asset->purchase_amount = $request->purchase_amount;
        $asset->purchase_currency = $request->purchase_currency;
        $asset->category = $request->category;
        $asset->status = $request->status;
        $asset->note = $request->note;
        $asset->is_asset = $request->is_asset;
        $asset->status = $request->status;
        $id =  $asset->save();
        // $id = Asset::create($data);
        return redirect(route('consumables.edit', $id))->with('msg', 'Completed');
        // return redirect(route('assets.edit', $id))->with('msg', 'Completed');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $asset)
    {

        $header = 'Asset';
        $assets = Asset::where('is_asset', 1)->get();
        return Inertia::render('Asset/AssetHome', [
            'records' => $assets,
            'activeData' => Asset::where('id', $asset->id)->where('is_asset', 1)->with(['vendors', 'technicians', 'locations'])->find($asset)->first(),
            'display' => 'view',
            'id' => $asset->id,
            'count' => $assets->count(),
            'title' => 'Assets Register',
            'icon' => 'hammer',
            'indexRoute' => 'assets.index',
            'createRoute' => 'assets.create',
            'showRoute' => 'assets.show',
            'editRoute' => 'assets.edit',
            'destroyRoute' => 'assets.destroy',
            'photoRoute' => 'photos.asset',
            'photoDeleteRoute' => 'photos.asset.delete',
            'description' => 'Assets with Warranty and Service Dates',
            'header' => ['singular' => $header, 'plural' => Str::plural($header)],
            'is_asset' => 1,
            'vendors' => Vendor::all(),
            'technicians' => Technician::all(),
            'locations' => Location::all(),
            'photos' => $asset->photos
        ]);
    }
    public function show_consumables(Asset $asset)
    {

        $header = 'Consumable';
        $consumables = Asset::where('is_asset', 0)->get();
        return Inertia::render('Asset/AssetHome', [
            'records' => $consumables,
            'activeData' => Asset::where('id', $asset->id)->where('is_asset', 0)->with(['vendors', 'technicians', 'locations'])->find($asset)->first(),
            'display' => 'view',
            'id' => $asset->id,
            'count' => $consumables->count(),
            'title' => 'Consumables Register',
            'icon' => 'bag',
            'indexRoute' => 'consumables.index',
            'createRoute' => 'consumables.create',
            'showRoute' => 'consumables.show',
            'editRoute' => 'consumables.edit',
            'destroyRoute' => 'consumables.destroy',
            'photoRoute' => 'photos.asset',
            'photoDeleteRoute' => 'photos.asset.delete',
            'description' => 'Items with Quantities and Expiration Dates',
            'header' => ['singular' => $header, 'plural' => Str::plural($header)],
            'is_asset' => 0,
            'vendors' => Vendor::all(),
            'technicians' => Technician::all(),
            'locations' => Location::all(),
            'photos' => $asset->photos
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function edit(Asset $asset)
    {
        $header = 'Asset';
        $assets = Asset::where('is_asset', 1)->get();
        return Inertia::render('Asset/AssetHome', [
            'records' => $assets,
            // 'activeData' => Asset::with(['vendors', 'technicians', 'locations'])->find($asset)->first(),
            'activeData' => $asset,
            'display' => 'form',
            'id' => $asset->id,
            'count' => $assets->count(),
            'title' => 'Assets Register',
            'icon' => 'hammer',
            'indexRoute' => 'assets.index',
            'createRoute' => 'assets.create',
            'showRoute' => 'assets.show',
            'storeRoute' => 'assets.store',
            'editRoute' => 'assets.edit',
            'updateRoute' => 'assets.update',
            'destroyRoute' => 'assets.destroy',
            'photoRoute' => 'photos.asset',
            'photoDeleteRoute' => 'photos.asset.delete',
            'description' => 'Assets with Warranty and Service Dates',
            'header' => ['singular' => $header, 'plural' => Str::plural($header)],
            'is_asset' => 1,
            'vendors' => Vendor::all(),
            'technicians' => Technician::all(),
            'locations' => Location::all(),
            'photos' => $asset->photos
        ]);
        // return Inertia::render('Asset/AssetHome', [
        //     'activeData' => $asset,
        //     'display' => 'form',
        //     'count' => Asset::count()
        // ]);
    }
    public function edit_consumables(Asset $asset)
    {
        $header = 'Consumable';
        $consumable = Asset::where('is_asset', 0)->get();
        return Inertia::render('Asset/AssetHome', [
            'records' => $consumable,
            // 'activeData' => Asset::with(['vendors', 'technicians', 'locations'])->find($asset)->first(),
            'activeData' => $asset,
            'display' => 'form',
            'id' => $asset->id,
            'count' => $consumable->count(),
            'title' => 'Consumables Register',
            'icon' => 'bag',
            'indexRoute' => 'consumables.index',
            'createRoute' => 'consumables.create',
            'showRoute' => 'consumables.show',
            'editRoute' => 'consumables.edit',
            'destroyRoute' => 'consumables.destroy',
            'photoRoute' => 'photos.asset',
            'photoDeleteRoute' => 'photos.asset.delete',
            'updateRoute' => 'consumables.update',
            'storeRoute' => 'consumables.store',
            'description' => 'Items with Quantities and Expiration Dates',
            'header' => ['singular' => $header, 'plural' => Str::plural($header)],
            'is_asset' => 0,
            'vendors' => Vendor::all(),
            'technicians' => Technician::all(),
            'locations' => Location::all(),
            'photos' => $asset->photos
        ]);
        // return Inertia::render('Asset/AssetHome', [
        //     'activeData' => $asset,
        //     'display' => 'form',
        //     'count' => Asset::count()
        // ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asset $asset)
    {
        $data = $this->do_validation($request);
        // return $asset;
        $id = $asset->update($data);
        return redirect(route('assets.index', $asset->id))->with('msg', 'Completed');
        // return redirect(route('assets.edit', $asset->id))->with('msg', 'Completed');
    }
    public function update_consumables(Request $request, Asset $asset)
    {
        $data = $this->do_validation($request);
        // return $asset;
        $id = $asset->update($data);
        return redirect(route('consumables.index', $asset->id))->with('msg', 'Completed');
        // return redirect(route('assets.edit', $asset->id))->with('msg', 'Completed');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset $asset)
    {
        $asset->delete();
        return redirect(route('assets.index'));
    }

    /*
    Steward Notes
    Assigning Relations
    */
    public function vendor(Request $request, Asset $asset, Vendor $vendor, $action = 'add')
    {
        if ($action == 'add') {
            $asset->vendors()->sync($vendor->id, false);
        } else {
            $asset->vendors()->detach($vendor->id);
        }
        return $this->show($asset);
        return Asset::with(['vendors', 'technicians', 'locations'])->find($asset)->first();
    }
    public function technician(Request $request, Asset $asset, Technician $technician, $action = 'add')
    {



        if ($action == 'add') {
            $asset->technicians()->sync($technician, false);
        } else {
            $asset->technicians()->detach($technician->id);
        }
        return $this->show($asset);
        return Asset::with(['vendors', 'technicians', 'locations'])->find($asset)->first();
    }
    public function location(Request $request, Asset $asset, Location $location, $action = 'add')
    {
        if ($action == 'add') {
            $asset->locations()->sync($location->id, false);
        } else {
            $asset->locations()->detach($location->id);
        }
        return $this->show($asset);
        return Asset::with(['locations', 'technicians', 'locations'])->find($asset)->first();
    }

    public function upload(Request $request, Asset $asset)
    {
        //VALIDATE UPLOADED PHOTO
        $validation = $request->validate([
            'file' => 'required|file|image|mimes:jpeg,png,gif,webp|max:2048'
            // for multiple file uploads
            // 'photo.*' => 'required|file|image|mimes:jpeg,png,gif,webp|max:2048'
        ]);

        //DELETE OLD FILE
        if ($asset->photo) {
            $fileToDelete = Storage::path('public/assets/' . basename($asset->photo));
            if (file_exists($fileToDelete))
                unlink(Storage::path('public/assets/' . basename($asset->photo)));
        }

        //CREATE FILENAME FOR NEW FILE, SCALE AND SAVE
        $id = "stx_asset_" . $asset->id . "_" . md5($asset->id . time());
        $filename = $id . '.jpg';
        $img = Image::make($request->file('file'));
        $img->fit(413, 531);

        //RESIZING THE IMAGE
        // $img->resize(400, 400, function ($constraint) {
        //     $constraint->aspectRatio();
        //     $constraint->upsize();
        // });

        //SAVE IMAGE TO PUBLIC PATH WITH SYMBOLINK STORAGE
        $res = $img->save(Storage::path('public/assets/' . $filename));
        $asset->photo = 'assets/' . $filename;
        $asset->save();

        //REDIRECT
        return redirect()->back()->with('message', 'Image Uploaded');
        // return redirect(route('asset.index'))->with('message', 'Image Uploaded');

        //PREVIEW UPLOADED IMAGE
        // return '<img src="' . asset('storage/assets/' . $filename) . '" />';
    }

    public function convert(Request $request, Asset $asset, $type)
    {
        $url = $request->route()->uri();
        $value = $type == 1 ? 0 : 1;

        $data = ['is_asset' => $value];
        $asset->update($data);

        if ($value === 1) {
            return $this->index();
        } else {
            return $this->index_consumables();
        }
    }
}
