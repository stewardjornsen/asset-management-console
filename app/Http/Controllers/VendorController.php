<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Vendor;
use Illuminate\Http\Request;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendors = Vendor::all();
        return Inertia::render('Vendor/VendorHome', [
            'vendors' => $vendors,
            'count' => Vendor::count(),

            'indexRoute' => 'vendors.index',
            'createRoute' => 'vendors.create',
            'showRoute' => 'vendors.show',
            'storeRoute' => 'vendors.store',
            'editRoute' => 'vendors.edit',
            'updateRoute' => 'vendors.update',
            'destroyRoute' => 'vendors.destroy',
            'photoRoute' => 'photos.vendor',
            'photoDeleteRoute' => 'photos.vendor.delete',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Vendor/VendorHome', [
            'activeData' => (object) [],
            'display' => 'form'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function do_validation(Request $request)
    {
        $data = $request->validate([
            "name" => "required",
            "contact_person" => "required",
            "address" => "",
            "city" => "",
            "state" => "",
            "country" => "",
            "email" => "email|nullable",
            "mobile" => "required",
            "phone" => "",
            // "phone_2" => "",
            // "fax" => "",
            "postal_zip" => "",
            "notes" => "",
            "status" => "required"
        ]);
        return $data;
    }
    public function store(Request $request)
    {
        // return $request;
        $data = $this->do_validation($request);
        $id = Vendor::create($data);
        return redirect(route('vendors.edit', $id))->with('msg', 'Completed');
        // return redirect(route('vendors.edit', $id))->with('msg', 'Completed');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show(Vendor $vendor)
    {
        return Inertia::render('Vendor/VendorHome', [
            'activeData' => $vendor,
            'id' => $vendor->id,
            'display' => 'view',
            'count' => Vendor::count(),
            'indexRoute' => 'vendors.index',
            'createRoute' => 'vendors.create',
            'showRoute' => 'vendors.show',
            'storeRoute' => 'vendors.store',
            'editRoute' => 'vendors.edit',
            'updateRoute' => 'vendors.update',
            'destroyRoute' => 'vendors.destroy',
            'photoRoute' => 'photos.vendor',
            'photoDeleteRoute' => 'photos.vendor.delete',
            'photos' => $vendor->photos
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function edit(Vendor $vendor)
    {
        // return $vendor;
        return Inertia::render('Vendor/VendorHome', [
            'activeData' => $vendor,
            'display' => 'form',
            'count' => Vendor::count(),
            'id' => $vendor->id,
            'count' => Vendor::count(),
            'indexRoute' => 'vendors.index',
            'createRoute' => 'vendors.create',
            'showRoute' => 'vendors.show',
            'storeRoute' => 'vendors.store',
            'editRoute' => 'vendors.edit',
            'updateRoute' => 'vendors.update',
            'destroyRoute' => 'vendors.destroy',
            'photoRoute' => 'photos.vendor',
            'photoDeleteRoute' => 'photos.vendor.delete',
            'photos' => $vendor->photos
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vendor $vendor)
    {
        $data = $this->do_validation($request);
        // return $vendor;
        $id = $vendor->update($data);
        return redirect(route('vendors.index', $vendor->id))->with('msg', 'Completed');
        // return redirect(route('vendors.edit', $vendor->id))->with('msg', 'Completed');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vendor $vendor)
    {
        $vendor->delete();
        return redirect(route('vendors.index'));
    }
}
