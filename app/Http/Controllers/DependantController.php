<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Traits\StxTrait;
use App\Models\Dependant;
use App\Models\EnrolleeType;
use App\Models\Principal;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class DependantController extends Controller
{
    use StxTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items =  Dependant::with(['EnrolleeType', 'Principal'])->get();
        return Inertia::render('Dependant/Index', [
            'display' => 'table',
            'items' => $items,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Principal $principal)
    {
        $principal = $principal;
        $dependant = (object) ['id' => null];
        $enrolleetypes = EnrolleeType::get(['id', 'type']);
        return Inertia::render('Dependant/Index', ['display' => 'form'])
            ->with(compact('principal'))
            ->with(compact('enrolleetypes'))
            ->with(compact('dependant'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $enrolleeCode = EnrolleeType::find($request->enrollee_type_id)->only('code')['code'];


        // $firstLevel = [
        //     'first_name' => 'required|max:50',
        //     'middle_name' => 'max:50',
        //     'last_name' => 'required|max:50',
        //     'registration_date' => 'date|required',
        //     'sex' => "required|max:6",
        //     'date_of_birth' => "required|date",
        //     'same_address_as_principal' => 'required|boolean',
        //     'principal_id' => 'required|numeric',
        //     'enrollee_type_id' => 'required|numeric',
        //     'medical_condition' => '',
        //     'about' => '',
        //     // 'photo' => '',
        // ];
        // $secondLevel = [
        //     'email' => "required|email",
        //     'telephone' => "required|max:20",
        //     'address' => "required|max:150",
        //     'zip_code' => "max:10",
        //     'city' => "required|max:50",
        //     'state' => "required|max:50",
        //     'country' => "required|max:50",
        // ];

        // if ($request->same_address_as_principal) {
        //     $finalLevel = array_merge($firstLevel);
        // } else {
        //     $finalLevel = array_merge($firstLevel, $secondLevel);
        // }

        // $validated = $request->validate($finalLevel, [
        //     'principal_id.required' => 'Link this account to a Principal',
        //     'sex.required' => 'Please specify a gender'
        // ]);

        //  dd($request->same_address_as_principal);


        $validated = $this->checkIfValid($request);
        // return $validated;

        $person = Dependant::create($validated);
        //TODO GET REAL COMPANY RECORD FROM PRINCIPAL
        $company = 'StewardXpress Nigeria Limited';
        $idCardNumber = $this->generateNewIdNumber('SW', $company, $person->id, $enrolleeCode);

        $person->id_card_number = $idCardNumber;
        $person->update();
        //

        return redirect(route('dependant.index'))->with('success', 'Post Created Successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Dependant  $dependant
     * @return \Illuminate\Http\Response
     */
    public function show(Dependant $dependant)
    {


        // return $dependant;
        if (!$dependant->id_card_number) {
            $enrolleeCode = EnrolleeType::find($request->enrollee_type_id)->only('code')['code'];
            //TODO GET REAL COMPANY RECORD FROM PRINCIPAL
            $company = 'StewardXpress Nigeria Limited';

            $idCardNumber = $this->generateNewIdNumber('SW', $company, $dependant->id, $enrolleeCode);

            $dependant->id_card_number = $idCardNumber;
            $dependant->update();
        }

        return Inertia::render('Dependant/Index', [
            'dependant' => fn () => $dependant,
            'display' => 'view'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Dependant  $dependant
     * @return \Illuminate\Http\Response
     */
    public function edit(Dependant $dependant)
    {
        return Inertia::render('Dependant/Index', [
            'dependant' => fn () => $dependant,
            'principal' => fn () => $dependant->principal,
            'display' => 'form'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Dependant  $dependant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dependant $dependant)
    {
        $data = $this->checkIfValid($request);
        $dependant->update($data);
        return redirect(route('dependant.edit', $dependant->id))->with('success', 'Post Updated Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Dependant  $dependant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dependant $dependant)
    {
        if ($dependant) $dependant->delete();
        return redirect(route('dependant.index'))->with('message', 'Item Deleted');
        dd($dependant);
    }

    /*

MY STX FUNCTIONS

    */

    public function generateIdNumber($str)
    {
        $strA = explode(' ', $str);
        $string = "";
        foreach ($strA as $words) {
            if (strlen(trim($words)) && is_string($words))
                $string = $string . $words[0];
        }
        return (strtoupper($string));
    }

    public function pickPhoto(Dependant $dependant)
    {
        // dd($dependant->id);
        return Inertia::render('Dependant/DependantPhoto', [
            'id' => $dependant->id,
            'data' => $dependant->only(['id', 'sex'])
        ]);
    }

    public function upload(Request $request, Dependant $dependant)
    {
        //VALIDATE UPLOADED PHOTO
        $validation = $request->validate([
            'file' => 'required|file|image|mimes:jpeg,png,gif,webp|max:2048'
            // for multiple file uploads
            // 'photo.*' => 'required|file|image|mimes:jpeg,png,gif,webp|max:2048'
        ]);

        //DELETE OLD FILE
        if ($dependant->photo) {
            $fileToDelete = Storage::path('public/dependants/' . basename($dependant->photo));
            if (file_exists($fileToDelete))
                unlink(Storage::path('public/dependants/' . basename($dependant->photo)));
        }

        //CREATE FILENAME FOR NEW FILE, SCALE AND SAVE
        $id = "stx_dependant_" . $dependant->id . "_" . md5($dependant->id . time());
        $filename = $id . '.jpg';
        $img = Image::make($request->file('file'));
        $img->fit(413, 531);

        //RESIZING THE IMAGE
        // $img->resize(400, 400, function ($constraint) {
        //     $constraint->aspectRatio();
        //     $constraint->upsize();
        // });

        //SAVE IMAGE TO PUBLIC PATH WITH SYMBOLINK STORAGE
        $res = $img->save(Storage::path('public/dependants/' . $filename));
        $dependant->photo = 'dependants/' . $filename;
        $dependant->save();

        //REDIRECT
        return redirect()->back()->with('message', 'Image Uploaded');
        // return redirect(route('dependant.index'))->with('message', 'Image Uploaded');

        //PREVIEW UPLOADED IMAGE
        // return '<img src="' . asset('storage/dependants/' . $filename) . '" />';
    }

    private function checkIfValid(Request $request)
    {


        $firstLevel = [
            'first_name' => 'required|max:50',
            'middle_name' => 'max:50',
            'last_name' => 'required|max:50',
            'registration_date' => 'date|required',
            'sex' => "required|max:6",
            'date_of_birth' => "required|date",
            'same_address_as_principal' => 'required|boolean',
            'principal_id' => 'required|numeric',
            'enrollee_type_id' => 'required|numeric',
            'medical_condition' => '',
            'about' => '',

        ];
        $secondLevel = [
            'email' => "required|email",
            'telephone' => "required|max:20",
            'address' => "required|max:150",
            'zip_code' => "max:10",
            'city' => "required|max:50",
            'state' => "required|max:50",
            'country' => "required|max:50",
        ];

        $thirdLevel = [
            'id_card_number' => 'required|max:50',
            'principal_id' => 'required|numeric',
            'enrollee_type_id' => 'required|numeric',
        ];

        if ($request->same_address_as_principal) {
            $finalLevel = array_merge($firstLevel);
            if ($request->has('id')) {
                $finalLevel = array_merge($firstLevel, $thirdLevel);
            }
        } else {
            $finalLevel = array_merge($firstLevel, $secondLevel);
            if ($request->has('id')) {
                $finalLevel = array_merge($firstLevel, $secondLevel, $thirdLevel);
            }
        }

        return  $validated = $request->validate($finalLevel, [
            'principal_id.required' => 'Link this account to a Principal',
            'sex.required' => 'Please specify a gender'
        ]);

        // return $validated = $request->validate([
        //     'first_name' => 'required|max:50',
        //     'middle_name' => 'max:50',
        //     'last_name' => 'required|max:50',
        //     'registration_date' => 'date|required',
        //     'sex' => "required|max:6",
        //     'date_of_birth' => "required|date",
        //     'email' => "required|email",
        //     'telephone' => "max:20",
        //     'address' => "max:150",
        //     'zip_code' => "max:50",
        //     'city' => "max:50",
        //     'state' => "max:50",
        //     'country' => "required|max:50",
        //     'client_id' => 'required|numeric',
        //     'medical_condition' => '',
        //     'about' => '',
        //     'id_card_number' => 'required|max:50',
        //     'principal_id' => 'required|numeric',
        //     'enrollee_type_id' => 'required|numeric',
        // ], [
        //     'client_id.required' => 'Please set a Company for this enrollee',
        //     'sex.required' => 'Please specify a gender'
        // ]);
    }
}
