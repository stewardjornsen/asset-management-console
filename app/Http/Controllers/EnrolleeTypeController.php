<?php

namespace App\Http\Controllers;

use App\Models\EnrolleeType;
use Illuminate\Http\Request;

class EnrolleeTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EnrolleeType  $enrolleeType
     * @return \Illuminate\Http\Response
     */
    public function show(EnrolleeType $enrolleeType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EnrolleeType  $enrolleeType
     * @return \Illuminate\Http\Response
     */
    public function edit(EnrolleeType $enrolleeType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EnrolleeType  $enrolleeType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EnrolleeType $enrolleeType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EnrolleeType  $enrolleeType
     * @return \Illuminate\Http\Response
     */
    public function destroy(EnrolleeType $enrolleeType)
    {
        //
    }
}
