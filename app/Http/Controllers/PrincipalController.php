<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Client;
use App\Traits\StxTrait;
use App\Models\Principal;
use Illuminate\Http\Request;
use function PHPSTORM_META\map;
use App\Exports\PrincipalsExport;

use App\Imports\PrincipalsImport;
use Maatwebsite\Excel\Facades\Excel;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class PrincipalController extends Controller
{
    use StxTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $principals =  Principal::with(['client' => function ($q) {
            $q->select('id', 'name', 'telephone');
        }])->get();

        return Inertia::render('Principal/Index', [
            'display' => 'table',
            'principals' => $principals,
            // 'principals' => Principal::get(),

            // ALWAYS included on first visit
            // OPTIONALLY included on partial reloads
            // ONLY evaluated when needed
            // 'principals' => fn () => Principal::get(),

            // NEVER included on first visit
            // OPTIONALLY included on partial reloads
            // ONLY evaluated when needed
            // 'principals' => Inertia::lazy(fn () => Principal::get()),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Client $client)
    {
        // return $client;

        $clientAll = Client::all('id', 'name');
        $clientAll = array_map(function ($item) {
            return ['value' => $item['id'], 'text' => $item['name']];
        }, $clientAll->toArray());
        // return $client;
        return Inertia::render('Principal/Index', ['display' => 'form', 'clients' => $clientAll, 'activeClient' => $client]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'first_name' => 'required|max:50',
            'middle_name' => 'max:50',
            'last_name' => 'required|max:50',
            'registration_date' => 'date|required',
            'sex' => "required|max:6",
            'date_of_birth' => "required|date",
            'email' => "required|email",
            'telephone' => "max:20",
            'address' => "max:150",
            'zip_code' => "max:50",
            'city' => "max:50",
            'state' => "max:50",
            'country' => "required|max:50",
            'client_id' => 'required|numeric',
            'medical_condition' => '',
            'about' => '',
            // 'photo' => '',
        ], [
            'client_id.required' => 'Please set a Company for this enrollee',
            'sex.required' => 'Please specify a gender'
        ]);

        $person = Principal::create($validated);
        //TODO GET REAL COMPANY RECORD
        $company = 'StewardXpress Nigeria Limited';
        $idCardNumber = $this->generateNewIdNumber('SW', $company, $person->id, 'PR');

        $person->id_card_number = $idCardNumber;
        $person->update();
        // dd();

        return redirect(route('principal.index'))->with('success', 'Post Created Successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Principal  $principal
     * @return \Illuminate\Http\Response
     */
    public function show(Principal $principal)
    {

        // return $principal;
        if (!$principal->id_card_number) {
            //TODO GET REAL COMPANY RECORD
            $company = 'StewardXpress Nigeria Limited';
            $idCardNumber = $this->generateNewIdNumber('SW', $company, $principal->id, 'PR');

            $principal->id_card_number = $idCardNumber;
            $principal->update();
        }

        return Inertia::render('Principal/Index', [
            'principal' => fn () =>  $principal->load(['Spouse', 'Children']),
            'display' => 'view'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Principal  $principal
     * @return \Illuminate\Http\Response
     */
    public function edit(Principal $principal)
    {

        $client = $principal->client;
        $clientAll = Client::all('id', 'name');
        $clientAll = array_map(function ($item) {
            return ['value' => $item['id'], 'text' => $item['name']];
        }, $clientAll->toArray());
        // return $client;
        return Inertia::render('Principal/Index', ['display' => 'form', 'principal' => fn () => $principal, 'activeClient' => $client]);


        // return Inertia::render('Principal/Index', [
        //     'principal' => fn () => $principal,
        //     'display' => 'form'
        // ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Principal  $principal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Principal $principal)
    {
        $data = $this->checkIfValid($request);
        $principal->update($data);
        return redirect(route('principal.edit', $principal->id))->with('success', 'Post Updated Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Principal  $principal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Principal $principal)
    {
        if ($principal) $principal->delete();
        return redirect(route('principal.index'))->with('message', 'Item Deleted');
        dd($principal);
    }

    /*

MY STX FUNCTIONS

    */

    public function generateIdNumber($str)
    {
        $strA = explode(' ', $str);
        $string = "";
        foreach ($strA as $words) {
            if (strlen(trim($words)) && is_string($words))
                $string = $string . $words[0];
        }
        return (strtoupper($string));
    }

    public function pickPhoto(Principal $principal)
    {
        // dd($principal->id);
        return Inertia::render('Principal/PrincipalPhoto', [
            'id' => $principal->id,
            'data' => $principal->only(['id', 'sex'])
        ]);
    }

    public function upload(Request $request, Principal $principal)
    {
        //VALIDATE UPLOADED PHOTO
        $validation = $request->validate([
            'file' => 'required|file|image|mimes:jpeg,png,gif,webp|max:2048'
            // for multiple file uploads
            // 'photo.*' => 'required|file|image|mimes:jpeg,png,gif,webp|max:2048'
        ]);

        //DELETE OLD FILE
        if ($principal->photo) {
            $fileToDelete = Storage::path('public/principals/' . basename($principal->photo));
            if (file_exists($fileToDelete))
                unlink(Storage::path('public/principals/' . basename($principal->photo)));
        }

        //CREATE FILENAME FOR NEW FILE, SCALE AND SAVE
        $id = "stx_principal_" . $principal->id . "_" . md5($principal->id . time());
        $filename = $id . '.jpg';
        $img = Image::make($request->file('file'));
        $img->fit(413, 531);

        //RESIZING THE IMAGE
        // $img->resize(400, 400, function ($constraint) {
        //     $constraint->aspectRatio();
        //     $constraint->upsize();
        // });

        //SAVE IMAGE TO PUBLIC PATH WITH SYMBOLINK STORAGE
        $res = $img->save(Storage::path('public/principals/' . $filename));
        $principal->photo = 'principals/' . $filename;
        $principal->save();

        //REDIRECT
        return redirect()->back()->with('message', 'Image Uploaded');
        // return redirect(route('principal.index'))->with('message', 'Image Uploaded');

        //PREVIEW UPLOADED IMAGE
        // return '<img src="' . asset('storage/principals/' . $filename) . '" />';
    }

    private function checkIfValid(Request $request)
    {
        return $validated = $request->validate([
            'first_name' => 'required|max:50',
            'middle_name' => 'max:50',
            'last_name' => 'required|max:50',
            'registration_date' => 'date|required',
            'sex' => "required|max:6",
            'date_of_birth' => "required|date",
            'email' => "required|email",
            'telephone' => "max:20",
            'address' => "max:150",
            'zip_code' => "max:50",
            'city' => "max:50",
            'state' => "max:50",
            'country' => "required|max:50",
            'client_id' => 'required|numeric',
            'medical_condition' => '',
            'about' => '',
            'id_card_number' => 'required|max:50',
        ], [
            'client_id.required' => 'Please set a Company for this enrollee',
            'sex.required' => 'Please specify a gender'
        ]);
    }

    public function import()
    {
        Excel::import(new PrincipalsImport, 'Principals.xlsx');

        return redirect(route('principal.index'))->with('success', 'All good!');
    }

    public function export()
    {
        return (new PrincipalsExport)->download('principals_list.xlsx');
    }
}
