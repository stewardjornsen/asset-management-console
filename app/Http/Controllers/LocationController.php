<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Location;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Location::all();
        return Inertia::render('Location/LocationHome', [
            'locations' => $locations,
            'count' => $locations->count()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Location/LocationHome', [
            'activeData' => (object) ['id'=>'-1'],
            'display' => 'form'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function do_validation(Request $request)
    {
        return $request->validate([
            "name" => "required",
            "short_name" => "required",
            "address" => "",
            "city" => "",
            "state" => "",
            "country" => "",
            "email" => "email|nullable",
            "mobile" => "",
            "phone_1" => "",
            "phone_2" => "",
            "fax" => "",
            "postal_zip" => "",
            "category" => "",
            "is_operating" => "numeric"
        ]);
    }
    public function store(Request $request)
    {

        $data = $this->do_validation($request);
        $id = Location::create($data);
        return redirect(route('locations.index', $id))->with('msg', 'Completed');
        // return redirect(route('locations.edit', $id))->with('msg', 'Completed');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        return Inertia::render('Location/LocationHome', [
            'activeData' =>
            $location,
            'display' => 'view',
            // 'locations' => Location::all(),
            'count' => Location::count(),
            'id' => $location->id,
            'indexRoute' => 'locations.index',
            'createRoute' => 'locations.create',
            'showRoute' => 'locations.show',
            'editRoute' => 'locations.edit',
            'destroyRoute' => 'locations.destroy',
            'photoRoute' => 'photos.asset',
            'photoDeleteRoute' => 'photos.asset.delete',
            'updateRoute' => 'locations.update',
            'storeRoute' => 'locations.store',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        // return $location;
        return Inertia::render('Location/LocationHome', [
            'activeData' =>
            $location,
            'display' => 'form',
            'count' => Location::count(),
            'id'=>$location->id,
            'indexRoute' => 'locations.index',
            'createRoute' => 'locations.create',
            'showRoute' => 'locations.show',
            'editRoute' => 'locations.edit',
            'destroyRoute' => 'locations.destroy',
            'photoRoute' => 'photos.asset',
            'photoDeleteRoute' => 'photos.asset.delete',
            'updateRoute' => 'locations.update',
            'storeRoute' => 'locations.store',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        $data = $this->do_validation($request);
        // return $location;
        $id = $location->update($data);
        return redirect(route('locations.index', $id))->with('msg', 'Completed');
        // return redirect(route('locations.edit', $location->id))->with('msg', 'Completed');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        $location->delete();
        return redirect(route('locations.index'));
    }


}
