<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Technician;
use Illuminate\Http\Request;

class TechnicianController  extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $technicians = Technician::all();
        return Inertia::render('Technician/TechnicianHome', [
            'technicians' => $technicians,
            'count' => Technician::count()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Technician/TechnicianHome', [
            'activeData' => (object) [],
            'display' => 'form',
            'count' => Technician::count()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function do_validation(Request $request)
    {
        $data = $request->validate([
            "name" => "required",
            "contact_person" => "required",
            "address" => "",
            "city" => "",
            "state" => "",
            "country" => "",
            "email" => "email|nullable",
            "mobile" => "required",
            "phone" => "",
            "specialization" => "",
            "website" => "",
            "postal_zip" => "",
            "notes" => "",
            "status" => "required"
        ]);
        return $data;
    }
    public function store(Request $request)
    {
        // return $request;
        $data = $this->do_validation($request);
        $id = Technician::create($data);
        return redirect(route('technicians.edit', $id))->with('msg', 'Completed');
        // return redirect(route('technicians.edit', $id))->with('msg', 'Completed');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Technician  $technician
     * @return \Illuminate\Http\Response
     */
    public function show(Technician $technician)
    {
        return Inertia::render('Technician/TechnicianHome', [
            'activeData' => $technician,
            'display' => 'view',
            'count' => Technician::count(),
            'id' => $technician->id,
             'indexRoute' => 'technicians.index',
            'createRoute' => 'technicians.create',
            'showRoute' => 'technicians.show',
            'storeRoute' => 'technicians.store',
            'editRoute' => 'technicians.edit',
            'updateRoute' => 'technicians.update',
            'destroyRoute' => 'technicians.destroy',
            'photoRoute' => 'photos.technician',
            'photoDeleteRoute' => 'photos.technician.delete',
            'photos' => $technician->photos
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Technician  $technician
     * @return \Illuminate\Http\Response
     */
    public function edit(Technician $technician)
    {
        // return $technician;
        return Inertia::render('Technician/TechnicianHome', [
            'activeData' => $technician,
            'display' => 'form',
            'count' => Technician::count(),
            'id' => $technician->id,
            'indexRoute' => 'technicians.index',
            'createRoute' => 'technicians.create',
            'showRoute' => 'technicians.show',
            'storeRoute' => 'technicians.store',
            'editRoute' => 'technicians.edit',
            'updateRoute' => 'technicians.update',
            'destroyRoute' => 'technicians.destroy',
            'photoRoute' => 'photos.technician',
            'photoDeleteRoute' => 'photos.technician.delete',
            'photos' => $technician->photos
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Technician  $technician
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Technician $technician)
    {
        $data = $this->do_validation($request);
        // return $technician;
        $id = $technician->update($data);
        return redirect(route('technicians.index', $technician->id))->with('msg', 'Completed');
        // return redirect(route('technicians.edit', $technician->id))->with('msg', 'Completed');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Technician  $technician
     * @return \Illuminate\Http\Response
     */
    public function destroy(Technician $technician)
    {
        $technician->delete();
        return redirect(route('technicians.index'));
    }
}
