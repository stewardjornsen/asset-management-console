<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Client;
use App\Traits\StxTrait;
use App\Models\EnrolleeType;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class ClientController extends Controller
{
    use StxTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items =  Client::all();
        return Inertia::render('Client/ClientIndex', [
            'display' => 'table',
            'items' => $items,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Client $client)
    {
        $client = $client;
        $client = (object) ['id' => null];
        $enrolleetypes = EnrolleeType::get(['id', 'type']);
        return Inertia::render('Client/ClientIndex', ['display' => 'form.create'])
            ->with(compact('enrolleetypes'))
            ->with(compact('client'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        // $enrolleeCode = EnrolleeType::find($request->enrollee_type_id)->only('code')['code'];

        $validated = $this->checkIfValid($request);
        // return $validated;

        $person = Client::create($validated);
        // $company = 'StewardXpress Nigeria Limited';
        // $idCardNumber = $this->generateNewIdNumber('SW', $company, $person->id, $enrolleeCode);

        // $person->id_card_number = $idCardNumber;
        $person->update();
        //

        return redirect(route('client.index'))->with('success', 'Post Created Successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {


        // return $client->principals;
        // if (!$client->id_card_number) {
        //     $enrolleeCode = EnrolleeType::find($request->enrollee_type_id)->only('code')['code'];
        //     //TODO GET REAL COMPANY RECORD FROM PRINCIPAL
        //     $company = 'StewardXpress Nigeria Limited';

        //     $idCardNumber = $this->generateNewIdNumber('SW', $company, $client->id, $enrolleeCode);

        //     $client->id_card_number = $idCardNumber;
        //     $client->update();
        // }

        return Inertia::render('Client/ClientIndex', [
            'client' => fn () => $client->load('principals'),
            'display' => 'view'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return Inertia::render('Client/ClientIndex', [
            'client' => fn () => $client,
            'display' => 'form.edit'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $data = $this->checkIfValid($request);
        $client->update($data);
        return redirect(route('client.edit', $client->id))->with('success', 'Post Updated Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        if ($client) $client->delete();
        return redirect(route('client.index'))->with('message', 'Item Deleted');
        dd($client);
    }

    /*

MY STX FUNCTIONS

    */

    public function generateIdNumber($str)
    {
        $strA = explode(' ', $str);
        $string = "";
        foreach ($strA as $words) {
            if (strlen(trim($words)) && is_string($words))
                $string = $string . $words[0];
        }
        return (strtoupper($string));
    }

    public function pickPhoto(Client $client)
    {
        // dd($client->id);
        return Inertia::render('Client/DependantPhoto', [
            'id' => $client->id,
            'data' => $client->only(['id', 'sex'])
        ]);
    }

    public function upload(Request $request, Client $client)
    {
        //VALIDATE UPLOADED PHOTO
        $validation = $request->validate([
            'file' => 'required|file|image|mimes:jpeg,png,gif,webp|max:2048'
            // for multiple file uploads
            // 'photo.*' => 'required|file|image|mimes:jpeg,png,gif,webp|max:2048'
        ]);

        //DELETE OLD FILE
        if ($client->photo) {
            $fileToDelete = Storage::path('public/clients/' . basename($client->photo));

            if (file_exists($fileToDelete))
                unlink(Storage::path('public/clients/' . basename($client->photo)));
        }

        //CREATE FILENAME FOR NEW FILE, SCALE AND SAVE
        $id = "stx_client_" . $client->id . "_" . md5($client->id . time());
        $filename = $id . '.jpg';
        $img = Image::make($request->file('file'));
        // $img->fit(413, 531);

        //RESIZING THE IMAGE
        $img->resize(400, 400, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        //SAVE IMAGE TO PUBLIC PATH WITH SYMBOLINK STORAGE
        $res = $img->save(Storage::path('public/clients/' . $filename));
        $client->photo = 'clients/' . $filename;
        $client->save();

        //REDIRECT
        return redirect()->back()->with('message', 'Image Uploaded');
        // return redirect(route('client.index'))->with('message', 'Image Uploaded');

        //PREVIEW UPLOADED IMAGE
        // return '<img src="' . asset('storage/clients/' . $filename) . '" />';
    }

    private function checkIfValid(Request $request)
    {


        $firstLevel = [
            'name' => 'required|max:100',
            'policy_code' => 'max:50',
            'policy_number' => 'max:50',
            'registration_date' => 'required|date',
            'about' => '',

        ];
        $secondLevel = [
            'email' => "required|email",
            'telephone' => "required|max:20",
            'address' => "required|max:150",
            'zip_code' => "max:10",
            'city' => "required|max:50",
            'state' => "required|max:50",
            'country' => "required|max:50",
        ];

        $thirdLevel = [
            'hmo_id' => 'numeric',
            'user_id' => 'numeric',
        ];

        // if ($request->same_address_as_principal) {
        //     $finalLevel = array_merge($firstLevel);
        //     if ($request->has('id')) {
        //         $finalLevel = array_merge($firstLevel, $thirdLevel);
        //     }
        // } else {
        //     $finalLevel = array_merge($firstLevel, $secondLevel);
        //     if ($request->has('id')) {
        //     }
        // }
        $finalLevel = array_merge($firstLevel, $secondLevel, $thirdLevel);

        return  $validated = $request->validate($finalLevel, [
            'name.required' => 'Provide the Company, Business or Organization Name',
            'email.required' => 'A valid email is required to create your account'
        ]);

        // return $validated = $request->validate([
        //     'first_name' => 'required|max:50',
        //     'middle_name' => 'max:50',
        //     'last_name' => 'required|max:50',
        //     'registration_date' => 'date|required',
        //     'sex' => "required|max:6",
        //     'date_of_birth' => "required|date",
        //     'email' => "required|email",
        //     'telephone' => "max:20",
        //     'address' => "max:150",
        //     'zip_code' => "max:50",
        //     'city' => "max:50",
        //     'state' => "max:50",
        //     'country' => "required|max:50",
        //     'client_id' => 'required|numeric',
        //     'medical_condition' => '',
        //     'about' => '',
        //     'id_card_number' => 'required|max:50',
        //     'principal_id' => 'required|numeric',
        //     'enrollee_type_id' => 'required|numeric',
        // ], [
        //     'client_id.required' => 'Please set a Company for this enrollee',
        //     'sex.required' => 'Please specify a gender'
        // ]);
    }
}
