<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Models\Photo;
use App\Models\Vendor;
use App\Models\Technician;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class PhotoController extends Controller
{

    public function uploader(Request $request, $path, $filename, $model)
    {
        $validation = $request->validate([
            'file' => 'required|file|image|mimes:jpeg,png,gif,webp|max:2048'
        ]);
        $validation = $request->validate([
            'file' => 'required|file|image|mimes:jpeg,png,gif,webp|max:2048'
        ]);

        if (!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }


        $img = Image::make($request->file('file'));
        $img->resize(800, 800, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $img->save($path . $filename);

        $data = ['url' => $path . $filename, 'info' => json_encode($img->exif())];
        $model->photos()->create($data);

        return redirect()->back();
        return redirect(route('vendors.show', $model));
    }



    public function asset_upload(Request $request, Asset $asset)
    {
        $path = 'photos/assets/';
        $filename = "stx_asset_" . $asset->id . "_" . md5($asset->id . time()) . '.jpg';
        $this->uploader($request, $path, $filename, $asset);
        return redirect()->back();
    }

    public function vendor_upload(Request $request, Vendor $vendor)
    {
        $path = 'photos/vendors/';
        $filename = "stx_vendor_" . $vendor->id . "_" . md5($vendor->id . time()) . '.jpg';
        $this->uploader($request, $path, $filename, $vendor);
        return redirect()->back();
    }


    public function technician_upload(Request $request, Technician $technician)
    {
        $path = 'photos/technicians/';
        $filename = "stx_technician_" . $technician->id . "_" . md5($technician->id . time()) . '.jpg';
        $this->uploader($request, $path, $filename, $technician);
        return redirect()->back();
    }

    /*
    DELETE LOGICS
    */

    public function deleter(Photo $photo)
    {
        $path =  $photo->url;
        $photo->delete();
        File::delete($path);
        return redirect()->back();
    }

    public function asset_delete(Photo $photo)
    {
        $this->deleter($photo);
        return redirect()->back();
    }

    public function vendor_delete(Photo $photo)
    {
        $this->deleter($photo);
        return redirect()->back();
    }

    public function technician_delete(Photo $photo)
    {
        $this->deleter($photo);
        return redirect()->back();
    }


    // public function asset($asset, $data)
    // {
    //     // $data = ['url' => 'xxx', 'info' => 'blah blah'];
    //     return  $asset2 =  $asset->photos()->create($data);
    // }

    // public function asset_upload(Request $request, Asset $asset)
    // {

    //     //VALIDATE UPLOADED PHOTO
    //     $validation = $request->validate([
    //         'file' => 'required|file|image|mimes:jpeg,png,gif,webp|max:2048'
    //         // for multiple file uploads
    //         // 'photo.*' => 'required|file|image|mimes:jpeg,png,gif,webp|max:2048'
    //     ]);



    //     //CREATE FILENAME FOR NEW FILE, SCALE AND SAVE
    //     $id = "stx_asset_" . $asset->id . "_" . md5($asset->id . time());
    //     $filename = $id . '.jpg';
    //     $img = Image::make($request->file('file'));
    //     // $img->fit(413, 531);

    //     //RESIZING THE IMAGE
    //     $img->resize(800, 800, function ($constraint) {
    //         $constraint->aspectRatio();
    //         $constraint->upsize();
    //     });


    //     Storage::makeDirectory('assets/');


    //     //SAVE IMAGE TO PUBLIC PATH WITH SYMBOLINK STORAGE
    //     $path = 'photos/assets/';
    //     if (!File::exists($path)) {
    //         File::makeDirectory($path, $mode = 0777, true, true);
    //     }

    //     $img->save('photos/assets/' . $filename);
    //     // $res = $img->save(Storage::path('assets/' . $filename));
    //     // $asset->photo = 'assets/' . $filename;
    //     // $asset->save();

    //     $info = json_encode(
    //         [
    //             'filesize' => $img->filesize(),
    //             'exif' => $img->exif()
    //         ]
    //     );

    //     $data = ['url' => $path . $filename, 'info' => $info];
    //     $asset->photos()->create($data);


    //     //DELETE OLD FILE
    //     // if ($asset->photo) {
    //     //     $fileToDelete = Storage::path('public/assets/' . basename($asset->photo));
    //     //     if (file_exists($fileToDelete))
    //     //         unlink(Storage::path('public/assets/' . basename($asset->photo)));
    //     // }

    //     //REDIRECT
    //     return redirect()->back();
    //     return redirect(route('assets.show', $asset))->with('message', 'Image Uploaded');
    //     // return redirect(route('asset.index'))->with('message', 'Image Uploaded');

    //     //PREVIEW UPLOADED IMAGE
    //     // return '<img src="' . asset('storage/assets/' . $filename) . '" />';
    // }

    // // public function asset_delete(Request $request, Asset $asset, Photo $photo)
    // // {
    // //     $photo->delete();
    // //     return redirect()->back();
    // //     // return redirect(route('assets.show', $asset));
    // // }


    /*
     Vendors Image Upload
      */
}
