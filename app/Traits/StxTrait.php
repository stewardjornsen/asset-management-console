<?php

namespace App\Traits;

trait StxTrait
{
    protected function generateNewIdNumber($prefix = 'SW', $companyName, $number, $suffix = 'PR')
    {
        $divider = '-';
        if ($companyName && (is_numeric($number * 1) && $number > 0)) {
            $numberCode = sprintf("%06d", $number * 1);
            $idNumber = strtoupper($prefix) . $divider . $this->generateCode($companyName) . $divider . $numberCode . $divider . strtoupper($suffix);
        }
        return     $idNumber;
    }

    protected function generateCode($str)
    {
        $strA = explode(' ', $str);
        $string = "";
        foreach ($strA as $words) {
            $words = preg_replace("/[^a-zA-Z]+/", "", $words);
            if (strlen(trim($words)) && is_string($words))
                $string = $string . $words[0];
        }

        return (strtoupper($string));
    }
}
