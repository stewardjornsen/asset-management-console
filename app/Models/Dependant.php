<?php

namespace App\Models;

use App\Models\Principal;
use App\Models\EnrolleeType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Dependant extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function EnrolleeType()
    {
        return $this->belongsTo(EnrolleeType::class);
    }
    public function Principal()
    {
        return $this->belongsTo(Principal::class);
    }

    public function getPhotoAttribute($value)
    {
        $avatar = $this->sex === 'Male' ? '/img/man-svgrepo-com.svg'
            : '/img/woman-svgrepo-com.svg';
        return $value ? asset('storage/' . $value) : $avatar;
    }
}
