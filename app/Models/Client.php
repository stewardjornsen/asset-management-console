<?php

namespace App\Models;

use App\Models\Principal;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Client extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function getPhotoAttribute($value)
    {
        $avatar = '/img/picture-svgrepo-com.svg';
        return $value ? asset('storage/' . $value) : $avatar;
    }

    public function principals()
    {
        return $this->hasMany(Principal::class);
    }
}
