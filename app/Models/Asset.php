<?php

namespace App\Models;

use App\Models\Photo;
use App\Models\Vendor;
use App\Models\Location;
use App\Models\Technician;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Asset extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $guarded = [];
    // protected $appends = ['ExpiredAsset'];

    protected $today, $lastTwoMonths, $lastOneMonth;

    function __construct()
    {
        $this->today = Carbon::today()->toDateString();
        $this->lastTwoMonths = Carbon::today()->subMonth(2)->toDateString();
        $this->lastOneMonth = Carbon::today()->subMonth(1)->toDateString();
    }

    public function vendors()
    {
        return $this->belongsToMany(Vendor::class);
    }

    public function technicians()
    {
        return $this->belongsToMany(Technician::class);
    }

    public function locations()
    {
        return $this->belongsToMany(Location::class);
    }

    public function photos()
    {
        return $this->morphMany(Photo::class, 'photoable');
    }

    public function getExpiredAssetAttribute()
    {

        $dt = Carbon::parse($this->warranty_end);
        return Carbon::now()->diffForHumans($dt). ' expiration';
    }

    public function scopeExpiredAssets($query)
    {

        //Valid Assets that have NOT expired
        // return $query->where('is_asset', 1)->whereDate('warranty_end', '>', $this->today);

        //Valid Assets that have NOT expired
        return $query->where('is_asset', 1)->whereBetween('warranty_end', [$this->lastTwoMonths, $this->today]);


        //Assets that have expired
        return $query->where('is_asset', 1)->whereDate('warranty_end', '<=', $this->today);
    }
}
