<?php

namespace App\Models;

use App\Models\Client;
use App\Models\Dependant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Principal extends Model
{
    use  HasFactory;
    protected $softDelete = true;
    protected $guarded = [];

    public function Spouse()
    {
        return $this->hasMany(Dependant::class)->where('enrollee_type_id', 2);
    }
    public function Children()
    {
        return $this->hasMany(Dependant::class)->where('enrollee_type_id', '>', 2);
    }

    public function getPhotoAttribute($value)
    {
        $avatar = $this->sex === 'Male' ? '/img/man-svgrepo-com.svg'
            : '/img/woman-svgrepo-com.svg';
        return $value ? asset('storage/' . $value) : $avatar;
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
