<?php

namespace App\Models;

use App\Models\Asset;
use App\Models\Photo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Vendor extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $guarded = [];

    public function assets()
    {
        return $this->belongsToMany(Asset::class);
    }

    public function photos()
    {
        return $this->morphMany(Photo::class, 'photoable');
    }
}
