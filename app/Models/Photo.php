<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Photo extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function photoable()
    {
        return $this->morphTo();
    }

    // public function getUrlAttribute($value)
    // {
    //     return  url("photos/{$value}");
    //     // return  asset("photos/{$value}");
    //     // return  asset('storage/' . $value) ;
    // }
}
