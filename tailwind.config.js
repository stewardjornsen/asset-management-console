const defaultTheme = require('tailwindcss/defaultTheme');
const colors = require("tailwindcss/colors");

module.exports = {
    purge: [
        "./vendor/laravel/jetstream/**/*.blade.php",
        "./storage/framework/views/*.php",
        "./resources/views/**/*.blade.php",
        "./resources/js/**/*.vue"
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ["Nunito", ...defaultTheme.fontFamily.sans],
                poppins: ["Poppins", "Nunito", ...defaultTheme.fontFamily.sans],
                nunito: ["Nunito", "Poppins", ...defaultTheme.fontFamily.sans]
            },
            colors: {
                primary: colors.red,
                secondary: colors.emerald,
                neutral: colors.gray,
                gray: colors.coolGray,
                brand: {
                    "100": "#07689F",
                    "200": "#C9D6DF",
                    "300": "#11D3BC",
                    "400": "#F67280",
                    "500": "#e02424",
                    // "500": "#A2D5F2",
                    "600": "#FF7E67",
                    "700": "#FAFAFA",
                    "800": "#4E4E4E",
                    "900": "#faca15"
                },

                denim: {
                    "50": "#f3fafc",
                    "100": "#dff7fb",
                    "200": "#b5ecf7",
                    "300": "#82dcf4",
                    "400": "#3fbdf1",
                    "500": "#1997eb",
                    "600": "#1274dd",
                    "700": "#155bb9",
                    "800": "#144688",
                    "900": "#123868"
                },
                royalblue: {
                    "50": "#f5f9fc",
                    "100": "#e8f3fc",
                    "200": "#ccdefa",
                    "300": "#adc4fa",
                    "400": "#8498fa",
                    "500": "#596af9",
                    "600": "#4048f4",
                    "700": "#3539e0",
                    "800": "#2a2db3",
                    "900": "#22268c"
                },
                flamingo: {
                    "50": "#f6f7fb",
                    "100": "#efedfb",
                    "200": "#ded2f9",
                    "300": "#cdb4f8",
                    "400": "#bb86f7",
                    "500": "#a659f6",
                    "600": "#883af1",
                    "700": "#672eda",
                    "800": "#4e26ae",
                    "900": "#3e2188"
                },
                cerise: {
                    "50": "#fcf9f9",
                    "100": "#fceff6",
                    "200": "#f9d1ec",
                    "300": "#f7aadf",
                    "400": "#f872c8",
                    "500": "#f946ac",
                    "600": "#f02a87",
                    "700": "#cd2068",
                    "800": "#9d1b4d",
                    "900": "#79173c"
                },
                maroon: {
                    "50": "#fcf9f8",
                    "100": "#fcf1f1",
                    "200": "#fad7e2",
                    "300": "#f8b2c8",
                    "400": "#f97c9b",
                    "500": "#f9506e",
                    "600": "#f0314b",
                    "700": "#ce253e",
                    "800": "#9f1e33",
                    "900": "#7c192a"
                },
                mango: {
                    "50": "#fbf8f3",
                    "100": "#fcf0e5",
                    "200": "#f9ddc6",
                    "300": "#f7bf93",
                    "400": "#f69253",
                    "500": "#f5672b",
                    "600": "#ea441b",
                    "700": "#c8331d",
                    "800": "#9d2920",
                    "900": "#7c221d"
                },
                chocolate: {
                    "50": "#fbf8f2",
                    "100": "#fbf3dd",
                    "200": "#f7e5b0",
                    "300": "#f3ce71",
                    "400": "#eea832",
                    "500": "#ea8115",
                    "600": "#d65c0d",
                    "700": "#ad4411",
                    "800": "#853515",
                    "900": "#682b15"
                },
                carrot: {
                    "50": "#fbf9f3",
                    "100": "#faf6de",
                    "200": "#f6ebad",
                    "300": "#f0d76b",
                    "400": "#e5b42b",
                    "500": "#d99010",
                    "600": "#bc6a0a",
                    "700": "#91500d",
                    "800": "#6b3c12",
                    "900": "#523013"
                },
                green: {
                    "50": "#f8faf8",
                    "100": "#f3f8ed",
                    "200": "#e4f0cf",
                    "300": "#cce2a4",
                    "400": "#97c764",
                    "500": "#5ea833",
                    "600": "#428520",
                    "700": "#37671f",
                    "800": "#2d4e20",
                    "900": "#243d1e"
                },
                pacific: {
                    "50": "#f2f9fa",
                    "100": "#e0f7f8",
                    "200": "#b9eeef",
                    "300": "#86dfe7",
                    "400": "#3fc5db",
                    "500": "#19a4c9",
                    "600": "#1483ae",
                    "700": "#07689f",
                    "800": "#185067",
                    "900": "#154051"
                }
            }
        }
    },

    variants: {
        fill: ["responsive", "hover", "focus", "group-hover", "disabled"],
        textColor: ["responsive", "hover", "focus", "group-hover"],
        zIndex: ["responsive", "focus"]
    },

    plugins: [require("@tailwindcss/forms"), require("@tailwindcss/typography")]
};
